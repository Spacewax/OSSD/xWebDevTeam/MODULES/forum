<?php
// $Id: xoops_version.php,v 1.6 2004/05/29 17:11:48 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //

$modversion['name'] = "NewBB";
$modversion['version'] = 2.00;
$modversion['description'] = _MI_NEWBB_DESC;
$modversion['credits'] = "The XOOPS Project, samuels, phppp, Dave_L, herve, Mithrandir,  The French Xoops Support";
$modversion['author'] = "The XOOPS Project Module Dev Team - Teamleader Predator";
$modversion['help'] = "";
$modversion['license'] = "GNU General Public License (GPL) see LICENSE";
$modversion['official'] = 1;
$modversion['image'] = "images/xoopsbb_slogo.png";
$modversion['dirname'] = "newbb";

// Added by marcan for the About page in admin section
$modversion['author_realname'] = "Marko Schmuck";
$modversion['author_website_url'] = "http://dev.xoops.org";
$modversion['author_website_name'] = "dev.xoops.org";
$modversion['author_email'] = "predator@xoops.org";
$modversion['status'] = "Beta testing";

$modversion['warning'] = "This module comes as is, without any guarantees what so ever. 
This module is BETA, meaning it is still under active development. This release is meant for
<b>testing purposes only</b> and we <b>strongly</b> recommend that you do not use it on a live 
website or in a production environment.";

$modversion['demo_site_url'] = "http://testsite.xoops2.org";
$modversion['demo_site_name'] = "NewBB Demo";
$modversion['support_site_url'] = "http://dev.xoops.org/modules/xfmod/project/?newbb";
$modversion['support_site_name'] = "Newbb on the Developpers Forge";
$modversion['submit_bug'] = "http://dev.xoops.org/modules/xfmod/tracker/?func=add&group_id=1001&atid=104";
$modversion['submit_feature'] = "http://dev.xoops.org/modules/xfmod/tracker/?func=add&group_id=1001&atid=107";

$modversion['author_word'] = "
<b>NewBB</b> is the result of multiple ideas from multiple people. 
<br /><br />
Special thanks also to Mithrandir (Jan Pedersen) for all the help by the grouppermission.
<br /><br />
Finally, thanks to all the people who made that module possible, samuels, phppp, David_L, herve, bbchen and so many others !
<br /><br />
So, enjoy newBB !
";


// Sql file (must contain sql generated by phpMyAdmin or phpPgAdmin)
// All tables should not have any prefix!
$modversion['sqlfile']['mysql'] = "sql/mysql.sql";

// Tables created by sql file (without prefix!)
$modversion['tables'][0] = "bb_archive";
$modversion['tables'][1] = "bb_categories";
$modversion['tables'][2] = "bb_moderate_user";
$modversion['tables'][3] = "bb_forums";
$modversion['tables'][4] = "bb_posts";
$modversion['tables'][5] = "bb_posts_text";
$modversion['tables'][6] = "bb_topics";
$modversion['tables'][7] = "bb_whoisonline";


// Admin things
$modversion['hasAdmin'] = 1;
$modversion['adminindex'] = "admin/index.php";
$modversion['adminmenu'] = "admin/menu.php";

// Menu
$modversion['hasMain'] = 1;

// Templates
$modversion['templates'][1]['file'] = 'newbb_index.html';
$modversion['templates'][1]['description'] = '';
$modversion['templates'][2]['file'] = 'newbb_search.html';
$modversion['templates'][2]['description'] = '';
$modversion['templates'][3]['file'] = 'newbb_searchresults.html';
$modversion['templates'][3]['description'] = '';
$modversion['templates'][4]['file'] = 'newbb_thread.html';
$modversion['templates'][4]['description'] = '';
$modversion['templates'][5]['file'] = 'newbb_viewforum.html';
$modversion['templates'][5]['description'] = '';
$modversion['templates'][6]['file'] = 'newbb_viewtopic_flat.html';
$modversion['templates'][6]['description'] = '';
$modversion['templates'][7]['file'] = 'newbb_viewtopic_thread.html';
$modversion['templates'][7]['description'] = '';
$modversion['templates'][8]['file'] = 'newbb_quickreply.html';
$modversion['templates'][8]['description'] = '';
$modversion['templates'][9]['file'] = 'newbb_post.html';
$modversion['templates'][9]['description'] = '';
$modversion['templates'][10]['file'] = 'newbb_preview.html';
$modversion['templates'][10]['description'] = '';

// Blocks
$modversion['blocks'][1]['file'] = "newbb_new.php";
$modversion['blocks'][1]['name'] = _MI_NEWBB_BNAME1;
$modversion['blocks'][1]['description'] = "Shows recent topics in the forums";
$modversion['blocks'][1]['show_func'] = "b_newbb_new_show";
$modversion['blocks'][1]['options'] = "10|1|time";
$modversion['blocks'][1]['edit_func'] = "b_newbb_new_edit";
$modversion['blocks'][1]['template'] = 'newbb_block_new.html';

$modversion['blocks'][2]['file'] = "newbb_new.php";
$modversion['blocks'][2]['name'] = _MI_NEWBB_BNAME2;
$modversion['blocks'][2]['description'] = "Shows most viewed topics in the forums";
$modversion['blocks'][2]['show_func'] = "b_newbb_new_show";
$modversion['blocks'][2]['options'] = "10|1|views";
$modversion['blocks'][2]['edit_func'] = "b_newbb_new_edit";
$modversion['blocks'][2]['template'] = 'newbb_block_top.html';

$modversion['blocks'][3]['file'] = "newbb_new.php";
$modversion['blocks'][3]['name'] = _MI_NEWBB_BNAME3;
$modversion['blocks'][3]['description'] = "Shows most active topics in the forums";
$modversion['blocks'][3]['show_func'] = "b_newbb_new_show";
$modversion['blocks'][3]['options'] = "10|1|replies";
$modversion['blocks'][3]['edit_func'] = "b_newbb_new_edit";
$modversion['blocks'][3]['template'] = 'newbb_block_active.html';

$modversion['blocks'][4]['file'] = "newbb_new.php";
$modversion['blocks'][4]['name'] = _MI_NEWBB_BNAME4;
$modversion['blocks'][4]['description'] = "Shows recent and private topics in the forums";
$modversion['blocks'][4]['show_func'] = "b_newbb_new_private_show";
$modversion['blocks'][4]['options'] = "10|1|time";
$modversion['blocks'][4]['edit_func'] = "b_newbb_new_edit";
$modversion['blocks'][4]['template'] = 'newbb_block_prv.html';

// Search
$modversion['hasSearch'] = 1;
$modversion['search']['file'] = "include/search.inc.php";
$modversion['search']['func'] = "newbb_search";

// Smarty
$modversion['use_smarty'] = 1;

$handle = opendir(XOOPS_ROOT_PATH.'/modules/newbb/images/imagesets/');

$i=0;
while (false !== ($file = readdir($handle))) {
  if (is_dir(XOOPS_ROOT_PATH.'/modules/newbb/images/imagesets/'.$file) && !preg_match("/^[.]{1,2}$/",$file) && strtolower($file) != 'cvs') {

    $dirlist[$file]=$i;
    $i++;

  }

}
closedir($handle);

$modversion['config'][1]['name'] = 'image_set';
$modversion['config'][1]['title'] = '_MI_IMG_SET';
$modversion['config'][1]['description'] = '_MI_IMG_SET_DESC';
$modversion['config'][1]['formtype'] = 'select';
$modversion['config'][1]['valuetype'] = 'array';
$modversion['config'][1]['options'] = $dirlist;
$modversion['config'][1]['default'] = 0; 

$modversion['config'][2]['name'] = 'dir_attachments';
$modversion['config'][2]['title'] = '_MI_DIR_ATTACHMENT';
$modversion['config'][2]['description'] = '_MI_DIR_ATTACHMENT_DESC';
$modversion['config'][2]['formtype'] = 'textbox';
$modversion['config'][2]['valuetype'] = 'text';
$modversion['config'][2]['default'] = 'attachments';

$modversion['config'][3]['name'] = 'max_img_width';
$modversion['config'][3]['title'] = '_MI_MAX_IMG_WIDTH';
$modversion['config'][3]['description'] = '_MI_MAX_IMGWIDTHDSC';
$modversion['config'][3]['formtype'] = 'textbox';
$modversion['config'][3]['valuetype'] = 'int';
$modversion['config'][3]['default'] = 500;

$modversion['config'][4]['name'] = 'wol_enabled';
$modversion['config'][4]['title'] = '_MI_WOL_ENABLE';
$modversion['config'][4]['description'] = '_MI_WOL_ENABLE_DESC';
$modversion['config'][4]['formtype'] = 'yesno';
$modversion['config'][4]['valuetype'] = 'int';
$modversion['config'][4]['default'] = 1;

$modversion['config'][5]['name'] = 'wol_admin_col';
$modversion['config'][5]['title'] = '_MI_WOL_ADMIN_COL';
$modversion['config'][5]['description'] = '_MI_WOL_ADMIN_COL_DESC';
$modversion['config'][5]['formtype'] = 'textbox';
$modversion['config'][5]['valuetype'] = 'text';
$modversion['config'][5]['default'] = '#FFA34F';

$modversion['config'][6]['name'] = 'wol_mod_col';
$modversion['config'][6]['title'] = '_MI_WOL_MOD_COL';
$modversion['config'][6]['description'] = '_MI_WOL_MOD_COL_DESC';
$modversion['config'][6]['formtype'] = 'textbox';
$modversion['config'][6]['valuetype'] = 'text';
$modversion['config'][6]['default'] = '#006600';

$modversion['config'][7]['name'] = 'levels_enabled';
$modversion['config'][7]['title'] = '_MI_LEVELS_ENABLE';
$modversion['config'][7]['description'] = '_MI_LEVELS_ENABLE_DESC';
$modversion['config'][7]['formtype'] = 'yesno';
$modversion['config'][7]['valuetype'] = 'int';
$modversion['config'][7]['default'] = 1;

$modversion['config'][8]['name'] = 'userbar_enabled';
$modversion['config'][8]['title'] = '_MI_USERBAR_ENABLE';
$modversion['config'][8]['description'] = '_MI_USERBAR_ENABLE_DESC';
$modversion['config'][8]['formtype'] = 'yesno';
$modversion['config'][8]['valuetype'] = 'int';
$modversion['config'][8]['default'] = 1;

$modversion['config'][9]['name'] = 'rating_enabled';
$modversion['config'][9]['title'] = '_MI_RATING_ENABLE';
$modversion['config'][9]['description'] = '_MI_RATING_ENABLE_DESC';
$modversion['config'][9]['formtype'] = 'yesno';
$modversion['config'][9]['valuetype'] = 'int';
$modversion['config'][9]['default'] = 1;

$modversion['config'][10]['name'] = 'reportmod_enabled';
$modversion['config'][10]['title'] = '_MI_REPORTMOD_ENABLE';
$modversion['config'][10]['description'] = '_MI_REPORTMOD_ENABLE_DESC';
$modversion['config'][10]['formtype'] = 'yesno';
$modversion['config'][10]['valuetype'] = 'int';
$modversion['config'][10]['default'] = 0;

$modversion['config'][11]['name'] = 'rss_enable';
$modversion['config'][11]['title'] = '_MI_RSS_ENABLE';
$modversion['config'][11]['description'] = '_MI_RSS_ENABLE_DESC';
$modversion['config'][11]['formtype'] = 'yesno';
$modversion['config'][11]['valuetype'] = 'int';
$modversion['config'][11]['default'] = 1;

$modversion['config'][12]['name'] = 'rss_maxitems';
$modversion['config'][12]['title'] = '_MI_RSS_MAX_ITEMS';
$modversion['config'][12]['description'] = '';
$modversion['config'][12]['formtype'] = 'textbox';
$modversion['config'][12]['valuetype'] = 'int';
$modversion['config'][12]['default'] = 10;

$modversion['config'][13]['name'] = 'rss_maxdescription';
$modversion['config'][13]['title'] = '_MI_RSS_MAX_DESCRIPTION';
$modversion['config'][13]['description'] = '';
$modversion['config'][13]['formtype'] = 'textbox';
$modversion['config'][13]['valuetype'] = 'int';
$modversion['config'][13]['default'] = 200;

$modversion['config'][14]['name'] = 'disc_show';
$modversion['config'][14]['title'] = '_MI_SHOW_DIS';
$modversion['config'][14]['description'] = '_MI_SHOW_DIS_DESC';
$modversion['config'][14]['formtype'] = 'select';
$modversion['config'][14]['valuetype'] = 'int';
$modversion['config'][14]['default'] = 0;
$modversion['config'][14]['options'] = array( _NONE => 0, _MI_POST => 1, _MI_REPLY => 2, _MI_OP_BOTH=> 3);

$modversion['config'][15]['name'] = 'disclaimer';
$modversion['config'][15]['title'] = '_MI_DISCLAIMER';
$modversion['config'][15]['description'] = '_MI_DISCLAIMER_DESC';
$modversion['config'][15]['formtype'] = 'textarea';
$modversion['config'][15]['valuetype'] = 'text';
$modversion['config'][15]['default'] = '';

$modversion['config'][16]['name'] = 'media_allowed';
$modversion['config'][16]['title'] = '_MI_MEDIA_ENABLE';
$modversion['config'][16]['description'] = '_MI_MEDIA_ENABLE_DESC';
$modversion['config'][16]['formtype'] = 'yesno';
$modversion['config'][16]['valuetype'] = 'int';
$modversion['config'][16]['default'] = 1;

$modversion['config'][17]['name'] = 'view_mode';
$modversion['config'][17]['title'] = '_MI_VIEWMODE';
$modversion['config'][17]['description'] = '_MI_VIEWMODE_DESC';
$modversion['config'][17]['formtype'] = 'select';
$modversion['config'][17]['valuetype'] = 'int';
$modversion['config'][17]['default'] = 1;
$modversion['config'][17]['options'] = array( _NONE => 0, _FLAT => 1, _THREADED => 2);

$modversion['config'][18]['name'] = 'hide_private';
$modversion['config'][18]['title'] = '_MI_HIDE_PRIVATE';
$modversion['config'][18]['description'] = '_MI_HIDE_PRIVATE_DESC';
$modversion['config'][18]['formtype'] = 'yesno';
$modversion['config'][18]['valuetype'] = 'int';
$modversion['config'][18]['default'] = 1;



// Notification

$modversion['hasNotification'] = 1;
$modversion['notification']['lookup_file'] = 'include/notification.inc.php';
$modversion['notification']['lookup_func'] = 'newbb_notify_iteminfo';

$modversion['notification']['category'][1]['name'] = 'thread';
$modversion['notification']['category'][1]['title'] = _MI_NEWBB_THREAD_NOTIFY;
$modversion['notification']['category'][1]['description'] = _MI_NEWBB_THREAD_NOTIFYDSC;
$modversion['notification']['category'][1]['subscribe_from'] = 'viewtopic.php';
$modversion['notification']['category'][1]['item_name'] = 'topic_id';
$modversion['notification']['category'][1]['allow_bookmark'] = 1;

$modversion['notification']['category'][2]['name'] = 'forum';
$modversion['notification']['category'][2]['title'] = _MI_NEWBB_FORUM_NOTIFY;
$modversion['notification']['category'][2]['description'] = _MI_NEWBB_FORUM_NOTIFYDSC;
$modversion['notification']['category'][2]['subscribe_from'] = array('viewtopic.php', 'viewforum.php');
$modversion['notification']['category'][2]['item_name'] = 'forum';
$modversion['notification']['category'][2]['allow_bookmark'] = 1;

$modversion['notification']['category'][3]['name'] = 'global';
$modversion['notification']['category'][3]['title'] = _MI_NEWBB_GLOBAL_NOTIFY;
$modversion['notification']['category'][3]['description'] = _MI_NEWBB_GLOBAL_NOTIFYDSC;
$modversion['notification']['category'][3]['subscribe_from'] = array('index.php', 'viewtopic.php', 'viewforum.php');

$modversion['notification']['event'][1]['name'] = 'new_post';
$modversion['notification']['event'][1]['category'] = 'thread';
$modversion['notification']['event'][1]['title'] = _MI_NEWBB_THREAD_NEWPOST_NOTIFY;
$modversion['notification']['event'][1]['caption'] = _MI_NEWBB_THREAD_NEWPOST_NOTIFYCAP;
$modversion['notification']['event'][1]['description'] = _MI_NEWBB_THREAD_NEWPOST_NOTIFYDSC;
$modversion['notification']['event'][1]['mail_template'] = 'thread_newpost_notify';
$modversion['notification']['event'][1]['mail_subject'] = _MI_NEWBB_THREAD_NEWPOST_NOTIFYSBJ;

$modversion['notification']['event'][2]['name'] = 'new_thread';
$modversion['notification']['event'][2]['category'] = 'forum';
$modversion['notification']['event'][2]['title'] = _MI_NEWBB_FORUM_NEWTHREAD_NOTIFY;
$modversion['notification']['event'][2]['caption'] = _MI_NEWBB_FORUM_NEWTHREAD_NOTIFYCAP;
$modversion['notification']['event'][2]['description'] = _MI_NEWBB_FORUM_NEWTHREAD_NOTIFYDSC;
$modversion['notification']['event'][2]['mail_template'] = 'forum_newthread_notify';
$modversion['notification']['event'][2]['mail_subject'] = _MI_NEWBB_FORUM_NEWTHREAD_NOTIFYSBJ;

$modversion['notification']['event'][3]['name'] = 'new_forum';
$modversion['notification']['event'][3]['category'] = 'global';
$modversion['notification']['event'][3]['title'] = _MI_NEWBB_GLOBAL_NEWFORUM_NOTIFY;
$modversion['notification']['event'][3]['caption'] = _MI_NEWBB_GLOBAL_NEWFORUM_NOTIFYCAP;
$modversion['notification']['event'][3]['description'] = _MI_NEWBB_GLOBAL_NEWFORUM_NOTIFYDSC;
$modversion['notification']['event'][3]['mail_template'] = 'global_newforum_notify';
$modversion['notification']['event'][3]['mail_subject'] = _MI_NEWBB_GLOBAL_NEWFORUM_NOTIFYSBJ;

$modversion['notification']['event'][4]['name'] = 'new_post';
$modversion['notification']['event'][4]['category'] = 'global';
$modversion['notification']['event'][4]['title'] = _MI_NEWBB_GLOBAL_NEWPOST_NOTIFY;
$modversion['notification']['event'][4]['caption'] = _MI_NEWBB_GLOBAL_NEWPOST_NOTIFYCAP;
$modversion['notification']['event'][4]['description'] = _MI_NEWBB_GLOBAL_NEWPOST_NOTIFYDSC;
$modversion['notification']['event'][4]['mail_template'] = 'global_newpost_notify';
$modversion['notification']['event'][4]['mail_subject'] = _MI_NEWBB_GLOBAL_NEWPOST_NOTIFYSBJ;

$modversion['notification']['event'][5]['name'] = 'new_post';
$modversion['notification']['event'][5]['category'] = 'forum';
$modversion['notification']['event'][5]['title'] = _MI_NEWBB_FORUM_NEWPOST_NOTIFY;
$modversion['notification']['event'][5]['caption'] = _MI_NEWBB_FORUM_NEWPOST_NOTIFYCAP;
$modversion['notification']['event'][5]['description'] = _MI_NEWBB_FORUM_NEWPOST_NOTIFYDSC;
$modversion['notification']['event'][5]['mail_template'] = 'forum_newpost_notify';
$modversion['notification']['event'][5]['mail_subject'] = _MI_NEWBB_FORUM_NEWPOST_NOTIFYSBJ;

$modversion['notification']['event'][6]['name'] = 'new_fullpost';
$modversion['notification']['event'][6]['category'] = 'global';
$modversion['notification']['event'][6]['admin_only'] = 1;
$modversion['notification']['event'][6]['title'] = _MI_NEWBB_GLOBAL_NEWFULLPOST_NOTIFY;
$modversion['notification']['event'][6]['caption'] = _MI_NEWBB_GLOBAL_NEWFULLPOST_NOTIFYCAP;
$modversion['notification']['event'][6]['description'] = _MI_NEWBB_GLOBAL_NEWFULLPOST_NOTIFYDSC;
$modversion['notification']['event'][6]['mail_template'] = 'global_newfullpost_notify';
$modversion['notification']['event'][6]['mail_subject'] = _MI_NEWBB_GLOBAL_NEWFULLPOST_NOTIFYSBJ;

?>