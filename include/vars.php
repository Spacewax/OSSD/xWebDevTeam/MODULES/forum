<?php
// $Id: vars.php,v 1.3 2004/05/29 17:11:49 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //

// You shouldn't have to change any of these
$forumUrl['root'] = XOOPS_URL."/modules/newbb/";
$forumUrl['admin'] = $forumUrl['root']."admin";
$forumUrl['image'] = $forumUrl['root']."images/imagesets";
$forumUrl['smilies'] = $forumUrl['root']."images/smiles";

$handle = opendir(XOOPS_ROOT_PATH.'/modules/newbb/images/imagesets/');
$setting=$xoopsModuleConfig['image_set'][0];
$i=0;
while (false !== ($file = readdir($handle))) {
  if (is_dir(XOOPS_ROOT_PATH.'/modules/newbb/images/imagesets/'.$file) && !preg_match("/^[.]{1,2}$/",$file) && strtolower($file) != 'cvs') {

    $dirlist[]=$file;
    $i++;

  }

}
closedir($handle);

if (is_dir(XOOPS_ROOT_PATH.'/modules/'. $xoopsModule->dirname() .'/images/imagesets/'.$dirlist[$setting].'/')) {
        $forumUrl['images']= $forumUrl['image']."/".$dirlist[$setting];
        $forumUrl['images_lang']=$forumUrl['images'].$xoopsConfig['language'].'/';
}
elseif (is_dir(XOOPS_ROOT_PATH.'/modules/'. $xoopsModule->dirname() .'/images/imagesets/'.$dirlist[$setting].'/english/')) {
	$forumUrl['images']= $forumUrl['image']."/".$dirlist[$setting];
        $forumUrl['images_lang']=$forumUrl['images']."english/";
}else {
	$forumUrl['images']= $forumUrl['image']."/".$dirlist[$setting];
        $forumUrl['images_lang']=$forumUrl['images'];
}
/* -- You shouldn't have to change anything after this point */
/* -- Images -- */
$forumImage['attachment'] = $forumUrl['images']."/attachment.gif";
$forumImage['clip'] = $forumUrl['images']."/clip.gif";
$forumImage['point'] = $forumUrl['images'].'/point.gif';
$forumImage['whosonline'] = $forumUrl['images']."/whosonline.gif";
$forumImage['moderators'] = $forumUrl['images'].'/moderators.gif';

$forumImage['post'] = $forumUrl['images']."/post.gif";
$forumImage['newposts_forum'] = $forumUrl['images']."/folder_new_big.gif";
$forumImage['folder_forum'] = $forumUrl['images']."/folder_big.gif";
$forumImage['locked_forum'] = $forumUrl['images']."/folder_locked_big.gif";
$forumImage['locked_forum_newposts'] = $forumUrl['images']."/folder_locked_big_newposts.gif";
$forumImage['folder_topic'] = $forumUrl['images']."/folder.gif";
$forumImage['hot_folder_topic'] = $forumUrl['images']."/hot_folder.gif";
$forumImage['newposts_topic'] = $forumUrl['images']."/red_folder.gif";
$forumImage['hot_newposts_topic'] = $forumUrl['images']."/hot_red_folder.gif";
$forumImage['locked_topic'] = $forumUrl['images']."/lock.gif";
$forumImage['locktopic'] = $forumUrl['images']."/lock_topic.gif";
$forumImage['deltopic'] = $forumUrl['images']."/del_topic.gif";
$forumImage['movetopic'] = $forumUrl['images']."/move_topic.gif";
$forumImage['unlocktopic'] = $forumUrl['images']."/unlock_topic.gif";
$forumImage['sticky'] = $forumUrl['images']."/sticky.gif";
$forumImage['unsticky'] = $forumUrl['images']."/unsticky.gif";
$forumImage['newtopic'] = $forumUrl['images']."/new_topic-dark.jpg";
$forumImage['folder_sticky'] = $forumUrl['images']."/folder_sticky.gif";
$forumImage['hot_user_folder_topic'] = $forumUrl['images']."/hot_user_folder.gif";
$forumImage['newposts_user_topic'] = $forumUrl['images']."/red_folder_user.gif";
$forumImage['folder_user_topic'] = $forumUrl['images']."/folder_user.gif";


$forumImage['up_white'] = $forumUrl['images']."/up_white.gif";
$forumImage['down_white'] = $forumUrl['images']."/down_white.gif";
$forumImage['up_blue'] = $forumUrl['images']."/up_blue.gif";
$forumImage['down_blue'] = $forumUrl['images']."/down_blue.gif";
$forumImage['new_forum'] = $forumUrl['images']."/new_forum.gif";
$forumImage['new_subforum'] = $forumUrl['images']."/new_subforum.gif";
$forumImage['editicon'] = $forumUrl['images']."/editicon.gif";
$forumImage['private'] = $forumUrl['images']."/private.gif";
$forumImage['new_forum']    = $forumUrl['images']."/new_forum.gif";
$forumImage['new_subforum'] = $forumUrl['images']."/new_subforum.gif";

$forumImage['admin_delete'] = $forumUrl['images_lang']."/admin_delete.gif";
$forumImage['admin_move'] = $forumUrl['images_lang']."/admin_move.gif";
$forumImage['admin_annouce'] = $forumUrl['images_lang']."/admin_annouce.gif";
$forumImage['admin_unannouce'] = $forumUrl['images_lang']."/admin_unannouce.gif";
$forumImage['admin_sticky'] = $forumUrl['images_lang']."/admin_sticky.gif";
$forumImage['admin_unsticky'] = $forumUrl['images_lang']."/admin_unsticky.gif";
$forumImage['admin_lock'] = $forumUrl['images_lang']."/admin_lock.gif";
$forumImage['admin_unlock'] = $forumUrl['images_lang']."/admin_unlock.gif";
$forumImage['admin_digest'] = $forumUrl['images_lang']."/admin_digest.gif";
$forumImage['admin_undigest'] = $forumUrl['images_lang']."/admin_undigest.gif";
$forumImage['admin_point'] = $forumUrl['images_lang']."/admin_point.gif";
/* -- Cookie settings (lastvisit) -- */
// Most likely you can leave this be, however if you have problems
// logging into the forum set this to your domain name, without
// the http://
// For example, if your forum is at http://www.mysite.com/phpBB then
// set this value to
// $forumCookie['domain'] = "www.mysite.com";
$forumCookie['domain'] = "";

// It should be safe to leave these alone as well.
$forumCookie['path'] = str_replace(basename($_SERVER['PHP_SELF']),"",$_SERVER['PHP_SELF']);
$forumCookie['secure'] = false;
// set expire dates: one for a year, one for 10 minutes
$expiredate1 = time() + 3600 * 24 * 365;
$expiredate2 = time() + 600;

// update LastVisit cookie. This cookie is updated each time auth.php runs
setcookie("NEWBBLastVisit", time(), $expiredate1,  $forumCookie['path'], $forumCookie['domain'], $forumCookie['secure']);

// set LastVisitTemp cookie, which only gets the time from the LastVisit
// cookie if it does not exist yet
// otherwise, it gets the time from the LastVisitTemp cookie
if (!isset($_COOKIE["NEWBBLastVisitTemp"])) {
    if(isset($_COOKIE["NEWBBLastVisit"])){
        $temptime = $_COOKIE["NEWBBLastVisit"];
    }else{
        $temptime = time();
    }
}
else {
    $temptime = $_COOKIE["NEWBBLastVisitTemp"];
}

// set cookie.
setcookie("NEWBBLastVisitTemp", $temptime ,$expiredate2, $forumCookie['path'], $forumCookie['domain'], $forumCookie['secure']);
$last_visit = $temptime;

?>