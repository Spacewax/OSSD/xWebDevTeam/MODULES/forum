<?php
// $Id: functions.php,v 1.1 2004/05/29 17:21:29 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //

function get_total_users()
{
	global $xoopsDB;
	
	$sql = "SELECT COUNT(*) AS total FROM ".$xoopsDB->prefix("users")."";
	
	if ( !$result = $xoopsDB->query($sql) ) {
		return _MD_ERROR;
	}
	
	if ( !$myrow = $xoopsDB->fetchArray($result) ) {
		return _MD_ERROR;
	}
	
	return $myrow['total'];
}

/*
* Gets the total number of topics in a form
*/
function get_total_topics($forum_id="")
{
	global $xoopsDB;
	if ( $forum_id ) {
		$sql = "SELECT COUNT(*) AS total FROM ".$xoopsDB->prefix("bb_topics")." WHERE forum_id = $forum_id";
	} else {
		$sql = "SELECT COUNT(*) AS total FROM ".$xoopsDB->prefix("bb_topics");
	}
	if ( !$result = $xoopsDB->query($sql) ) {
		return _MD_ERROR;
	}
	
	if ( !$myrow = $xoopsDB->fetchArray($result) ) {
		return _MD_ERROR;
	}
	
	return $myrow['total'];
}

/*
* Returns the total number of posts in the whole system, a forum, or a topic
* Also can return the number of users on the system.
*/
function get_total_posts($id, $type)
{
	global $xoopsDB;
	switch ( $type ) {
		case 'users':
		$sql = "SELECT COUNT(*) AS total FROM ".$xoopsDB->prefix("users")." WHERE (uid > 0) AND ( level >0 )";
		break;
		case 'all':
		$sql = "SELECT COUNT(*) AS total FROM ".$xoopsDB->prefix("bb_posts");
		break;
		case 'forum':
		$sql = "SELECT COUNT(*) AS total FROM ".$xoopsDB->prefix("bb_posts")." WHERE forum_id = $id";
		break;
		case 'topic':
		$sql = "SELECT COUNT(*) AS total FROM ".$xoopsDB->prefix("bb_posts")." WHERE topic_id = $id";
		break;
		// Old, we should never get this.
		case 'user':
		exit("Should be using the users.user_posts column for this.");
	}
	if ( !$result = $xoopsDB->query($sql) ) {
		return "ERROR";
	}
	if ( !$myrow = $xoopsDB->fetchArray($result) ) {
		return 0;
	}
	return $myrow['total'];
}

function get_total_views()
{
	global $xoopsDB;
	$total="";
	
	$sql = "SELECT topic_views  FROM ".$xoopsDB->prefix("bb_topics")."";
	
	if ( !$result = $xoopsDB->query($sql) ) {
		return _MD_ERROR;
	}
	
	
	while ($arr = $xoopsDB->fetchArray($result)) {
		$total=$total+$arr['topic_views'];
	}
	
	return $total;
}

/*
* Returns the most recent post in a forum, or a topic
*/
function get_last_post($id, $type)
{
	global $xoopsDB;
	switch ( $type ) {
		case 'time_fix':
		$sql = "SELECT post_time FROM ".$xoopsDB->prefix("bb_posts")." WHERE topic_id = $id ORDER BY post_time DESC";
		break;
		case 'forum':
		$sql = "SELECT p.post_time, p.uid, u.uname FROM ".$xoopsDB->prefix("bb_posts")." p, ".$xoopsDB->prefix("users")." u WHERE p.forum_id = $id AND p.uid = u.uid ORDER BY post_time DESC";
		break;
		case 'topic':
		$sql = "SELECT p.post_time, u.uname FROM ".$xoopsDB->prefix("bb_posts")." p, ".$xoopsDB->prefix("users")." u WHERE p.topic_id = $id AND p.uid = u.uid ORDER BY post_time DESC";
		break;
		case 'user':
		$sql = "SELECT post_time FROM ".$xoopsDB->prefix("bb_posts")." WHERE uid = $id";
		break;
	}
	if ( !$result = $xoopsDB->query($sql,1,0) ) {
		return _MD_ERROR;
	}
	if ( !$myrow = $xoopsDB->fetchArray($result) ) {
		return _MD_NOPOSTS;
	}
	if ( ($type != 'user') && ($type != 'time_fix') ) {
		$val = sprintf("%s <br /> %s %s", $myrow['post_time'], _MD_BY, $myrow['uname']);
	} else {
		$val = $myrow['post_time'];
	}
	return $val;
}

/*
* Returns an array of all the moderators of a forum with a link
*/
function get_moderators($forum_id)
{
	global $xoopsDB, $myts;
	$forum_moderators = "";
	$count = 0;
	
	$sql_mod = "SELECT forum_moderator FROM ".$xoopsDB->prefix("bb_forums")." WHERE forum_id = $forum_id ";
	if ( !$result_mod = $xoopsDB->query($sql_mod) ) {
		return $forum_moderators;
	}
	if ( !$myrow_mod = $xoopsDB->fetchArray($result_mod) ) {
		return $forum_moderators;
	}
	
	$moderator = explode(" ", $myrow_mod['forum_moderator']);
	for ($i=0; $i<count($moderator); $i++)
	{
		$sql = "SELECT uid, uname FROM ".$xoopsDB->prefix("users")." WHERE uid = $moderator[$i]";
		if ( !$result = $xoopsDB->query($sql) ) {
			return $forum_moderators;
		}
		if ( !$myrow = $xoopsDB->fetchArray($result) ) {
			return $forum_moderators;
		}
		$array[] = array($myrow['uid'] => $myrow['uname']);
	}
	
	foreach ( $array as $mods ) {
		foreach ( $mods as $mod_id => $mod_name ) {
			if ( $count > 0 ) {
				$forum_moderators .= ", ";
			}
			$forum_moderators .=  '<a href="'.XOOPS_URL.'/userinfo.php?uid='.$mod_id.'">'.$myts->htmlSpecialChars($mod_name).'</a>';
			$count = 1;
		}
	}
	
	return $forum_moderators;
}

/**
* Checks the groupname.
* If found gives the groupname back.
*/
function GetGroupsNames ($uid)
{
	$g='';
	$member_handler =& xoops_gethandler('member');
	$User= new XoopsUser($uid);
	$user_groups=$User->getGroups();
	$count = count($user_groups);
	for ($i=0;$i<$count;$i++)
	{
		$thisgroup =& $member_handler->getGroup($user_groups[$i]);
		$g.=$thisgroup->getVar('name')." <br>";
	}
	return ($g);
}

/*
* Checks if a topic is locked
*/
function is_locked($topic)
{
	global $xoopsDB;
	$ret = false;
	$sql = "SELECT topic_status FROM ".$xoopsDB->prefix("bb_topics")." WHERE topic_id = $topic";
	if ( $r = $xoopsDB->query($sql) ) {
		if ( $m = $xoopsDB->fetchArray($r) ) {
			if ( $m['topic_status'] == 1 ) {
				$ret = true;
			}
		}
	}
	return $ret;
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function make_jumpbox() {
	global $xoopsDB, $xoopsUser, $myts, $xoopsModule;
	
	include_once(XOOPS_ROOT_PATH . "/modules/" . $xoopsModule->dirname() . "/class/class.permission.php");
	
	$box ='<form action="viewforum.php" method="get">';
	$box .='<select class="select" name="forum"><option value="-1"><b>-- '._MD_SELFORUM.' --</b></option>';
	
	$sql = "SELECT cat_id, cat_groupid, cat_state, cat_title FROM ".$xoopsDB->prefix('bb_categories')." ORDER BY cat_order";
	
	if ($result = $xoopsDB->query($sql)) {
		$myrow = $xoopsDB->fetchArray($result);
		$catperm = new Permissions();
		$myrow['cat_title'] = $myts->htmlSpecialChars($myrow['cat_title']);
		do {
			
			$permissions = $catperm->checkPermission($myrow['cat_groupid'], $myrow['cat_state']);
			
			if (!$permissions){
				continue;
			}
			$box .= "
                <option value='-1'>&nbsp;</option>
                <option value='-1'>".$myrow['cat_title']."</option>";
			$sub_sql = "SELECT forum_id, forum_name FROM ".$xoopsDB->prefix('bb_forums')." WHERE cat_id ='$myrow[cat_id]' ORDER BY forum_id";
			if ($res = $xoopsDB->query($sub_sql)) {
				while (list($forum_id, $forum_name) = $xoopsDB->fetchRow($res))
				{
					$perm = new Permissions();
					$permissions = $perm->get_forum_auth($forum_id);
					if ($permissions['can_view'] == 0)
					{
						continue;
					}
					$name = $myts->htmlSpecialChars($forum_name);
					$box .= "<option value='".$forum_id."'>-- $name</option>";
				}
			} else {
				echo "<option value='0'>Error Connecting to DB</option>";
			}
		} while ($myrow = $xoopsDB->fetchArray($result));
		
	} else {
		$box .= "<option value='-1'>ERROR</option>";
	}
	
	$box .= "</select> <input type='submit' class='button' value='"._GO."' /></form>";
	return $box;
}

function build_rss()
{
	global $xoopsDB, $xoopsModuleConfig, $xoopsModule, $myts;
	
	include (XOOPS_ROOT_PATH . "/modules/" . $xoopsModule->dirname() . "/class/class.xml-rss.php");
	if ($xoopsModuleConfig['rss_enable'] == 1)
	{
		$sql= "SELECT t.* FROM ".$xoopsDB->prefix('bb_topics')." AS t, ".$xoopsDB->prefix('bb_group_access')." AS g WHERE t.forum_id=g.forum_id AND g.group_id=3 ORDER BY t.topic_last_post_id DESC";
		$query = $xoopsDB->query($sql, $xoopsModuleConfig['rss_maxitems']);
		if ($query)
		{
			$rss = new xml_rss(XOOPS_ROOT_PATH . '/cache/forum.xml');
			$rss->channel_title .= " :: "._MD_FORUM;
			$rss->image_title   .= " :: "._MD_FORUM;
			$rss->max_items            = $xoopsModuleConfig['rss_maxitems'];
			$rss->max_item_description = $xoopsModuleConfig['rss_maxdescription'];
			
			while ( $row = $xoopsDB->fetchArray($query) )
			{
				$link = XOOPS_URL . 'viewtopic.php?topic_id=' . $row['topic_id'] . '&amp;forum=' . $row['forum_id'];
				$title = $myts->htmlSpecialChars($row['topic_title']);
				$description = '';
				
				$sql = 'SELECT p.*, t.post_text FROM '.$xoopsDB->prefix('bb_posts').' p, '.$xoopsDB->prefix('bb_posts_text')." t WHERE p.topic_id=".$row['topic_id']." AND p.post_id = t.post_id ORDER BY p.post_id";
				if($res = $xoopsDB->query($sql))
				{
					if($post_row = $xoopsDB->fetchArray($res))
					{
						$description .= "\"".$post_row['post_text']."\"";
						$description .= " <BR> "._MD_BY." ";
						$description .= XoopsUser::getUnameFromID($post_row['uid']);
						$description .= " (".formatTimestamp($post_row['post_time']).")";
					}
				}
				
				$rss->build($title, $link, $description);
			}
			$rss->save();
		}
	}
}

function saveNewbbAccess($grps) {
	
	if ( is_array($grps) ) { $grps = implode(" ", $grps); }
	return($grps);
}


function sync($id, $type)
{
	global $xoopsDB;
	switch ( $type ) {
		case 'forum':
		$sql = "SELECT MAX(post_id) AS last_post FROM ".$xoopsDB->prefix("bb_posts")." WHERE forum_id = $id";
		if ( !$result = $xoopsDB->query($sql) ) {
			exit("Could not get post ID");
		}
		if ( $row = $xoopsDB->fetchArray($result) ) {
			$last_post = $row['last_post'];
		}
		
		$sql = "SELECT COUNT(post_id) AS total FROM ".$xoopsDB->prefix("bb_posts")." WHERE forum_id = $id";
		if ( !$result = $xoopsDB->query($sql) ) {
			exit("Could not get post count");
		}
		if ( $row = $xoopsDB->fetchArray($result) ) {
			$total_posts = $row['total'];
		}
		
		$sql = "SELECT COUNT(topic_id) AS total FROM ".$xoopsDB->prefix("bb_topics")." WHERE forum_id = $id";
		if ( !$result = $xoopsDB->query($sql) ) {
			exit("Could not get topic count");
		}
		if ( $row = $xoopsDB->fetchArray($result) ) {
			$total_topics = $row['total'];
		}
		
		$sql = sprintf("UPDATE %s SET forum_last_post_id = %u, forum_posts = %u, forum_topics = %u WHERE forum_id = %u", $xoopsDB->prefix("bb_forums"), $last_post, $total_posts, $total_topics, $id);
		if ( !$result = $xoopsDB->queryF($sql) ) {
			exit("Could not update forum $id");
		}
		break;
		case 'topic':
		$sql = "SELECT max(post_id) AS last_post FROM ".$xoopsDB->prefix("bb_posts")." WHERE topic_id = $id";
		if ( !$result = $xoopsDB->query($sql) ) {
			exit("Could not get post ID");
		}
		if ( $row = $xoopsDB->fetchArray($result) ) {
			$last_post = $row['last_post'];
		}
		if ( $last_post > 0 ) {
			$sql = "SELECT COUNT(post_id) AS total FROM ".$xoopsDB->prefix("bb_posts")." WHERE topic_id = $id";
			if ( !$result = $xoopsDB->query($sql) ) {
				exit("Could not get post count");
			}
			if ( $row = $xoopsDB->fetchArray($result) ) {
				$total_posts = $row['total'];
			}
			$total_posts -= 1;
			$sql = sprintf("UPDATE %s SET topic_replies = %u, topic_last_post_id = %u WHERE topic_id = %u", $xoopsDB->prefix("bb_topics"), $total_posts, $last_post, $id);
			if ( !$result = $xoopsDB->queryF($sql) ) {
				exit("Could not update topic $id");
			}
		}
		break;
		case 'all forums':
		$sql = "SELECT forum_id FROM ".$xoopsDB->prefix("bb_forums");
		if ( !$result = $xoopsDB->query($sql) ) {
			exit("Could not get forum IDs");
		}
		while ( $row = $xoopsDB->fetchArray($result) ) {
			$id = $row['forum_id'];
			sync($id, "forum");
		}
		break;
		case 'all topics':
		$sql = "SELECT topic_id FROM ".$xoopsDB->prefix("bb_topics");
		if ( !$result = $xoopsDB->query($sql) ) {
			exit("Could not get topic ID's");
		}
		while ( $row = $xoopsDB->fetchArray($result) ) {
			$id = $row['topic_id'];
			sync($id, "topic");
		}
		break;
	}
	return true;
}

// for private forum queries returns 0 for anonymous users
function forum_user() {
	global $xoopsUser;
	if ( $xoopsUser ) {
		$forum_user = $xoopsUser->getVar("uid");
	}
	else $forum_user = 0;
	
	return $forum_user;
}

function getLinkedUnameFromId($userid = 0, $name= 0)
{
	if (!is_numeric($userid)) {
		return $userid;
	}
	
	$userid = intval($userid);
	if ($userid > 0) {
		$member_handler =& xoops_gethandler('member');
		$user =& $member_handler->getUser($userid);
		
		if (is_object($user)) {
			$ts =& MyTextSanitizer::getInstance();
			$username = $user->getVar('uname');
			$usernameu = $user->getVar('name');
			
			if ( ($name) && !empty($usernameu))  {
				$username = $user->getVar('name');
			}
			if ( !empty($usernameu)) {
				$linkeduser = "<a href='".XOOPS_URL."/userinfo.php?uid=".$userid."'>". $ts->htmlSpecialChars($username) ."</a>";
			}
			else {
				$linkeduser = "<a href='".XOOPS_URL."/userinfo.php?uid=".$userid."'>". ucwords($ts->htmlSpecialChars($username)) ."</a>";
			}
			return $linkeduser;
		}
	}
	return $GLOBALS['xoopsConfig']['anonymous'];
}

function getGroupInClause() {
	global $xoopsUser;
	$member_handler =& xoops_gethandler('member');
	
	// create the argument to a sql in as in
	// column in ( value1, value2, ...)
	// with a list of the current users groups
	if ( $xoopsUser ) {
		$groupids = $member_handler->getGroupsByUser($xoopsUser->getVar("uid"));
		if (count($groupids) >= 1) {
			$size = count($groupids);
			$in_clause = '(';
			for ($i = 0; $i < $size; $i++) {
				if ($i > 0) $in_clause .= ", ";
				$in_clause .= $groupids[$i];
			}
			$in_clause .= ")";
		}
	}
	else $in_clause = '';
	
	return $in_clause;
}

function subforum($forum)
{
	global $xoopsConfig, $xoopsDB, $xoopsUser, $xoopsModule, $xoopsTpl, $myts, $forumImage, $HTTP_COOKIE_VARS;
	
	include_once(XOOPS_ROOT_PATH . "/modules/" . $xoopsModule->dirname() . "/class/class.permission.php");
	
	$sql = 'SELECT f.*, u.uname, u.uid, p.topic_id, p.post_time, p.subject, p.icon FROM '.$xoopsDB->prefix('bb_forums').' f LEFT JOIN '.$xoopsDB->prefix('bb_posts').' p ON p.post_id = f.forum_last_post_id LEFT JOIN '.$xoopsDB->prefix('users').' u ON u.uid = p.uid';
	
	$sql .= ' WHERE f.parent_forum = '.$forum.' ORDER BY f.parent_forum, f.forum_order';
	$xoopsTpl->assign('forum_index_title', sprintf(_MD_FORUMINDEX,$xoopsConfig['sitename']));
	
	if ( !$result = $xoopsDB->query($sql) ) {
		exit("Error");
	}
	
	// Read 'lastread' cookie, if exists
	$topic_lastread = !empty($HTTP_COOKIE_VARS['newbb_topic_lastread']) ? unserialize($HTTP_COOKIE_VARS['newbb_topic_lastread']) : array();
	// Read 'lastview' cookie, if exists
	$forum_lastview = !empty($HTTP_COOKIE_VARS['newbb_forum_lastview']) ? unserialize($HTTP_COOKIE_VARS['newbb_forum_lastview']): array();
	
	$forums = array(); // RMV-FIX
	while ( $forum_data = $xoopsDB->fetchArray($result) ) {
		if ($forum_data['post_time']) {
			$forum_lastpost_time = formatTimestamp($forum_data['post_time']);
			$last_post_icon = '<a href="'.XOOPS_URL.'/modules/' . $xoopsModule->dirname() . '/viewtopic.php?post_id='.$forum_data['forum_last_post_id'].'&amp;topic_id='.$forum_data['topic_id'].'&amp;forum='.$forum_data['forum_id'].'#forumpost'.$forum_data['forum_last_post_id'].'">';
			if ( $forum_data['icon'] ) {
				$last_post_icon .= '<img src="'.XOOPS_URL.'/images/subject/'.$forum_data['icon'].'" border="0" alt="" />';
			} else {
				$last_post_icon .= '<img src="'.XOOPS_URL.'/images/subject/icon1.gif" width="15" height="15" border="0" alt="" />';
			}
			$last_post_icon .= '</a>';
			$forum_lastpost_icon = $last_post_icon;
			if ( $forum_data['uid'] != 0 && $forum_data['uname'] ){
				$forum_lastpost_user = ''._MD_BY.'&nbsp;<a href="'.XOOPS_URL.'/userinfo.php?uid='.$forum_data['uid'].'">' . $myts->htmlSpecialChars($forum_data['uname']).'</a>';
			} else {
				$forum_lastpost_user = $xoopsConfig['anonymous'];
			}
			$forum_lastread = !empty($topic_lastread[$forum_data['topic_id']]) ? $topic_lastread[$forum_data['topic_id']] : false;
			if (isset($forum_lastview[$forum_data['forum_id']]) and $forum_lastview[$forum_data['forum_id']]>$forum_data['post_time'])
			{
				if ( $forum_data['forum_type'] == 1 )
				{
					$forum_folder = $forumImage['locked_forum'];
				}else{$forum_folder = $forumImage['folder_forum'];}
			}
			elseif ( $forum_data['post_time'] > $forum_lastread && !empty($forum_data['topic_id'])) {
				if ( $forum_data['forum_type'] == 1 )
				{
					$forum_folder = $forumImage['locked_forum_newposts'];
				}else{$forum_folder = $forumImage['newposts_forum'];}
			} else {
				if ( $forum_data['forum_type'] == 1 )
				{
					$forum_folder = $forumImage['locked_forum'];
				}else{$forum_folder = $forumImage['folder_forum'];}
			}
		} else {
			// no forums, so put empty values
			$forum_lastpost_time = "";
			$forum_lastpost_icon = "";
			$forum_lastpost_user = _MD_NOPOSTS;
			if ( $forum_data['forum_type'] == 1 ) {
				$forum_folder = $forumImage['locked_forum'];
			} else {
				$forum_folder = $forumImage['folder_forum'];
			}
		}
		$perms = new Permissions();
		$forumpermissions = $perms->checkPermission($forum_data['forum_access'], $forum_data['forum_type'], $forum_data['forum_moderator']);
		
		$forums = array( 'forum_id' => $forum_data['forum_id'],
		'forum_name' => $forum_data['forum_name'],
		'forum_desc' => $forum_data['forum_desc'],
		'forum_posts' => $forum_data['forum_posts'],
		'forum_topics' => $forum_data['forum_topics'],
		'forum_type' => $forum_data['forum_type'],
		'forum_lastpost_time' => $forum_lastpost_time,
		'forum_lastpost_icon' => $forum_lastpost_icon,
		'forum_lastpost_user' => $forum_lastpost_user,
		'forum_folder' => $forum_folder,
		'forum_moderators' => get_moderators($forum_data['forum_id']),
		'forum_permission' => $forumpermissions
		
		);
		$xoopsTpl->append("subforum", $forums);
	}
}

function get_user_level($uid)
{
	global $xoopsDB, $xoopsUser;
	
	$user = new XoopsUser($uid);
	
	$RPG = $user->getVar('posts');
	$RPGDIFF = $user->getVar('user_regdate');
	
	$today = time();
	$diff = $today - $RPGDIFF;
	$exp = round($diff / 86400,0);
	if ($exp<=0) { $exp = 1; }
	$ppd= round($RPG / $exp, 0);
	$level = pow (log10 ($RPG), 3);
	$ep = floor (100 * ($level - floor ($level)));
	$showlevel = floor ($level + 1);
	$hpmulti =round ($ppd / 6, 1);
	if ($hpmulti > 1.5) { $hpmulti = 1.5; }
	if ($hpmulti < 1) { $hpmulti = 1; }
	$maxhp = $level * 25 * $hpmulti;
	$hp= $ppd / 5;
	if ($hp >= 1) {
		$hp= $maxhp;
	} else {
		$hp= floor ($hp * $maxhp);
	}
	$hp= floor ($hp);
	$maxhp= floor ($maxhp);
	if ($maxhp <= 0) {
		$zhp = 1;
	} else {
		$zhp = $maxhp;
	}
	$hpf= floor (100 * ($hp / $zhp)) - 1;
	$maxmp= ($exp * $level) / 5;
	$mp= $RPG / 3;
	if ($mp >= $maxmp) { $mp = $maxmp; }
	$maxmp = floor ($maxmp);
	$mp = floor ($mp);
	if ($maxmp <= 0) {
		$zmp = 1;
	} else {
		$zmp = $maxmp;
	}
	$mpf= floor (100 * ($mp / $zmp)) - 1;
	if ( $hpf >= 98 ) { $hpf = $hpf - 2; }
	if ( $ep >= 98 ) { $ep = $ep - 2; }
	if ( $mpf >= 98 ) { $mpf = $mpf - 2; }
	
	$level = array();
	$level['LEVEL']  = $showlevel ;
	$level['EXP'] = $ep;
	$level['HP']  = $hp;
	$level['HP_MAX']  = $maxhp;
	$level['HP_WIDTH'] = $hpf;
	$level['MP']  = $mp;
	$level['MP_MAX']  = $maxmp;
	$level['MP_WIDTH'] = $mpf;
	
	return $level;
}

function adminmenu($currentoption=0,$breadcrumb)
{
	global $xoopsModule, $xoopsConfig;
	$tblColors=Array();
	$tblColors[0]=$tblColors[1]=$tblColors[2]=$tblColors[3]=$tblColors[4]=$tblColors[5]=$tblColors[6]=$tblColors[7]='#DDE';
	$tblColors[$currentoption]='white';
	
	// Breadcrumb
	echo "<table width='100%' cellspacing=0 cellpadding=0 border=0 class = outer>";
	echo "<tr>";
	echo "<td width='300' style=\"font-size: 10px; text-align: left; color: #2F5376; padding: 2px 6px; line-height: 18px; \">";
	echo "<a href=\"../../system/admin.php?fct=preferences&amp;op=showmod&amp;mod=".$xoopsModule -> getVar( 'mid' )."\">"._MD_A_GENERALSET ."</a> | ";
	echo "<a href=\"../index.php\">"._MD_A_GOTOMOD."</a> | ";
	echo "<a href=\"../help/smartfaq.html\">"._MD_A_HELP."</a> | ";
	echo "<a href=\"about.php\">"._MD_A_ABOUT."</a>";
	echo "</td>";
	echo "<td style=\"font-size: 10px; text-align: right; color: #2F5376; padding: 2px 6px; line-height: 18px; \"><b>".$xoopsModule->name()." "._MD_A_MODULEADMIN."</b> ".$breadcrumb."</td>";
	echo "</tr>";
	
	echo "</table>";
	
	echo "<div id=\"navcontainer\"><ul style=\"padding: 3px 0; margin-left: 0; font: bold 12px Verdana, sans-serif; \">";
	echo "<li style=\"list-style: none; margin: 0; display: inline; \">
    <a href=\"index.php\" style=\"padding: 3px 0.5em; margin-left: 3px; border: 1px solid #778; background: ".$tblColors[0].";
     text-decoration: none; \">Index</a></li>";
	echo "<li style=\"list-style: none; margin: 0; display: inline; \">
     <a href=\"admin_forum_manager.php?op=manage\" style=\"padding: 3px 0.5em; margin-left: 3px; border: 1px solid #778; background: ".$tblColors[1].";
     text-decoration: none; \">"._MD_A_FORUM_MANAGER ."</a></li>";
	echo "<li style=\"list-style: none; margin: 0; display: inline; \">
    <a href=\"admin_cat_manager.php?op=manage\" style=\"padding: 3px 0.5em; margin-left: 3px; border: 1px solid #778; background: ".$tblColors[2].";
     text-decoration: none; \">"._MD_A_CATADMIN."</a></li>";
	echo "<li style=\"list-style: none; margin: 0; display: inline; \">
    <a href=\"admin_forum_manager.php?op=sync\" style=\"padding: 3px 0.5em; margin-left: 3px; border: 1px solid #778; background: ".$tblColors[3].";
     text-decoration: none; \">"._MD_A_SYNCFORUM."</a></li>";
	echo "<li style=\"list-style: none; margin: 0; display: inline; \">
    <a href=\"admin_forum_reorder.php\" style=\"padding: 3px 0.5em; margin-left: 3px; border: 1px solid #778; background: ".$tblColors[4].";
     text-decoration: none; \">". _MD_A_SETFORUMORDER ."</a></li></div></ul>";
	
	echo "<div id=\"navcontainer\"><ul style=\"padding: 3px 0; margin-left: 0; font: bold 12px Verdana, sans-serif; \">";
	echo "<li style=\"list-style: none; margin: 0; display: inline; \">
    <a href=\"admin_forum_prune.php\" style=\"padding: 3px 0.5em; margin-left: 3px; border: 1px solid #778; background: ".$tblColors[5].";
     text-decoration: none; \">"._MD_A_PRUNE_TITLE."</a></li>";
	echo "<li style=\"list-style: none; margin: 0; display: inline; \">
    <a href=\"admin_mod_user.php\" style=\"padding: 3px 0.5em; margin-left: 3px; border: 1px solid #778; background: ".$tblColors[6].";
     text-decoration: none; \">"._MD_A_MODERATEUSER."</a></li>";
	echo "<li style=\"list-style: none; margin: 0; display: inline; \">
    <a href=\"myblocksadmin.php\" style=\"padding: 3px 0.5em; margin-left: 3px; border: 1px solid #778; background: ".$tblColors[7].";
     text-decoration: none; \">"._MD_A_BLOCKS."</a></li>";
	
	
	echo "</div></ul>";
	echo "<br><br>";
	
}


?>