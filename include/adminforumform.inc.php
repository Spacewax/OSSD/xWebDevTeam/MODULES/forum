<?php
// $Id: adminforumform.inc.php,v 1.3 2004/05/29 17:11:49 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //


if (!defined('XOOPS_ROOT_PATH')) {
	exit();
}
include_once XOOPS_ROOT_PATH."/class/xoopslists.php";
include_once XOOPS_ROOT_PATH."/include/xoopscodes.php";
include_once XOOPS_ROOT_PATH . '/modules/' . $xoopsModule->dirname() . '/class/class.permission.php';

echo "<form action='index.php' method='post' name='forumform' id='forumform' onsubmit='return xoopsValidate(\"subject\", \"message\", \"contents_submit\", \"".htmlspecialchars(_PLZCOMPLETE, ENT_QUOTES)."\", \"".htmlspecialchars(_MESSAGETOOLONG, ENT_QUOTES)."\", \"".htmlspecialchars(_ALLOWEDCHAR, ENT_QUOTES)."\", \"".htmlspecialchars(_CURRCHAR, ENT_QUOTES)."\"); 'enctype='multipart/form-data'><table cellspacing='1' class='outer' width='100%'><tr>";

echo "</td></tr>";
echo "<tr>
<td class='head' valign='top' nowrap='nowrap'>". _MD_A_SUBJECTC ."</td>
<td class='odd'>";
echo "<input type='text' id='subject' name='subject' size='60' maxlength='100' value='$subject' /></td></tr>
<tr>
<td class='head' valign='top' nowrap='nowrap'>". _MD_A_MESSAGEICON ."</td>
<td class='even'>
";

$lists = new XoopsLists;
$filelist = $lists->getSubjectsList();
$count = 1;
while ( list($key, $file) = each($filelist) ) {
	$checked = "";
	if ( isset($icon) && $file==$icon ) {
		$checked = " checked='checked'";
	}
	echo "<input type='radio' value='$file' name='icon'$checked />&nbsp;";
	echo "<img src='".XOOPS_URL."/images/subject/$file' alt='' />&nbsp;";
	if ( $count == 8 ) {
		echo "<br />";
		$count = 0;
	}
	$count++;
}

echo "</td></tr>
<tr align='left'>
<td class='head' valign='top' nowrap='nowrap'>". _MD_A_MESSAGEC ."
</td>
<td class='odd'>";
xoopsCodeTarea("message");
xoopsSmilies("message");

echo "</td></tr>";

	$post_id = isset($post_id) ? intval($post_id) : '';
	$topic_id = isset($topic_id) ? intval($topic_id) : '';
	$order = isset($order) ? intval($order) : '';
	$pid = isset($pid) ? intval($pid) : 0;
	echo "<tr><td class='head'></td><td class='odd'>
	<input type='hidden' name='pid' value='".intval($pid)."' />
	<input type='hidden' name='post_id' value='".$post_id."' />
	<input type='hidden' name='topic_id' value='".$topic_id."' />
	<input type='hidden' name='forum' value='".intval($forum)."' />
	<input type='hidden' name='viewmode' value='$viewmode' />
	<input type='hidden' name='order' value='".$order."' />
	<input type='submit' name='contents_submit' class='formButton' id='contents_submit' value='"._SUBMIT."' />
	<input type='button' onclick='location=\"";
if ( isset($topic_id) && $topic_id != "" ) {
	echo "viewtopic.php?topic_id=".intval($topic_id)."&amp;forum=".intval($forum)."\"'";
} else {
	echo "viewforum.php?forum=".intval($forum)."\"'";
}
echo " class='formButton' value='"._MD_A_CANCELPOST."' />";

echo "</td></tr></table></form>\n";
?>

