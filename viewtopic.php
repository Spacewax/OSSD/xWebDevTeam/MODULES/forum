<?php
// $Id: viewtopic.php,v 1.8 2004/05/29 17:11:48 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //

include 'header.php';
include_once XOOPS_ROOT_PATH."/include/xoopscodes.php";
include_once(XOOPS_ROOT_PATH . "/modules/" . $xoopsModule->dirname() . "/class/class.permission.php");
include_once(XOOPS_ROOT_PATH . "/modules/" . $xoopsModule->dirname() . "/class/class.forumpost.php");

$forum = isset($_GET['forum']) ? intval($_GET['forum']) : 0;
$topic_id = isset($_GET['topic_id']) ? intval($_GET['topic_id']) : 0;
$topic_time = (isset($_GET['topic_time'])) ? intval($_GET['topic_time']) : 0;
$post_id = !empty($_GET['post_id']) ? intval($_GET['post_id']) : 0;

if (isset($_POST['message'])) $message = $_POST['message'];

if ( empty($forum) ) {
	redirect_header('index.php',2,_MD_ERRORFORUM);
	exit();
} elseif ( empty($topic_id) ) {
	redirect_header('viewforum.php?forum='.$forum,2,_MD_ERRORTOPIC);
	exit();
}

$sql = "SELECT
        t.topic_title,
        t.topic_status,
        t.topic_sticky,
        f.forum_name,
        f.allow_html,
        f.allow_sig,
        f.posts_per_page,
        f.hot_threshold,
        f.topics_per_page,
        f.allow_attachments,
        f.attach_maxkb,
        f.attach_ext,
        f.allow_polls,
        f.parent_forum
        FROM
        ".$xoopsDB->prefix('bb_topics')." t
        LEFT JOIN ".$xoopsDB->prefix('bb_forums')." f ON f.forum_id = t.forum_id
        WHERE
        t.topic_id = $topic_id
        AND
        t.forum_id = $forum";

if ( !$result = $xoopsDB->query($sql) ) {
        $error = "<h4>"._MD_ERROROCCURED."</h4><hr />"._MD_COULDNOTQUERY;
        redirect_header("index.php", 2, $error);
        exit();
}

if ( !$forumdata = $xoopsDB->fetchArray($result) ) {
        redirect_header("index.php", 2, _MD_FORUMNOEXIST);
        exit();
}

$perm = new Permissions();
$permission_set = $perm->get_forum_auth($forum);
if ($permission_set['can_view'] == 0)
{
        redirect_header("index.php", 2, _MD_NORIGHTTOACCESS);
        exit();
}
$isadmin = 0;
if ($xoopsUser) {
        if ( $xoopsUser->isAdmin($xoopsModule->mid()) || $perm->is_moderator($forum, $xoopsUser->uid())) {
                $isadmin = 1;
        }
}
$total_posts = get_total_posts($topic_id, "topic");

setcookie("bbtopic_time[$topic_id]", time());

if ( isset($post_id) && $post_id != "" ) {
        $forumpost = new ForumPosts($post_id);
        } else {
                $forumpost = new ForumPosts();
        }
//use users preferences
if (is_object($xoopsUser)) {
        $viewmode = $xoopsUser->getVar('umode');
        $order = ($xoopsUser->getVar('uorder') == 1) ? 'DESC' : 'ASC';
} else {
        $viewmode = 'flat';
        $order = 'ASC';
}
// newbb does not have nested mode
if ($viewmode == 'nest') {
        $viewmode = 'thread';
}
if ($xoopsModuleConfig['view_mode'] == 1)
{
   $viewmode = 'flat';
}
if ($xoopsModuleConfig['view_mode'] == 2)
{
   $viewmode = 'thread';
}

if ($order == 1) {
        $qorder = "post_time DESC";
        } else {
                $qorder = "post_time ASC";
        }

// override mode/order if any requested
if (isset($_GET['viewmode']) && ($_GET['viewmode'] == 'flat' || $_GET['viewmode'] == 'thread')) {
        $viewmode = $_GET['viewmode'];
}
if (isset($_GET['order']) && ($_GET['order'] == 'ASC' || $_GET['order'] == 'DESC')) {
        $order = $_GET['order'];
}

$forumpost->setOrder($qorder);

if ( !isset($post_id) || $post_id == "" ) {
        $pid=0;
        $forumpost->setTopicId($topic_id);
        $forumpost->setParent($pid);
}


if ($viewmode == "thread") {
        $xoopsOption['template_main'] =  'newbb_viewtopic_thread.html';
        $postsArray = $forumpost->getTopPosts();

       } else {
                $viewmode = "flat";
                $xoopsOption['template_main'] =  'newbb_viewtopic_flat.html';
                if ( isset($start) ) {
                        $postsArray = $forumpost->getAllPosts($topic_id, $order, $forumdata['posts_per_page'], $start,$post_id);
                        } else {
                                $postsArray = $forumpost->getAllPosts($topic_id, $order, $forumdata['posts_per_page'], $start);
                        }
        }

include XOOPS_ROOT_PATH.'/header.php';

if ( isset($_GET['move']) && 'next' == $_GET['move'] ) {
        $sql = 'SELECT t.topic_id, t.topic_title, t.topic_time, t.topic_status, t.topic_sticky, t.topic_last_post_id, f.forum_id, f.forum_name, f.parent_forum, f.forum_access, f.forum_type, f.allow_html, f.allow_sig, f.posts_per_page, f.hot_threshold, f.topics_per_page, f.allow_attachments, f.attach_maxkb, f.attach_ext FROM '.$xoopsDB->prefix('bb_topics').' t LEFT JOIN '.$xoopsDB->prefix('bb_forums').' f ON f.forum_id = t.forum_id WHERE t.topic_time > '.$topic_time.' AND t.forum_id = '.$forum.' ORDER BY t.topic_time ASC LIMIT 1';
} elseif ( isset($_GET['move']) && 'prev' == $_GET['move']) {
        $sql = 'SELECT t.topic_id, t.topic_title, t.topic_time, t.topic_status, t.topic_sticky, t.topic_last_post_id, f.forum_id, f.forum_name, f.parent_forum, f.forum_access, f.forum_type, f.allow_html, f.allow_sig, f.posts_per_page, f.hot_threshold, f.topics_per_page, f.allow_attachments, f.attach_maxkb, f.attach_ext FROM '.$xoopsDB->prefix('bb_topics').' t LEFT JOIN '.$xoopsDB->prefix('bb_forums').' f ON f.forum_id = t.forum_id WHERE t.topic_time < '.$topic_time.' AND t.forum_id = '.$forum.' ORDER BY t.topic_time DESC LIMIT 1';
} else {
        $sql = 'SELECT t.topic_id, t.topic_title, t.topic_time, t.topic_status, t.topic_sticky, t.topic_last_post_id, f.forum_id, f.forum_name, f.parent_forum, f.forum_access, f.forum_type, f.allow_html, f.allow_sig, f.posts_per_page, f.hot_threshold, f.topics_per_page, f.allow_attachments, f.attach_maxkb, f.attach_ext FROM '.$xoopsDB->prefix('bb_topics').' t LEFT JOIN '.$xoopsDB->prefix('bb_forums').' f ON f.forum_id = t.forum_id WHERE t.topic_id = '.$topic_id.' AND t.forum_id = '.$forum;
}

if ( !$result = $xoopsDB->query($sql) ) {
        redirect_header('viewforum.php?forum='.$forum,2,_MD_ERROROCCURED);
        exit();
}

if ( !$forumdata = $xoopsDB->fetchArray($result) ) {
        redirect_header('viewforum.php?forum='.$forum,2,_MD_FORUMNOEXIST);
        exit();
}

if($forumdata['parent_forum'] > 0)
{
        $q = "select forum_name from ".$xoopsDB->prefix('bb_forums')." WHERE forum_id=".$forumdata['parent_forum'];
        $row = $xoopsDB->fetchArray($xoopsDB->query($q));
        $xoopsTpl->assign(array('parent_forum' => $forumdata['parent_forum'], 'parent_name' => $row['forum_name']));

        $forumdata['topic_title'] = $myts->htmlSpecialChars($forumdata['topic_title']);$forumdata['forum_name'] = $myts->htmlSpecialChars($forumdata['forum_name']);
        $xoopsTpl->assign(array('topic_title' => '<a href="'.$forumUrl['root'].'viewtopic.php?viewmode='.$viewmode.'&amp;topic_id='.$topic_id.'&amp;forum='.$forum.'">'.$forumdata['topic_title'].'</a>', 'forum_name' => $forumdata['forum_name'], 'topic_time' => $forumdata['topic_time'], 'lang_nexttopic' => _MD_NEXTTOPIC, 'lang_prevtopic' => _MD_PREVTOPIC));

}

else
{
$forumdata['topic_title'] = $myts->htmlSpecialChars($forumdata['topic_title']);$forumdata['forum_name'] = $myts->htmlSpecialChars($forumdata['forum_name']);
$xoopsTpl->assign(array('topic_title' => '<a href="'.$forumUrl['root'].'viewtopic.php?viewmode='.$viewmode.'&amp;topic_id='.$topic_id.'&amp;forum='.$forum.'">'.$forumdata['topic_title'].'</a>', 'forum_name' => $forumdata['forum_name'], 'topic_time' => $forumdata['topic_time'], 'lang_nexttopic' => _MD_NEXTTOPIC, 'lang_prevtopic' => _MD_PREVTOPIC));
}

$xoopsTpl->assign('folder_topic', $forumImage['folder_topic']);

$xoopsTpl->assign('topic_id', $forumdata['topic_id']);
$topic_id = $forumdata['topic_id'];
$xoopsTpl->assign('forum_id', $forumdata['forum_id']);
$forum = $forumdata['forum_id'];

$show_reg = 0;

if ($order == 'DESC') {
	$xoopsTpl->assign(array('order_current' => 'DESC', 'order_other' => 'ASC', 'lang_order_other' => _OLDESTFIRST));
} else {
	$xoopsTpl->assign(array('order_current' => 'ASC', 'order_other' => 'DESC', 'lang_order_other' => _NEWESTFIRST));
}

// initialize the start number of select query
$start = !empty($_GET['start']) ? intval($_GET['start']) : 0;

$xoopsTpl->assign(array('lang_threaded' => _THREADED, 'lang_flat' => _FLAT));

if ( $permission_set['can_post'] == 1 ) {
	$xoopsTpl->assign(array('viewer_can_post' => true, 'forum_post_or_register' => "<a href=\"newtopic.php?forum=".$forum."\"><img src=\"".$forumImage['post']."\" alt=\""._MD_POSTNEW."\" /></a>"));
} else {
	$xoopsTpl->assign('viewer_can_post', false);
	if ( $show_reg == 1 ) {
		$xoopsTpl->assign('forum_post_or_register', '<a href="'.XOOPS_URL.'/user.php?xoops_redirect='.htmlspecialchars($xoopsRequestUri).'">'._MD_REGTOPOST.'</a>');
	} else {
		$xoopsTpl->assign('forum_post_or_register', '');
	}
}


if ($viewmode == "thread") {

        foreach ($postsArray as $eachpost) {
                $forumpost->setType($eachpost->type);

                $eachpost->showPost($isadmin, $permission_set, $forumdata['topic_status'], $forumdata['topic_sticky'], $forumdata['allow_sig']);

                //if not in the top page, show links
                if ( $forumpost->parent() ) {
                        $parentlink ="<div style='text-align:left'>";
                        $parentlink .="&nbsp;<a href='./viewtopic.php?forum=$forum&amp;topic_id=$topic_id&amp;viewmode=$viewmode&amp;order=$order'>"._MD_TOP."</a>&nbsp;|&nbsp;<a href='./viewtopic.php?forum=$forum&amp;topic_id=$topic_id&amp;post_id=".$forumpost->parent()."&amp;viewmode=".$viewmode."&amp;order=".$order."#".$forumpost->parent()."'>"._MD_PARENT."</a>";
                        $parentlink .="</div>";
                        $xoopsTpl->assign('parent_link', $parentlink);
                }

                $treeArray = $forumpost->getPostTree($eachpost->postid());
                if ( count($treeArray) > 0 ) {

                        $count = 0;
                        foreach ($treeArray as $treeItem) {

                                $tree = $treeItem->showTreeItem($viewmode, $order);
                                $xoopsTpl->append("topic_trees", array("post_id" => $tree['post_id'], "post_parent_id" => $tree['parent'], "post_date" => $tree['date'], "post_image" => $tree['icon'], "post_title" => $tree['subject'], "post_prefix" => $tree['prefix'], "poster" => $tree['poster']));
                                $count++;
                        }

                }
         }
}
if ($viewmode == "flat") {
        $xoopsTpl->assign(array('topic_viewmode' => 'flat', 'lang_top' => _MD_TOP, 'lang_subject' => _MD_SUBJECT, 'lang_bottom' => _MD_BOTTOM));
        $postsArray = ForumPosts::getAllPosts($topic_id, $order, $forumdata['posts_per_page'], $start, $post_id);

        foreach ($postsArray as $eachpost) {

                $forumpost->setType($eachpost->type);
                $eachpost->showPost($isadmin, $permission_set, $forumdata['topic_status'], $forumdata['topic_sticky'], $forumdata['allow_sig']);

        }


    if ( $forumdata['topic_status'] != 1 ) {
    $message='';
    $result = $xoopsDB->query("SELECT post_id FROM ".$xoopsDB->prefix("bb_posts")." WHERE topic_id=$topic_id ORDER BY post_id asc",1,0);
    list($post_id) = $xoopsDB->fetchRow($result);
    $xoopsTpl->assign('quickreply', array( 'show' => 1, 'subject' => $forumdata['topic_title'], 'pid' => $post_id, 'topic_id' => $topic_id, 'forum' => $forum, 'viewmode' => $viewmode, 'order' => $order ));
    ob_start();
    xoopsSmilies("message");
    $xoopsTpl->assign('xoops_smilies', ob_get_contents());
    ob_end_clean();
    ob_start();
    xoopsCodeTarea("message",75,10);
    $xoopsTpl->assign('xoops_codes', ob_get_contents());
    ob_end_clean();

      }
	if ( $total_posts > $forumdata['posts_per_page'] ) {
		include XOOPS_ROOT_PATH.'/class/pagenav.php';
		$nav = new XoopsPageNav($total_posts, $forumdata['posts_per_page'], $start, "start", 'topic_id='.$topic_id.'&amp;forum='.$forum.'&amp;viewmode='.$viewmode.'&amp;order='.$order);
		$xoopsTpl->assign('forum_page_nav', $nav->renderNav(4));
	} else {
		$xoopsTpl->assign('forum_page_nav', '');
	}
}

$xoopsTpl->assign('permission_table', permission_table($permission_set));
// create jump box
$xoopsTpl->assign(array('forum_jumpbox' => make_jumpbox($forum), 'lang_forum_index' => sprintf(_MD_FORUMINDEX,$xoopsConfig['sitename']), 'lang_from' => _MD_FROM, 'lang_joined' => _MD_JOINED, 'lang_posts' => _MD_POSTS, 'lang_poster' => _MD_POSTER, 'lang_thread' => _MD_THREAD, 'lang_edit' => _EDIT, 'lang_delete' => _DELETE, 'lang_reply' => _REPLY, 'lang_postedon' => _MD_POSTEDON,'lang_groups' => _MD_GROUPS));
// Read in cookie of 'lastread' times
$topic_lastread = !empty($HTTP_COOKIE_VARS['newbb_topic_lastread']) ? unserialize($HTTP_COOKIE_VARS['newbb_topic_lastread']) : array();
// if cookie is not set for this topic, update view count and set cookie
if ( empty($topic_lastread[$topic_id]) ) {
	$sql = 'UPDATE '.$xoopsDB->prefix('bb_topics').' SET topic_views = topic_views + 1 WHERE topic_id ='. $topic_id;
	$xoopsDB->queryF($sql);
}

$topic_lastread[$topic_id] = time();
setcookie("newbb_topic_lastread", serialize($topic_lastread), time()+365*24*3600, $forumCookie['path'], $forumCookie['domain'], $forumCookie['secure']);

if ($xoopsModuleConfig['rss_enable'] == 1) {
    $xoopsTpl->assign("rss_enable","<div align='right'><a href='../../cache/forum.xml' target='_blank'><img src='./images/rss.png' border='0' vspace='2'></a></div>");
}
 include XOOPS_ROOT_PATH.'/footer.php';
?>