<?php
// $Id: viewforum.php,v 1.7 2004/05/29 17:11:48 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //

include "header.php";
include_once(XOOPS_ROOT_PATH . "/modules/" . $xoopsModule->dirname() . "/class/class.permission.php");
include_once(XOOPS_ROOT_PATH . "/modules/" . $xoopsModule->dirname() . "/class/class.forum.php");

if (isset($_GET['forum'])) $forum = intval($_GET['forum']);

if ($xoopsUser)$UserUid=$xoopsUser->getVar("uid");
else $UserUid=null;

if ( $forum < 1 ) {
	redirect_header("index.php", 2, _MD_ERRORFORUM);
	exit();
}
$forumcontent = new Forums();
$forumdata = $forumcontent->getForum($forum);

$xoopsOption['template_main'] = 'newbb_viewforum.html';
include XOOPS_ROOT_PATH."/header.php";

$show_reg = 0;
if ($xoopsModuleConfig['wol_enabled'])
{
	include_once XOOPS_ROOT_PATH.'/modules/newbb/class/class.whoisonline.php';
	$whosonline = new WhosOnline($forum);
	$xoopsTpl->assign('whois', $whosonline->show_online_list());
}

subforum($forum);
$getpermission = new Permissions();
$forumpermission = $getpermission->getPermissions("forum");
if (isset($forumpermission[$forumdata['forum_id']]['global_forum_access'])) {
	$forumpermissions=  1;
}
else
{
	$forumpermissions = 0;
}

$permission_set = $getpermission->getPermissions("topic");

$xoopsTpl->assign("forum_id", $forum);
if (isset($permission_set[$forumdata['forum_id']]['forum_can_post'])) {
	$xoopsTpl->assign('viewer_can_post', true);
	$xoopsTpl->assign('forum_post_or_register', "<a href=\"newtopic.php?forum=".$forum."\"><img src=\"".$forumImage['post']."\" alt=\""._MD_POSTNEW."\" /></a>");
} else {
	$xoopsTpl->assign('viewer_can_post', false);
	if ( $show_reg == 1 ) {
		$xoopsTpl->assign('forum_post_or_register', '<a href="'.XOOPS_URL.'/user.php?xoops_redirect='.htmlspecialchars($xoopsRequestUri).'">'._MD_REGTOPOST.'</a>');
	} else {
		$xoopsTpl->assign('forum_post_or_register', "");
	}
}
if($forumdata['parent_forum'] > 0)
{
	$q = "select forum_name from ".$xoopsDB->prefix('bb_forums')." WHERE forum_id=".$forumdata['parent_forum'];
	$row = $xoopsDB->fetchArray($xoopsDB->query($q));
	$xoopsTpl->assign(array('parent_forum' => $forumdata['parent_forum'], 'parent_name' => $row['forum_name']));
	
	$xoopsTpl->assign('forum_index_title', sprintf(_MD_FORUMINDEX,$xoopsConfig['sitename']));
	$xoopsTpl->assign('folder_topic', $forumImage['folder_topic']);
	$xoopsTpl->assign('forum_name', $myts->htmlSpecialChars($forumdata['forum_name']));
	$xoopsTpl->assign('forum_moderators', get_moderators($forum));
}

else
{
	$xoopsTpl->assign('forum_index_title', sprintf(_MD_FORUMINDEX,$xoopsConfig['sitename']));
	$xoopsTpl->assign('folder_topic', $forumImage['folder_topic']);
	$xoopsTpl->assign('forum_name', $myts->htmlSpecialChars($forumdata['forum_name']));
	$xoopsTpl->assign('forum_moderators', get_moderators($forum));
}

$sel_sort_array = array("t.topic_title"=>_MD_TOPICTITLE, "t.topic_replies"=>_MD_NUMBERREPLIES, "u.uname"=>_MD_TOPICPOSTER, "t.topic_views"=>_MD_VIEWS, "p.post_time"=>_MD_LASTPOSTTIME);
if ( !isset($_GET['sortname']) || !in_array($_GET['sortname'], array_keys($sel_sort_array)) ) {
	$sortname = "p.post_time";
} else {
	$sortname = $_GET['sortname'];
}

$forum_selection_sort = '<select name="sortname">';
foreach ( $sel_sort_array as $sort_k => $sort_v ) {
	$forum_selection_sort .= '<option value="'.$sort_k.'"'.(($sortname == $sort_k) ? ' selected="selected"' : '').'>'.$sort_v.'</option>';
}
$forum_selection_sort .= '</select>';

// assign to template
$xoopsTpl->assign('forum_selection_sort', $forum_selection_sort);

$sortorder = (!isset($_GET['sortorder']) || $_GET['sortorder'] != "ASC") ? "DESC" : "ASC";
$forum_selection_order = '<select name="sortorder">';
$forum_selection_order .= '<option value="ASC"'.(($sortorder == "ASC") ? ' selected="selected"' : '').'>'._MD_ASCENDING.'</option>';
$forum_selection_order .= '<option value="DESC"'.(($sortorder == "DESC") ? ' selected="selected"' : '').'>'._MD_DESCENDING.'</option>';
$forum_selection_order .= '</select>';

// assign to template
$xoopsTpl->assign('forum_selection_order', $forum_selection_order);

$sortsince = !empty($_GET['sortsince']) ? intval($_GET['sortsince']) : 100;
$sel_since_array = array(1, 2, 5, 10, 20, 30, 40, 60, 75, 100);
$forum_selection_since = '<select name="sortsince">';
foreach ($sel_since_array as $sort_since_v) {
	$forum_selection_since .= '<option value="'.$sort_since_v.'"'.(($sortsince == $sort_since_v) ? ' selected="selected"' : '').'>'.sprintf(_MD_FROMLASTDAYS,$sort_since_v).'</option>';
}
$forum_selection_since .= '<option value="365"'.(($sortsince == 365) ? ' selected="selected"' : '').'>'.sprintf(_MD_THELASTYEAR,365).'</option>';
$forum_selection_since .= '<option value="1000"'.(($sortsince == 1000) ? ' selected="selected"' : '').'>'.sprintf(_MD_BEGINNING,1000).'</option>';
$forum_selection_since .= '</select>';

// assign to template
$xoopsTpl->assign('forum_selection_since', $forum_selection_since);
$xoopsTpl->assign('h_topic_link', "viewforum.php?forum=$forum&amp;sortname=t.topic_title&amp;sortsince=$sortsince&amp;sortorder=". (($sortname == "t.topic_title" && $sortorder == "DESC") ? "ASC" : "DESC"));
$xoopsTpl->assign('h_reply_link', "viewforum.php?forum=$forum&amp;sortname=t.topic_replies&amp;sortsince=$sortsince&amp;sortorder=". (($sortname == "t.topic_replies" && $sortorder == "DESC") ? "ASC" : "DESC"));
$xoopsTpl->assign('h_poster_link', "viewforum.php?forum=$forum&amp;sortname=u.uname&amp;sortsince=$sortsince&amp;sortorder=". (($sortname == "u.uname" && $sortorder == "DESC") ? "ASC" : "DESC"));
$xoopsTpl->assign('h_views_link', "viewforum.php?forum=$forum&amp;sortname=t.topic_views&amp;sortsince=$sortsince&amp;sortorder=". (($sortname == "t.topic_views" && $sortorder == "DESC") ? "ASC" : "DESC"));
$xoopsTpl->assign('h_ratings_link', "viewforum.php?forum=$forum&amp;sortname=t.topic_ratings&amp;sortsince=$sortsince&amp;sortorder=". (($sortname == "t.topic_ratings" && $sortorder == "DESC") ? "ASC" : "DESC"));
$xoopsTpl->assign('h_date_link', "viewforum.php?forum=$forum&amp;sortname=p.post_time&amp;sortsince=$sortsince&amp;sortorder=". (($sortname == "p.post_time" && $sortorder == "DESC") ? "ASC" : "DESC"));

$startdate = time() - (86400* $sortsince);
$start = !empty($_GET['start']) ? intval($_GET['start']) : 0;

$sql = 'SELECT t.*, u.uname, u2.uname as last_poster, p.post_time as last_post_time, p.icon FROM '.$xoopsDB->prefix("bb_topics").' t LEFT JOIN '.$xoopsDB->prefix("users").' u ON u.uid = t.topic_poster LEFT JOIN '.$xoopsDB->prefix('bb_posts').' p ON p.post_id = t.topic_last_post_id LEFT JOIN '.$xoopsDB->prefix("users").' u2 ON  u2.uid = p.uid WHERE t.forum_id = '.$forum.' AND (p.post_time > '.$startdate.' OR t.topic_sticky=1) ORDER BY topic_sticky DESC, '.$sortname.' '.$sortorder;
if ( !$result = $xoopsDB->query($sql,$forumdata['topics_per_page'],$start) ) {
	redirect_header('index.php',2,_MD_ERROROCCURED);
	exit();
}

if (isset($_GET['mark_read']) && $_GET['mark_read'] == 1)
{
	$forum_lastview = !empty($HTTP_COOKIE_VARS['newbb_forum_lastview']) ? unserialize($HTTP_COOKIE_VARS['newbb_forum_lastview']) : array();
	$forum_lastview[$forum] = time();
	setcookie("newbb_forum_lastview", serialize($forum_lastview), time()+365*24*3600, $forumCookie['path'], $forumCookie['domain'], $forumCookie['secure']);
	
	$url=XOOPS_URL . "/modules/newbb/viewforum.php?forum=".$forum;
	redirect_header($url,2,_MD_MARKEDASREAD);
}


$forum_lastview = !empty($HTTP_COOKIE_VARS['newbb_forum_lastview']) ? unserialize($HTTP_COOKIE_VARS['newbb_forum_lastview']) : array();
$topic_att = "";
// Read topic 'lastread' times from cookie, if exists
$topic_lastread = !empty($HTTP_COOKIE_VARS['newbb_topic_lastread']) ? unserialize($HTTP_COOKIE_VARS['newbb_topic_lastread']) : array();


$ft = new Forums();
$sticky = $ft->getAllTopics($forum,$startdate,$start,$sortname,$sortorder,$forumdata['topics_per_page'],$topic_lastread,$forum_lastview,$forumdata['hot_threshold'],$forumdata['posts_per_page'],$UserUid);

$xoopsTpl->assign('sticky', $sticky);
$xoopsTpl->assign('rating_enable', $xoopsModuleConfig['rating_enabled']);
$xoopsTpl->assign('img_newposts', $forumImage['newposts_topic']);
$xoopsTpl->assign('img_hotnewposts', $forumImage['hot_newposts_topic']);
$xoopsTpl->assign('img_folder', $forumImage['folder_topic']);
$xoopsTpl->assign('img_hotfolder', $forumImage['hot_folder_topic']);
$xoopsTpl->assign('img_locked', $forumImage['locked_topic']);
$xoopsTpl->assign('img_sticky', $forumImage['folder_sticky']);
$xoopsTpl->assign('mark_read', "<a href=\"viewforum.php?forum=".$forum."&mark_read=1\">"._MD_MARK_ALL_POSTS."</a>");

$sql = 'SELECT COUNT(*) FROM '.$xoopsDB->prefix('bb_topics').' WHERE forum_id = '.$forum.' AND (topic_time > '.$startdate.' OR topic_sticky = 1)';
if ( !$r = $xoopsDB->query($sql) ) {
	redirect_header('index.php',2,_MD_ERROROCCURED);
	//exit();
}
list($all_topics) = $xoopsDB->fetchRow($r);
if ( $all_topics > $forumdata->topics_per_page) {
	include XOOPS_ROOT_PATH.'/class/pagenav.php';
	$nav = new XoopsPageNav($all_topics, $forumdata['topics_per_page'], $start, "start", 'forum='.$forum.'&amp;sortname='.$sortname.'&amp;sortorder='.$sortorder.'&amp;sortsince='.$sortsince);
	$xoopsTpl->assign('forum_pagenav', $nav->renderNav(4));
} else {
	$xoopsTpl->assign('forum_pagenav', '');
}
$xoopsTpl->assign('forum_jumpbox', make_jumpbox($forum));
$xoopsTpl->assign('permission_table', $getpermission->permission_table($permission_set,$forumdata['forum_id']));
include XOOPS_ROOT_PATH."/footer.php";
?>