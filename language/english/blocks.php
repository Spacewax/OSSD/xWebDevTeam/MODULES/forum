<?php
// $Id: blocks.php,v 1.2 2004/05/16 23:13:04 praedator Exp $
// Blocks
define("_MB_NEWBB_FORUM","Forum");
define("_MB_NEWBB_TOPIC","Topic");
define("_MB_NEWBB_RPLS","Replies");
define("_MB_NEWBB_VIEWS","Views");
define("_MB_NEWBB_LPOST","Last Post");
define("_MB_NEWBB_VSTFRMS"," Visit Forums");
define("_MB_NEWBB_DISPLAY","Display %s posts");
define("_MB_NEWBB_DISPLAYF","Display in full size");
?>