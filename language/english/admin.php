<?php
// $Id: admin.php,v 1.3 2004/05/29 17:11:49 praedator Exp $
//%%%%%%	File Name  index.php   	%%%%%
define("_MD_A_FORUMCONF","Forum Configuration");
define("_MD_A_ADDAFORUM","Add a Forum");
define("_MD_A_SYNCFORUM","Sync forum/topic index");
define("_MD_A_LINK2SYNC","This link will allow you to sync up the forum and topic indexes to fix any discrepancies that might arise");
define("_MD_A_ADDACAT","Add a Category");
define("_MD_A_REORDERFORUM","Reorder Board");
define("_MD_A_FORUM_MANAGER","Forum Manager");
define("_MD_A_PRUNE_TITLE","Forum Prune System");
define("_MD_A_CATADMIN","Categories Manager");
define("_MD_A_GENERALSET", "Module Settings" );
define("_MD_A_BOARDSET","Board Settings");
define("_MD_A_USEFULL","Main Setting Tools");
define("_MD_A_NEW","New");
define("_MD_A_MODULEADMIN","Module Admin:");
define("_MD_A_HELP","Help");
define("_MD_A_ABOUT","About");
define("_MD_A_MODERATEUSER","Moderate User");
define("_MD_A_BOARDSUMMARY","Board Statistic");
define("_MD_A_PENDING_POSTS_FOR_AUTH","Posts pending authorization");
define("_MD_A_POSTID","Post ID");
define("_MD_A_POSTDATE","Post Date");
define("_MD_A_POSTER","Poster");
define("_MD_A_TOPICS","Topics");
define("_MD_A_SHORTSUMMARY","Board Summary");
define("_MD_A_TOTALPOSTS","Total Posts");
define("_MD_A_TOTALTOPICS","Total Topics");
define("_MD_A_TOTALVIEWS","Total Views");
define("_MD_A_TOTALUSER","Total User");
define("_MD_A_BLOCKS","Blocks");
define("_MD_A_SUBJECT","Subject");
define("_MD_A_APPROVE","Approve Post");
define("_MD_A_APPROVETEXT","Content of this Posting");
define('_MD_A_DEL_ONE','Delete only this message');
define('_MD_A_POSTSDELETED','Selected post deleted.');
define('_MD_A_NOAPPROVEPOST','There are presently no posts waiting approval.');
define('_MD_A_SUBJECTC','Subject:');
define('_MD_A_MESSAGEICON','Message Icon:');
define('_MD_A_MESSAGEC','Message:');
define('_MD_A_CANCELPOST','Cancel Post'); 
define('_MD_A_GOTOMOD','Go to module');


// admin_forum_manager.php
define("_MD_A_NAME","Name");
define("_MD_A_ACCESS","Access");
define("_MD_A_CREATEFORUM","Create Forum");
define("_MD_A_EDITFORM","Submit changes");
define("_MD_A_EDIT","Edit");
define("_MD_A_CLEAR","Clear");
define("_MD_A_DELETE","Delete");
define("_MD_A_ADD","Add");
define("_MD_A_MOVE","Move");
define("_MD_A_ORDER","Order");
define("_MD_A_ADD_CATEGORY","Add a Category");
define("_MD_A_BACK_TO_FM","Back to forum Manager");
define("_MD_A_TWDAFAP","Note: This will remove the forum and all posts in it.<br><br>WARNING: Are you sure you want to delete this Forum?");
define("_MD_A_FORUMREMOVED","Forum Removed.");
define("_MD_A_CREATENEWFORUM","Create a New Forum");
define("_MD_A_EDITTHISFORUM","Editing Forum:");
define("_MD_A_SET_FORUMORDER","Set Forum Position:");
define("_MD_A_ALLOWPOLLS","Allow Polls:");
define("_MD_A_ATTACHMENT_SIZE" ,"Max Size in kb`s:");
define("_MD_A_ALLOWED_EXTENSIONS", "Allowed Extensions:<span style='font-size: xx-small; font-weight: normal; display: block;'>Extensions delimited by |</span>");
define("_MD_A_ALLOW_ATTACHMENTS", "Allow Attachments:");
define("_MD_A_ALLOWHTML","Allow HTML:");
define("_MD_A_YES","Yes");
define("_MD_A_NO","No");
define("_MD_A_ALLOWSIGNATURES","Allow Signatures:");
define("_MD_A_HOTTOPICTHRESHOLD","Hot Topic Threshold:");
define("_MD_A_POSTPERPAGE","Posts per Page:<span style='font-size: xx-small; font-weight: normal; display: block;'>(This is the number of posts<br> per topic that will be<br> displayed per page of a topic)</span>");
define("_MD_A_TOPICPERFORUM","Topics per Forum:<span style='font-size: xx-small; font-weight: normal; display: block;'>(This is the number of topics<br> per forum that will be<br> displayed per page of a forum)</span>");
define("_MD_A_SHOWNAME","Replace user's name with real name:");
define("_MD_A_SHOWICONSPANEL","Show icons panel:");
define("_MD_A_SHOWSMILIESPANEL","Show smilies panel:");
define("_MD_A_PRIVATE","Private");

// admin_cat_manager.php

define("_MD_A_SETCATEGORYORDER","Set Category Position:");
define("_MD_A_VISIBLE","Visible");
define("_MD_A_PUBLIC","Public");
define("_MD_A_HIDDEN","Hidden");
define("_MD_A_STATE","Status:");
define("_MD_A_CATEGORYDESC","Category Description:");
define("_MD_A_SHOWDESC","Show Description?");
define("_MD_A_SPONSORIMAGE","Sponsor Image:");
define("_MD_A_SPONSORLINK","Sponsor Link:");
define("_MD_A_DELCAT","Delete Category");
define("_MD_A_WAYSYWTDTTAL","Note: This will NOT remove the forums under the category, you must do that via the Edit Forum section.<br><br>WARNING: Are you sure you want to delete this Category?");



//%%%%%%        File Name  admin_forums.php           %%%%%
define("_MD_A_FORUMUPDATED","Forum Updated");
define("_MD_A_HTSMHNBRBITHBTWNLBAMOTF","However the selected moderator(s) have not be removed because if they had been there would no longer be any moderators on this forum.");

define("_MD_A_FRFDAWAIP","Forum removed from database along with all its posts.");
define("_MD_A_NOSUCHFORUM","No such forum");
define("_MD_A_FORUMNAME","Forum Name:");
define("_MD_A_FORUMDESCRIPTION","Forum Description:");
define("_MD_A_MODERATOR","Moderator(s):");
define("_MD_A_REMOVE","Remove");
define("_MD_A_NOMODERATORASSIGNED","No Moderators Assigned");
define("_MD_A_NONE","None");
define("_MD_A_CATEGORY","Category:");
define("_MD_A_ANONYMOUSPOST","Anonymous Posting");
define("_MD_A_REGISTERUSERONLY","Registered users only");
define("_MD_A_MODERATORANDADMINONLY","Moderators/Administrators only");
define("_MD_A_TYPE","Type:");
define("_MD_A_SELECTFORUMEDIT","Select a Forum to Edit");
define("_MD_A_NOFORUMINDATABASE","No Forums in Database");
define("_MD_A_DATABASEERROR","Database Error");
define("_MD_A_CATEGORYUPDATED","Category Updated.");
define("_MD_A_EDITCATEGORY","Editing Category:");
define("_MD_A_CATEGORYTITLE","Category Title:");
define("_MD_A_SELECTACATEGORYEDIT","Select a Category to Edit");
define("_MD_A_CATEGORYCREATED","Category Created.");
define("_MD_A_REMOVECATEGORY","Remove Category");
define("_MD_A_CREATENEWCATEGORY","Create a New Category");
define("_MD_A_YDNFOATPOTFDYAA","You did not fill out all the parts of the form.<br>Did you assign at least one moderator? Please go back and correct the form.");
define("_MD_A_FORUMCREATED","Forum Created.");
define("_MD_A_VTFYJC","View the forum  you just created.");
define("_MD_A_EYMAACBYAF","Error, you must add a category before you add forums");
define("_MD_A_ACCESSLEVEL","Global Access Level:");
define("_MD_A_TODHITOTCWDOTIP","The order displayed here is the order the categories will display on the index page. To move a category up in the ordering click Move Up to move it down click Move Down.");
define("_MD_A_ECWMTCPUODITO","Each click will move the category 1 place up or down in the ordering.");
define("_MD_A_CATEGORY1","Category");
define("_MD_A_FORUMUPDATE","Forum Settings Updated");
define("_MD_A_RETURNTOADMINPANEL","Return to the Administration Panel.");
define("_MD_A_RETURNTOFORUMINDEX","Return to the forum index");
define("_MD_A_SAVECHANGES","Save Changes");
define("_MD_A_CLICKBELOWSYNC","Clicking the button below will sync up your forums and topics pages with the correct data from the database. Use this section whenever you notice flaws in the topics and forums lists.");
define("_MD_A_SYNCHING","Synchronizing forum index and topics (This may take a while)");
define("_MD_A_DONESYNC","Done!");
define("_MD_A_CATEGORYDELETED","Category deleted.");
define("_MD_A_MOVE2CAT","Move to category:");
define("_MD_A_MAKE_SUBFORUM_OF","Make subforum of:");
define("_MD_A_MSG_FORUM_MOVED","Forum moved!");
define("_MD_A_MSG_ERR_FORUM_MOVED","Failed to move forum.");
define("_MD_A_SELECT","< Select >");
define("_MD_A_MOVETHISFORUM","Move this Forum");

// admin_mod_user.php

define("_MD_A_USERTOBEMODERATE","User which are moderated");
define("_MD_A_ADDMODUSER","Add User to be moderate");
define("_MD_A_USER","User");
define("_MD_A_NOUSER","No User found with Status: moderated");
define("_MD_A_REASON","Reason");
define("_MD_A_USERDELETED","User no longer be moderated");
define("_MD_A_EDITMODUSER","Edit moderated User");
define("_MD_A_CREATENEWMODUSER","Add User to be moderate");
define("_MD_A_USERUID","User UID");
define("_MD_A_USERADDED","User added");
define("_MD_A_USERUPDATED","User updated");
define("_MD_A_USERNOTUPDATED","User NOT updated");
define("_MD_A_MODWAYSYWTDTTAL","Note: This will remove the moderated Status from this User.<br><br>WARNING: Are you sure you want to delete this User?");



//%%%%%%        File Name  admin_priv_forums.php           %%%%%

define("_MD_A_SAFTE","Select a Forum to Edit");
define("_MD_A_NFID","No Forums in Database");
define("_MD_A_EFPF","Editing Forum Permissions for:");
define("_MD_A_UWA","Users With Access:");
define("_MD_A_UWOA","Users Without Access:");
define("_MD_A_GWA","Groups With Access:");
define("_MD_A_GWOA","Groups Without Access:");
define("_MD_A_ADDUSERS","Add Users -->");
define("_MD_A_CLEARALLUSERS","Clear all users");
define("_MD_A_ADDGROUPS","Add Groups -->");
define("_MD_A_CLEARALLGROUPS","Clear All Groups");
define("_MD_A_REVOKEPOSTING","revoke posting");
define("_MD_A_GRANTPOSTING","grant posting");

//%%%%%%        File Name  admin_forum_reorder.php           %%%%%
define("_MD_A_REORDERID","ID");
define("_MD_A_REORDERTITLE","Title");
define("_MD_A_REORDERWEIGHT","Possition");
define("_MD_A_SETFORUMORDER","Set Board Ordering");
define("_MD_A_BOARDREORDER","The Board has reordered");

// forum_access.php
define("_MD_A_ALLOW_GROUP", "Allow Group");
define("_MD_A_ALLOW_USER", "Allow User");
define("_MD_A_PERMISSIONS_FORUM","Permissions for Forum:");
define("_MD_A_PERMISSIONS_TO_THIS_FORUM","Permissions for this Forum");
define("_MD_A_PERMISSIONS_GROUP","Group Permissions");
define("_MD_A_PERMISSIONS_USER","User Permissions");
define("_MD_A_GROUP_NAME","Group Name");
define("_MD_A_USER_NAME","User Name");
define("_MD_A_UPDATE_GROUPS","Update Groups");
define("_MD_A_UPDATE_USERS","Update Users");
define("_MD_A_CAN_VIEW","Can View");
define("_MD_A_CAN_POST","Can Post");
define("_MD_A_CAN_REPLY","Can Reply");
define("_MD_A_CAN_EDIT","Can Edit");
define("_MD_A_CAN_DELETE","Can Delete");
define("_MD_A_CAN_ADDPOLL","Can Add Poll");
define("_MD_A_CAN_VOTE","Can Vote");
define("_MD_A_CAN_ATTACH","Can Attach");
define("_MD_A_ACTION","Action");
define("_MD_A_MSG_GROUP_ADD","Allowed group added.");
define("_MD_A_MSG_ERR_GROUP_ADD","Failed to add allowed group.");
define("_MD_A_MSG_GROUP_REVOKE","Group access revoked.");
define("_MD_A_MSG_ERR_GROUP_REVOKE","Failed to revoke group.");
define("_MD_A_MSG_GROUPS_UPDATED","Groups updated.");
define("_MD_A_MSG_USER_ADD","Allowed user added.");
define("_MD_A_MSG_ERR_USER_ADD","Failed to add allowed user.");
define("_MD_A_MSG_USER_REVOKE","User access revoked.");
define("_MD_A_MSG_ERR_USER_REVOKE","Failed to revoke user.");
define("_MD_A_MSG_USERS_UPDATED","Users updated.");
define("_MD_A_COPY_PERMISSIONS","Copy permisions from forum:");
define("_MD_A_COPY","Copy");
define("_MD_A_REVOKE","Revoke");
define("_MD_A_GRPMGR_APPLY","Apply modifications");
define("_MD_A_GRPMGR_SYN","Synchronize");
define("_MD_A_GRPMGR_ADDTOALL","Set this Permission to all Forums"); 
define("_MD_A_GRPMGR_SYN_NOTE","Click 'Synchronize' for sychronizing newbb groups with Xoops groups");
define("_MD_A_GRPMGR_EMPTY","newbb Group table was empty !<br /><br />Default groups created.");

// admin_forum_prune.php

define ("_MD_A_PRUNE_RESULTS_TITLE","Prune Results");
define ("_MD_A_PRUNE_RESULTS_TOPICS","Pruned Topics");
define ("_MD_A_PRUNE_RESULTS_POSTS","Pruned Posts");
define ("_MD_A_PRUNE_RESULTS_FORUMS","Pruned Forums");
define ("_MD_A_PRUNE_STORE","Store posts in this forum instead delete them");
define ("_MD_A_PRUNE_ARCHIVE","Make a copy of posts into Archive");
define ("_MD_A_PRUNE_FORUMSELERROR","You forgot select forum/s to prune");

define ("_MD_A_PRUNE_DAYS","Remove topics without replies in:");
define ("_MD_A_PRUNE_FORUMS","Forums to be pruned");
define ("_MD_A_PRUNE_STICKY","Keep Sticky topics");
define ("_MD_A_PRUNE_LOCK","Keep Lock topics");
define ("_MD_A_PRUNE_HOT","Keep topics with more than this replies");
define ("_MD_A_PRUNE_SUBMIT","Ok");
define ("_MD_A_PRUNE_RESET","Reset");
define ("_MD_A_PRUNE_YES","Yes");
define ("_MD_A_PRUNE_NO","No");
define ("_MD_A_PRUNE_WEEK","A Week");
define ("_MD_A_PRUNE_2WEEKS","Two Weeks");
define ("_MD_A_PRUNE_MONTH","A Month");
define ("_MD_A_PRUNE_2MONTH","Two Months");
define ("_MD_A_PRUNE_4MONTH","Four Months");
define ("_MD_A_PRUNE_YEAR","A Year");
define ("_MD_A_PRUNE_2YEARS","2 Years");

// About.php constants
define('_MD_A_AUTHOR_INFO', "Author Informations");
define('_MD_A_AUTHOR_NAME', "Author");
define('_MD_A_AUTHOR_WEBSITE', "Author's website");
define('_MD_A_AUTHOR_EMAIL', "Author's email");
define('_MD_A_AUTHOR_CREDITS', "Credits");
define('_MD_A_MODULE_INFO', "Module Developpment Informations");
define('_MD_A_MODULE_STATUS', "Status");
define('_MD_A_MODULE_DEMO', "Demo Site");
define('_MD_A_MODULE_SUPPORT', "Official support site");
define('_MD_A_MODULE_BUG', "Report a bug for this module");
define('_MD_A_MODULE_FEATURE', "Suggest a new feature for this module");
define('_MD_A_MODULE_DISCLAIMER', "Disclaimer");
define('_MD_A_AUTHOR_WORD', "The Author's Word");
define('_MD_A_BY','By');


?>