<?php
// $Id: modinfo.php,v 1.3 2004/05/29 17:11:49 praedator Exp $
// Module Info

// The name of this module
define("_MI_NEWBB_NAME","Forum");

// A brief description of this module
define("_MI_NEWBB_DESC","Forums module for XOOPS");

// Names of blocks for this module (Not all module has blocks)
define("_MI_NEWBB_BNAME1","Recent Topics");
define("_MI_NEWBB_BNAME2","Most Viewed Topics");
define("_MI_NEWBB_BNAME3","Most Active Topics");
define("_MI_NEWBB_BNAME4","Recent Private Topics");

// Names of admin menu items
define("_MI_NEWBB_ADMENU1","Add Forum");
define("_MI_NEWBB_ADMENU2","Edit Forum");
define("_MI_NEWBB_ADMENU3","Edit Priv. Forum");
define("_MI_NEWBB_ADMENU4","Sync forums/topics");
define("_MI_NEWBB_ADMENU5","Add Category");
define("_MI_NEWBB_ADMENU6","Edit Category");
define("_MI_NEWBB_ADMENU7","Delete Category");
define("_MI_NEWBB_ADMENU8","Re-order Category");
define("_MI_NEWBB_ADMENU9","Re-order Forum");

//config options

define("_MI_IMG_SET","Image Set");
define("_MI_IMG_SET_DESC","Select the Image Set to use");
define("_MI_DIR_ATTACHMENT","Path where the attachments will be stored.");
define("_MI_DIR_ATTACHMENT_DESC","Like attachments without leading and ending /");
define("_MI_MAX_IMG_WIDTH","Maximum Image Attachment Width");
define("_MI_MAX_IMGWIDTHDSC", "Sets the maximum allowed <b>Width</b> size of an image when uploading.");
define("_MI_SHOW_DIS","Show Disclaimer On");
define("_MI_DISCLAIMER","Disclaimer");
define("_MI_DISCLAIMER_DESC","Enter your Disclaimer which will be shown to the above selected option.");
define("_MI_NONE","None");
define("_MI_POST","Post");
define("_MI_REPLY","Reply");
define("_MI_OP_BOTH","Both");
define("_MI_WOL_ENABLE","Enable Who's Online");
define("_MI_WOL_ENABLE_DESC","Enable Who's Online Block shown below the Indexpage and the Forumpages");
define("_MI_WOL_ADMIN_COL","Administrator Highlight Color");
define("_MI_WOL_ADMIN_COL_DESC","Highlight Color of the Administrators shown in the Who's Online Block");
define("_MI_WOL_MOD_COL","Moderator Highlight Color");
define("_MI_WOL_MOD_COL_DESC","Highlight Color of the Moderators shown in the Who's Online Block");
define("_MI_LEVELS_ENABLE", "Enable HP/MP/EXP Levels Mod");
define("_MI_LEVELS_ENABLE_DESC", "<b>HP</b>  is determined by your average posts per day.<br><b>MP</b>  is determined by your join date related to your post count.<br><b>EXP</b> goes up each time you post, and when you get to 100%, you gain a level and the EXP drops to 0 again.");
define("_MI_RSS_ENABLE","Enable RSS Feed");
define("_MI_RSS_ENABLE_DESC","Enable RSS Feed, with below maximum Items and Description length");
define("_MI_RSS_MAX_ITEMS", "Max. Items");
define("_MI_RSS_MAX_DESCRIPTION", "Max. Description Length");
define("_MI_ALLOW_POSTANON", "Allow Post Anonymously");
define("_MI_ALLOW_POSTANON_DESC", "Allow Post Anonymously");
define("_MI_MEDIA_ENABLE","Enable Media Features");
define("_MI_MEDIA_ENABLE_DESC","Allow to show attached Images directly in the post.");
define("_MI_USERBAR_ENABLE","Enable Userbar");
define("_MI_USERBAR_ENABLE_DESC","Allow to show the expand Userbar: Profile, PM, ICQ, MSN, etc...");
define("_MI_RATING_ENABLE","Enable Rating Function");
define("_MI_RATING_ENABLE_DESC","Allow to rate a Topic");
define("_MI_VIEWMODE","View Mode of the Threads");
define("_MI_VIEWMODE_DESC","To override the General Settings to the viewmode of the Threads choice FLAT, THREADED or NONE for no overriding");
define("_MI_HIDE_PRIVATE", "Hide private forums from users without permission");
define("_MI_HIDE_PRIVATE_DESC", "Allows private forum posts to be included in blocks and searches, but won`t be visible for users without permission.");
define("_MI_REPORTMOD_ENABLE","Report a Post to a Moderator");
define("_MI_REPORTMOD_ENABLE_DESC","User can report posts to an Moderator for taking action"); 


// RMV-NOTIFY
// Notification event descriptions and mail templates

define ('_MI_NEWBB_THREAD_NOTIFY', 'Thread');
define ('_MI_NEWBB_THREAD_NOTIFYDSC', 'Notification options that apply to the current thread.');

define ('_MI_NEWBB_FORUM_NOTIFY', 'Forum');
define ('_MI_NEWBB_FORUM_NOTIFYDSC', 'Notification options that apply to the current forum.');

define ('_MI_NEWBB_GLOBAL_NOTIFY', 'Global');
define ('_MI_NEWBB_GLOBAL_NOTIFYDSC', 'Global forum notification options.');

define ('_MI_NEWBB_THREAD_NEWPOST_NOTIFY', 'New Post');
define ('_MI_NEWBB_THREAD_NEWPOST_NOTIFYCAP', 'Notify me of new posts in the current thread.');
define ('_MI_NEWBB_THREAD_NEWPOST_NOTIFYDSC', 'Receive notification when a new message is posted to the current thread.');
define ('_MI_NEWBB_THREAD_NEWPOST_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} auto-notify : New post in thread');

define ('_MI_NEWBB_FORUM_NEWTHREAD_NOTIFY', 'New Thread');
define ('_MI_NEWBB_FORUM_NEWTHREAD_NOTIFYCAP', 'Notify me of new topics in the current forum.');
define ('_MI_NEWBB_FORUM_NEWTHREAD_NOTIFYDSC', 'Receive notification when a new thread is started in the current forum.');
define ('_MI_NEWBB_FORUM_NEWTHREAD_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} auto-notify : New thread in forum');

define ('_MI_NEWBB_GLOBAL_NEWFORUM_NOTIFY', 'New Forum');
define ('_MI_NEWBB_GLOBAL_NEWFORUM_NOTIFYCAP', 'Notify me when a new forum is created.');
define ('_MI_NEWBB_GLOBAL_NEWFORUM_NOTIFYDSC', 'Receive notification when a new forum is created.');
define ('_MI_NEWBB_GLOBAL_NEWFORUM_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} auto-notify : New forum');

define ('_MI_NEWBB_GLOBAL_NEWPOST_NOTIFY', 'New Post');
define ('_MI_NEWBB_GLOBAL_NEWPOST_NOTIFYCAP', 'Notify me of any new posts.');
define ('_MI_NEWBB_GLOBAL_NEWPOST_NOTIFYDSC', 'Receive notification when any new message is posted.');
define ('_MI_NEWBB_GLOBAL_NEWPOST_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} auto-notify : New post');

define ('_MI_NEWBB_FORUM_NEWPOST_NOTIFY', 'New Post');
define ('_MI_NEWBB_FORUM_NEWPOST_NOTIFYCAP', 'Notify me of any new posts in the current forum.');
define ('_MI_NEWBB_FORUM_NEWPOST_NOTIFYDSC', 'Receive notification when any new message is posted in the current forum.');
define ('_MI_NEWBB_FORUM_NEWPOST_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} auto-notify : New post in forum');

define ('_MI_NEWBB_GLOBAL_NEWFULLPOST_NOTIFY', 'New Post (Full Text)');
define ('_MI_NEWBB_GLOBAL_NEWFULLPOST_NOTIFYCAP', 'Notify me of any new posts (include full text in message).');
define ('_MI_NEWBB_GLOBAL_NEWFULLPOST_NOTIFYDSC', 'Receive full text notification when any new message is posted.');
define ('_MI_NEWBB_GLOBAL_NEWFULLPOST_NOTIFYSBJ', '[{X_SITENAME}] {X_MODULE} auto-notify : New post (full text)');

?>