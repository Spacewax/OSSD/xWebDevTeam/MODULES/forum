#
# Table structure for table `bb_archive`
#
# Creation: Apr 17, 2004 at 09:24 AM
# Last update: Apr 17, 2004 at 04:21 PM
#

CREATE TABLE `bb_archive` (
  `topic_id` tinyint(8) NOT NULL default '0',
  `post_id` tinyint(8) NOT NULL default '0',
  `post_text` text NOT NULL
) TYPE=MyISAM;
# --------------------------------------------------------

#
# Table structure for table `bb_categories`
#
# Creation: May 02, 2004 at 01:37 PM
# Last update: May 03, 2004 at 02:15 AM
#

CREATE TABLE `bb_categories` (
  `cat_id` smallint(3) unsigned NOT NULL auto_increment,
  `cat_image` varchar(50) NOT NULL default '',
  `cat_title` varchar(100) NOT NULL default '',
  `cat_description` text NOT NULL,
  `cat_order` smallint(3) unsigned NOT NULL default '0',
  `cat_state` enum('0','1') NOT NULL default '0',
  `cat_url` varchar(50) NOT NULL default '',
  `cat_showdescript` smallint(3) NOT NULL default '0',
  PRIMARY KEY  (`cat_id`)
) TYPE=MyISAM AUTO_INCREMENT=17 ;
# --------------------------------------------------------

#
# Table structure for table `bb_forums`
#
# Creation: Apr 29, 2004 at 07:27 AM
# Last update: May 16, 2004 at 06:46 PM
#

CREATE TABLE `bb_forums` (
  `forum_id` smallint(4) unsigned NOT NULL auto_increment,
  `forum_name` varchar(150) NOT NULL default '',
  `forum_desc` text,
  `parent_forum` int(10) NOT NULL default '0',
  `forum_moderator` text NOT NULL,
  `forum_topics` int(8) NOT NULL default '0',
  `forum_posts` int(8) NOT NULL default '0',
  `forum_last_post_id` int(5) unsigned NOT NULL default '0',
  `cat_id` int(2) NOT NULL default '0',
  `forum_type` enum('0','1') NOT NULL default '0',
  `allow_html` enum('0','1') NOT NULL default '1',
  `allow_sig` enum('0','1') NOT NULL default '1',
  `show_name` enum('0','1') NOT NULL default '0',
  `show_icons` enum('0','1') NOT NULL default '1',
  `show_smilies` enum('0','1') NOT NULL default '1',
  `posts_per_page` tinyint(3) unsigned NOT NULL default '20',
  `hot_threshold` tinyint(3) unsigned NOT NULL default '10',
  `topics_per_page` tinyint(3) unsigned NOT NULL default '20',
  `forum_order` int(8) NOT NULL default '0',
  `allow_attachments` enum('0','1') NOT NULL default '1',
  `attach_maxkb` int(10) NOT NULL default '1000',
  `attach_ext` text NOT NULL,
  `allow_polls` enum('0','1') NOT NULL default '0',
  `subforum_count` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`forum_id`)
) TYPE=MyISAM AUTO_INCREMENT=15 ;
# --------------------------------------------------------

#
# Table structure for table `bb_moderate_user`
#
# Creation: May 16, 2004 at 01:57 PM
# Last update: May 16, 2004 at 03:25 PM
#

CREATE TABLE `bb_moderate_user` (
  `id` int(11) NOT NULL auto_increment,
  `uid` int(11) NOT NULL default '0',
  `reason` text NOT NULL,
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=8 ;
# --------------------------------------------------------

#
# Table structure for table `bb_posts`
#
# Creation: May 02, 2004 at 12:54 PM
# Last update: May 16, 2004 at 08:17 PM
# Last check: May 16, 2004 at 05:18 PM
#

CREATE TABLE `bb_posts` (
  `post_id` int(8) unsigned NOT NULL auto_increment,
  `pid` int(8) NOT NULL default '0',
  `topic_id` int(8) NOT NULL default '0',
  `forum_id` int(4) NOT NULL default '0',
  `post_time` int(10) NOT NULL default '0',
  `uid` int(5) unsigned NOT NULL default '0',
  `poster_ip` varchar(15) NOT NULL default '',
  `subject` varchar(255) NOT NULL default '',
  `nohtml` tinyint(1) NOT NULL default '0',
  `nosmiley` tinyint(1) NOT NULL default '0',
  `icon` varchar(25) NOT NULL default '',
  `attachsig` tinyint(1) NOT NULL default '0',
  `attachment` varchar(255) default NULL,
  `pay_money` varchar(25) NOT NULL default '0',
  `approved` enum('0','1') NOT NULL default '1',
  PRIMARY KEY  (`post_id`),
  KEY `uid` (`uid`),
  KEY `pid` (`pid`),
  KEY `subject` (`subject`(40)),
  KEY `forumid_uid` (`forum_id`,`uid`),
  KEY `topicid_uid` (`topic_id`,`uid`),
  KEY `topicid_postid_pid` (`topic_id`,`post_id`,`pid`),
  FULLTEXT KEY `search` (`subject`)
) TYPE=MyISAM AUTO_INCREMENT=176 ;
# --------------------------------------------------------

#
# Table structure for table `bb_posts_text`
#
# Creation: Mar 22, 2004 at 03:08 PM
# Last update: May 16, 2004 at 07:04 PM
# Last check: May 09, 2004 at 05:46 PM
#

CREATE TABLE `bb_posts_text` (
  `post_id` int(8) unsigned NOT NULL auto_increment,
  `post_text` text,
  PRIMARY KEY  (`post_id`),
  FULLTEXT KEY `search` (`post_text`)
) TYPE=MyISAM AUTO_INCREMENT=176 ;
# --------------------------------------------------------

#
# Table structure for table `bb_topics`
#
# Creation: May 02, 2004 at 01:20 AM
# Last update: May 16, 2004 at 07:47 PM
# Last check: May 02, 2004 at 01:20 AM
#

CREATE TABLE `bb_topics` (
  `topic_id` int(8) unsigned NOT NULL auto_increment,
  `topic_title` varchar(255) default NULL,
  `topic_poster` int(5) NOT NULL default '0',
  `topic_time` int(10) NOT NULL default '0',
  `topic_views` int(5) NOT NULL default '0',
  `topic_replies` int(4) NOT NULL default '0',
  `topic_last_post_id` int(8) unsigned NOT NULL default '0',
  `forum_id` int(4) NOT NULL default '0',
  `topic_status` tinyint(1) NOT NULL default '0',
  `topic_sticky` tinyint(1) NOT NULL default '0',
  `rating` double(6,4) NOT NULL default '0.0000',
  PRIMARY KEY  (`topic_id`),
  KEY `forum_id` (`forum_id`),
  KEY `topic_last_post_id` (`topic_last_post_id`),
  KEY `topic_poster` (`topic_poster`),
  KEY `topic_forum` (`topic_id`,`forum_id`),
  KEY `topic_sticky` (`topic_sticky`)
) TYPE=MyISAM AUTO_INCREMENT=34 ;
# --------------------------------------------------------

#
# Table structure for table `bb_whoisonline`
#
# Creation: Mar 22, 2004 at 03:08 PM
# Last update: May 16, 2004 at 07:47 PM
# Last check: May 15, 2004 at 11:25 AM
#

CREATE TABLE `bb_whoisonline` (
  `forum_id` int(10) NOT NULL default '0',
  `user_id` int(10) default NULL,
  `user_ip` varchar(32) default NULL,
  `timestamp` int(14) default NULL
) TYPE=MyISAM;
