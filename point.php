<?php
// ------------------------------------------------------------------------- //
//                Karma Hack for XOOPS - PHP Content Management System       //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //
// Author of Original File: Kazumi Ono (http://www.mywebaddons.com/)         //
// ------------------------------------------------------------------------- //
// Author of Modified Karma File: biomech (http://www.grayhat.org/)          //
################################################################################
$pagetype = "point";
include("header.php");
/*********************************************************/
/* Admin Authentication                                  */
/*********************************************************/
$admintest = 0;

if ( $xoopsUser ) {
	if ( !$xoopsUser->isAdmin() ) {
		redirect_header("index.php",2,_MD_NORIGHTTOACCESS);
		exit();
	}
	
} else {
	redirect_header("index.php",2,_MD_NORIGHTTOACCESS);
	exit();
}
if(!isset($forum)){
	redirect_header("index.php", 2, _MD_ERRORFORUM);
	exit();
}else{	
	include_once($xoopsConfig['root_path']."class/module.textsanitizer.php");

	$sql = "SELECT forum_name, forum_access, forum_type FROM ".$xoopsDB->prefix("bb_forums")." WHERE forum_id = $forum";
	if(!$result = $xoopsDB->query($sql)){
		error_die(_MD_CANTGETFORUM);
	}
	$myrow = $xoopsDB->fetchArray($result);
	$myts=new MyTextsanitizer;
	$forum_name = $myts->htmlSpecialChars($myrow['forum_name']);
	$forum_access = $myrow['forum_access'];
	if ( $myrow['forum_type'] == 1 ) {
		// To get here, we have a logged-in user. So, check whether that user is allowed to post in
		// this private forum.
		$accesserror = 0; //initialize
		if ( $xoopsUser ) {
			if ( !$xoopsUser->isAdmin($xoopsModule->mid()) ){
				if ( !check_priv_forum_auth($xoopsUser->uid(), $forum, TRUE) ) {
					$accesserror = 1;
				}
			}
		} else {
			$accesserror = 1;
		}
		if ( $accesserror == 1 ) {
			redirect_header("viewtopic.php?topic_id=$topic_id&post_id=$post_id&order=$order&viewmode=$viewmode&pid=$pid&forum=$forum",2,_MD_NORIGHTTOPOST);
			exit();
		}
	}
	$accesserror = 0;
	if ( $myrow['forum_access'] == 3 ) {
		if ( $xoopsUser ) {
			if ( !$xoopsUser->isAdmin($xoopsModule->mid()) ) {
				if ( !is_moderator($forum, $xoopsUser->uid()) ) {
					$accesserror = 1;
				}
			}
		} else {
			$accesserror = 1;
		}
		
	} elseif ( $myrow['forum_access'] == 1 && !$xoopsUser ) {
		$accesserror = 1;
	}
	if ( $accesserror == 1 ) {
		redirect_header("viewtopic.php?topic_id=$topic_id&post_id=$post_id&order=$order&viewmode=$viewmode&pid=$pid&forum=$forum",2,_MD_NORIGHTTOPOST);
		exit();
	}
	include($xoopsConfig['root_path']."header.php");
	include_once("class/class.forumpost.php");
	OpenTable();
//	include("page_header.php");


	## Get UserID ##
	$sql = "SELECT post_id, uid FROM ".$xoopsDB->prefix("bb_posts")." WHERE topic_id = $topic_id";
        if(!$result = $xoopsDB->query($sql)){
                error_die(_MD_CANTGETFORUM);
        }
        $myrow = $xoopsDB->fetchArray($result);
	$uid = $myrow['uid'];

	## Get UserName ##
        $sql = "SELECT uid, uname, money FROM ".$xoopsDB->prefix("users")." WHERE uid = $uid";
        if(!$result = $xoopsDB->query($sql)){
                error_die(_MD_CANTGETFORUM);
        }
        $myrow = $xoopsDB->fetchArray($result);
        $uname = $myrow['uname'];
        $pointn = $myrow['money'];

	if (!$point) {
	## Display Karma Adjust Form ##
		echo "<TABLE><TR><TD><B>Adjust $uname($pointn) POINT: </B>
			<FORM METHOD=\"post\" ACTION=\"point.php\">
			<INPUT TYPE=\"hidden\" NAME=\"forum\" VALUE=\"$forum\">
			<INPUT TYPE=\"hidden\" NAME=\"post_id\" VALUE=\"$post_id\">
			<INPUT TYPE=\"hidden\" NAME=\"topic_id\" VALUE=\"$topic_id\">
			<INPUT TYPE=\"hidden\" NAME=\"order\" VALUE=\"$order\">
			<INPUT TYPE=\"hidden\" NAME=\"viewmode\" VALUE=\"$viewmode\">
			<INPUT TYPE=\"hidden\" NAME=\"pid\" VALUE=\"$pid\">
			<input type=\"text\" name=\"countpoint\" value=\"1\">
			<INPUT TYPE=\"submit\" NAME=\"point\" VALUE=\"[+]\">
			<INPUT TYPE=\"submit\" NAME=\"point\" VALUE=\"[-]\">
			</FORM>
		</TABLE>";
	} else {
		## Get Current Karma ##
		$sql = "SELECT uid, money FROM ".$xoopsDB->prefix("users")." WHERE uid = $uid";
		if(!$result = $xoopsDB->query($sql)) {
			error_die("Could not get $uname's money from DB.");
		}
		$myrow = $xoopsDB->fetchArray($result);
		$oldpointval = $myrow['money'];

		## Adjust Karma ##
		
		$point = strtolower($point);
		if ($point == "[+]") {
			$newpointval = $oldpointval + $countpoint;
		} elseif ($point == "[-]") {
			$newpointval = $oldpointval - $countpoint;
		}

		$sql = "UPDATE ".$xoopsDB->prefix("users")." SET money='$newpointval' WHERE uid = '$uid'";
		# echo "SQL: $sql<P>";
		if(!$result = $xoopsDB->query($sql)){
			echo "Could not update DB.";
		}

		echo "<center>\n";
		redirect_header("viewtopic.php?topic_id=$topic_id&post_id=$post_id&order=$order&viewmode=$viewmode&pid=$pid&forum=$forum",2,"$uname POINT $point: $oldpointval => $newpointval.");
	}

//	include("page_tail.php");
	CloseTable();
	include($xoopsConfig['root_path']."footer.php");
}
?>
