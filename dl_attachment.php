<?php
// $Id: dl_attachment.php,v 1.2 2004/05/16 23:13:03 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //

	//(c) 2002 by alphalogic
    include_once("../../mainfile.php");
	$attachid = isset($_GET['attachid']) ? intval($_GET['attachid']) : 0;
    $sql = "SELECT * FROM ".$xoopsDB->prefix("bb_posts")." WHERE post_id=$attachid";
		if (!$result = $xoopsDB->query($sql)) {
			die(_MD_DBERROR);
	}
	while ($o_attach = mysql_fetch_object($result)) {
		if ($o_attach->attachment && $o_attach->attachment!='') {
			$attachment_csv = explode("|",$o_attach->attachment);
		} ELSE {
			die(_MD_NO_SUCH_FILE);
		}
	}
	if (!$attachment_csv && $attachment_csv=='') {
		die(_MD_NO_SUCH_FILE);
	}
	if(!file_exists(XOOPS_ROOT_PATH.'/modules/'.$xoopsModule->dirname().'/attachments/'.$attachment_csv[1])) {
		die(_MD_NO_SUCH_FILE);
	}
	$attachment_csv[3]++;
	$attachment_info = $attachment_csv[0].'|'.$attachment_csv[1].'|'.$attachment_csv[2].'|'.$attachment_csv[3];
	$sql = "UPDATE ".$xoopsDB->prefix("bb_posts")." SET attachment= '$attachment_info' WHERE post_id=$attachid";
		if (!$result = mysql_query($sql)) {
			die(_MD_DBERROR);
		}
	header("Content-type: $attachment_csv[2]");
	header("Content-Disposition: attachment; filename=$attachment_csv[0]");
	readfile(XOOPS_ROOT_PATH.'/modules/'.$xoopsModule->dirname().'/attachments/'.$attachment_csv[1]);
?>
 