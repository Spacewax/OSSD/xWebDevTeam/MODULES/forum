<?php
// $Id: delete.php,v 1.5 2004/05/18 08:41:24 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //

include 'header.php';

$attachment = isset($_GET['attachment']) ? intval($_GET['attachment']) : 0;

$ok = 0;
$forum = isset($_GET['forum']) ? intval($_GET['forum']) : 0;
$post_id = isset($_GET['post_id']) ? intval($_GET['post_id']) : 0;
$pid = isset($_GET['pid']) ? intval($_GET['pid']) : 0; 
$topic_id = isset($_GET['topic_id']) ? intval($_GET['topic_id']) : 0;
$order = isset($_GET['order']) ? intval($_GET['order']) : 0;
$viewmode = (isset($_GET['viewmode']) && $_GET['viewmode'] != 'flat') ? 'thread' : 'flat';
extract($_POST, EXTR_OVERWRITE);
if ( empty($forum) ) {
    redirect_header("index.php", 2, _MD_ERRORFORUM);
    exit();
} elseif ( empty($post_id) ) {
    redirect_header("viewforum.php?forum=$forum", 2, _MD_ERRORPOST);
    exit();
}

if ( $xoopsUser ) {
    if ( !$xoopsUser->isAdmin($xoopsModule->mid()) ) {
        if ( !is_moderator($forum, $xoopsUser->uid()) ) {
            redirect_header("viewtopic.php?topic_id=$topic_id&order=$order&viewmode=$viewmode&pid=$pid&forum=$forum", 2, _MD_DELNOTALLOWED);
            exit();
        }
    }
} else {
    redirect_header("viewtopic.php?topic_id=$topic_id&order=$order&viewmode=$viewmode&pid=$pid&forum=$forum", 2, _MD_DELNOTALLOWED);
    exit();
}

include_once 'class/class.forumpost.php';

if ($attachment==1)
{
    $post = new ForumPosts($post_id);
    $post = delete_attachment();
}

if ( !empty($ok) ) {
    if ( !empty($post_id) ) {
        $post = new ForumPosts($post_id);
        if($ok==1){
        $post->delete();
        }
        if($ok==2){
        $post->delete_one();
        }
        sync($post->forum(), "forum");
        sync($post->topic(), "topic");
    }
    if ( $post->istopic() ) {
        redirect_header("viewforum.php?forum=$forum", 2, _MD_POSTSDELETED);
        exit();
    } else {
        redirect_header("viewtopic.php?topic_id=$topic_id&order=$order&viewmode=$viewmode&pid=$pid&forum=$forum", 2, _MD_POSTSDELETED);
        exit();
    }
} else {
    include XOOPS_ROOT_PATH."/header.php";
    xoops_confirm(array('post_id' => $post_id, 'viewmode' => $viewmode, 'order' => $order, 'forum' => $forum, 'topic_id' => $topic_id, 'ok' => 2), 'delete.php', _MD_DEL_ONE);
    xoops_confirm(array('post_id' => $post_id, 'viewmode' => $viewmode, 'order' => $order, 'forum' => $forum, 'topic_id' => $topic_id, 'ok' => 1), 'delete.php', _MD_DEL_RELATED);
}
include XOOPS_ROOT_PATH.'/footer.php';
?>