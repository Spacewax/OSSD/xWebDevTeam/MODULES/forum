<?php
/**
* $Id: about.php,v 1.1 2004/05/29 17:11:48 praedator Exp $
* Module: Smartfaq
* Version: v 0.5
* Release Date:
* Author: marcan
* Licence: GNU
*/

include( "admin_header.php" );


xoops_cp_header();

$module_handler =& xoops_gethandler('module');
$versioninfo =& $module_handler->get($xoopsModule->getVar('mid'));

adminmenu(-1, _MD_A_ABOUT . " " . $versioninfo->getInfo('name'));

// Left headings...
echo "<img src='".XOOPS_URL."/modules/".$xoopsModule->dirname()."/".$versioninfo->getInfo('image')."' alt='' hspace='10' vspace='10' align='left'></a>";
echo "<div style='margin-top: 10px; color: #33538e; margin-bottom: 4px; font-size: 18px; line-height: 18px; font-weight: bold; display: block;'>" . $versioninfo->getInfo('name') . " version " . $versioninfo->getInfo('version') . "</div>";
if  ( $versioninfo->getInfo('author_realname') != '')
{
	$author_name = $versioninfo->getInfo('author') . " (" . $versioninfo->getInfo('author_realname') . ")";
} else
{
	$author_name = $versioninfo->getInfo('author');
}

echo "<div style = 'line-height: 16px; font-weight: bold; display: block;'>" . _MD_A_BY . " " .$author_name;
echo "</div>";
echo "<div style = 'line-height: 16px; display: block;'>" . $versioninfo->getInfo('license') . "</div><br><br></>\n";


// Author Information
echo "<table width='100%' cellspacing=1 cellpadding=3 border=0 class = outer>";
echo "<tr>";
echo "<td colspan='2' class='bg3' align='left'><b>" . _MD_A_AUTHOR_INFO . "</b></td>";
echo "</tr>";

If ( $versioninfo->getInfo('$author_name') != '' )
{
	echo "<tr>";
	echo "<td class='head' width = '150px' align='left'>" ._MD_A_AUTHOR_NAME . "</td>";
	echo "<td class='even' align='left'>" . $author_name . "</td>";
	echo "</tr>";
}
If ( $versioninfo->getInfo('author_website_url') != '' )
{
	echo "<tr>";
	echo "<td class='head' width = '150px' align='left'>" . _MD_A_AUTHOR_WEBSITE . "</td>";
	echo "<td class='even' align='left'><a href='" . $versioninfo->getInfo('author_website_url') . "' target='blank'>" . $versioninfo->getInfo('author_website_name') . "</a></td>";
	echo "</tr>";
}
If ( $versioninfo->getInfo('author_email') != '' )
{
	echo "<tr>";
	echo "<td class='head' width = '150px' align='left'>" . _MD_A_AUTHOR_EMAIL . "</td>";
	echo "<td class='even' align='left'><a href='mailto:" . $versioninfo->getInfo('author_email') . "'>" . $versioninfo->getInfo('author_email') . "</a></td>";
	echo "</tr>";
}
If ( $versioninfo->getInfo('credits') != '' )
{
	echo "<tr>";
	echo "<td class='head' width = '150px' align='left'>" . _MD_A_AUTHOR_CREDITS . "</td>";
	echo "<td class='even' align='left'>" . $versioninfo->getInfo('credits') . "</td>";
	echo "</tr>";
}

echo "</table>";
echo "<br />\n";

// Module Developpment information
echo "<table width='100%' cellspacing=1 cellpadding=3 border=0 class = outer>";
echo "<tr>";
echo "<td colspan='2' class='bg3' align='left'><b>" . _MD_A_MODULE_INFO . "</b></td>";
echo "</tr>";

If ( $versioninfo->getInfo('status') != '' )
{
	echo "<tr>";
	echo "<td class='head' width = '200' align='left'>" . _MD_A_MODULE_STATUS . "</td>";
	echo "<td class='even' align='left'>" . $versioninfo->getInfo('status') . "</td>";
	echo "</tr>";
}

If ( $versioninfo->getInfo('demo_site_url') != '' )
{
	echo "<tr>";
	echo "<td class='head' align='left'>" . _MD_A_MODULE_DEMO . "</td>";
	echo "<td class='even' align='left'><a href='" . $versioninfo->getInfo('demo_site_url') . "' target='blank'>" . $versioninfo->getInfo('demo_site_name') . "</a></td>";
	echo "</tr>";
}

If ( $versioninfo->getInfo('support_site_url') != '' )
{
	echo "<tr>";
	echo "<td class='head' align='left'>" . _MD_A_MODULE_SUPPORT . "</td>";
	echo "<td class='even' align='left'><a href='" . $versioninfo->getInfo('support_site_url') . "' target='blank'>" . $versioninfo->getInfo('support_site_name') . "</a></td>";
	echo "</tr>";
}

If ( $versioninfo->getInfo('submit_bug') != '' )
{
	echo "<tr>";
	echo "<td class='head' align='left'>" . _MD_A_MODULE_BUG . "</td>";
	echo "<td class='even' align='left'><a href='" . $versioninfo->getInfo('submit_bug') . "' target='blank'>" . "Submit a Bug in newBB Bug Tracker" . "</a></td>";
	echo "</tr>";
}
If ( $versioninfo->getInfo('submit_feature') != '' )
{
	echo "<tr>";
	echo "<td class='head' align='left'>" . _MD_A_MODULE_FEATURE . "</td>";
	echo "<td class='even' align='left'><a href='" . $versioninfo->getInfo('submit_feature') . "' target='blank'>" . "Request a feature in the newBB Feature Tracker" . "</a></td>";
	echo "</tr>";
}

echo "</table>";

// Warning
If ( $versioninfo->getInfo('warning') != '' )
{
	echo "<br />\n";
	echo "<table width='100%' cellspacing=1 cellpadding=3 border=0 class = outer>";
	echo "<tr>";
	echo "<td class='bg3' align='left'><b>" . _MD_A_MODULE_DISCLAIMER . "</b></td>";
	echo "</tr>";
	
	echo "<tr>";
	echo "<td class='even' align='left'>" . $versioninfo->getInfo('warning') . "</td>";
	echo "</tr>";
	
	echo "</table>";
}


// Author's note
If ( $versioninfo->getInfo('author_word') != '' )
{
	echo "<br />\n";
	echo "<table width='100%' cellspacing=1 cellpadding=3 border=0 class = outer>";
	echo "<tr>";
	echo "<td class='bg3' align='left'><b>" . _MD_A_AUTHOR_WORD . "</b></td>";
	echo "</tr>";
	
	echo "<tr>";
	echo "<td class='even' align='left'>" . $versioninfo->getInfo('author_word') . "</td>";
	echo "</tr>";
	
	echo "</table>";
}

xoops_cp_footer();

?>