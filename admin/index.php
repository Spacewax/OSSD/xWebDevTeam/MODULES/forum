<?php
// $Id: index.php,v 1.5 2004/05/29 17:11:48 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //
include('admin_header.php');

include_once(XOOPS_ROOT_PATH . "/modules/" . $xoopsModule->dirname() . "/class/class.forumpost.php");


$op = '';
$ok = 0;
if ( isset( $_GET['post_id'])) $post_id = intval($_GET['post_id']);
if ( isset( $_GET['ok'])) $ok = intval($_GET['ok']);
if ( isset( $_POST['ok']))  $ok = intval($_POST['ok']);
if ( isset( $_GET['approved'])) $approved = $_GET['approved'];
if ( isset( $_POST['post_id'])) $post_id = intval($_POST['post_id']);
if ( isset( $_GET['op'])) $op = $_GET['op'];
if ( isset( $_POST['op'])) $op = $_POST['op'];

switch ( $op )
        {

        case "del":
        echo $ok;
        echo $post_id;
        if ( !empty($ok) ) {
                if ( !empty($post_id) ) {
                $post = new ForumPosts($post_id);

                if($ok==2){
                $post->delete_one();
                }
                sync($post->forum(), "forum");
                sync($post->topic(), "topic");
        }
        if ( $post->istopic() ) {
                redirect_header("index.php", 2, _MD_A_POSTSDELETED);
                exit();
        } else {
                redirect_header("index.php", 2, _MD_A_POSTSDELETED);
                exit();
        }
        } else {
        xoops_cp_header();
        xoops_confirm(array('post_id' => $post_id, 'op' => 'del', 'ok' => 2), 'index.php', _MD_A_DEL_ONE);
        xoops_cp_footer();
        }
        exit();
        break;

        case "approve":

        if ($post_id)
        {
          if ($xoopsDB->queryF("UPDATE " . $xoopsDB->prefix("bb_posts") . " SET approved = '".$approved."'  WHERE post_id = '".$post_id."'" ) )
           {
            redirect_header( "index.php", 1, _MD_A_USERUPDATED );
           }
           else
           {
            redirect_header( "index.php", 1, _MD_A_USERNOTUPDATED );
           }
         }
         exit();
         break;

        case "mod":

        if ( empty($post_id) ) {
                redirect_header("viewforum.php?forum=$forum", 2, _MD_ERRORPOST);
                 exit();
        } else {

        xoops_cp_header();
        adminmenu(0,"" );

        $forumpost = new ForumPosts($post_id);
        $post_id2 = $forumpost->pid;
        $editerror = false;

        $nohtml = $forumpost->nohtml();
        $nosmiley = $forumpost->nosmiley();
        $icon = $forumpost->icon();
        $attachsig = $forumpost->attachsig();
        $topic_id=$forumpost->topic();
        if ( $forumpost->istopic() ) {
                $istopic = 1;
        } else {
                $istopic = 0;
        }
        $subject=$forumpost->subject("Edit");
        $message=$forumpost->text("Edit");
        $attachment=$forumpost->attachment("Show");
        $hidden = "";

        include '../include/adminforumform.inc.php';
        xoops_cp_footer();
        }

        exit();
        break;

        case "default":
        default:
        
        xoops_cp_header();

        adminmenu(0,"Index" );

        echo "<br><br>";


        echo "<fieldset><legend style='font-weight: bold; color: #900;'>" . _MD_A_BOARDSUMMARY . "</legend>";
        echo "<div style='padding: 12px;'>" . _MD_A_TOTALUSER . " <b>".get_total_users()."</b> | ";
        echo _MD_A_TOTALTOPICS . " <b>".get_total_topics()."</b> | ";
        echo _MD_A_TOTALPOSTS . " <b>".get_total_posts(0, 'all')."</b> | ";
        echo _MD_A_TOTALVIEWS . " <b>".get_total_views()."</b></div>";
        echo "</fieldset><br />";
echo "<fieldset><legend style='font-weight: bold; color: #900;'>" . _MD_A_PENDING_POSTS_FOR_AUTH . "</legend><br />";


$sql = "SELECT p.*, t.post_text FROM ".$xoopsDB->prefix("bb_posts")." p LEFT JOIN ".$xoopsDB->prefix("bb_posts_text")." t ON p.post_id=t.post_id LEFT JOIN ".$xoopsDB->prefix("bb_topics")." tp ON tp.topic_id=p.topic_id WHERE p.approved='0'";

//$sql = "SELECT * FROM " . $xoopsDB->prefix("bb_posts") . " WHERE approved='0'";
$result = $xoopsDB ->query($sql);
$numrows = $xoopsDB ->getRowsNum($result);
echo "<table width='100%' cellspacing=1 cellpadding=3 border=0 class = outer>";
        echo "<tr>";
        echo "<td width='10%' class='bg3' align='center'><b>" . _MD_A_POSTID . "</b></td>";
        echo "<td width='20%' class='bg3' align='center'><b>" . _MD_A_POSTER . "</b></td>";
        echo "<td width='40%'class='bg3' align='center'><b>" . _MD_A_SUBJECT . "</b></td>";
        echo "<td width='15%' class='bg3' align='center'><b>" . _MD_A_POSTDATE . "</b></td>";
        echo "<td width='15%' class='bg3' align='center'><b>" . _MD_A_ACTION . "</b></td>";
        echo "</tr>";
        if ( $numrows > 0 ) // That is, if there ARE columns in the system
                        {
                        while ( $post = $xoopsDB -> fetchArray($result) )
                                {
                                $user = getLinkedUnameFromId( $post['uid'], 0 );
                                $modify = "<a href='index.php?op=mod&post_id=" . $post['post_id'] . "'><img src=" . XOOPS_URL . "/modules/" . $xoopsModule->dirname() . "/images/edit.gif ALT='"._EDIT."'></a>";
                                $delete = "<a href='index.php?op=del&post_id=" . $post['post_id'] . "'><img src=" . XOOPS_URL . "/modules/" . $xoopsModule->dirname() . "/images/delete.gif ALT='"._DELETE."'></a>";
                                $approve = "<a href='index.php?op=approve&post_id=" . $post['post_id'] . "&approved=1'><img src=" . XOOPS_URL . "/modules/" . $xoopsModule->dirname() . "/images/approve.gif ALT='"._MD_A_APPROVE."'></a>";
                                $text = $myts->displayTarea($post['post_text'],1,1,1);
                                echo "<tr>";
                                echo "<td class='head' align='center'>" . $post['post_id'] . "</td>";
                                echo "<td colspan='4'><table width='100%'><tr>";
                                echo "<td width='20%' class='even' align='center'>" . $user . "</td>";
                                echo "<td width='40%' class='even' align='center'>" . $post['subject'] . "</td>";
                                echo "<td width='15%' class='even' align='center'>" . formatTimestamp($post['post_time']) . "</td>";
                                echo "<td width='15%' class='even' align='center'> $modify $approve $delete </td>";
                                echo "</tr>";
                                echo "<tr>";
                                echo "<td colspan='4' class='head' align='center'>" . _MD_A_APPROVETEXT . "</td>";
                                echo "</tr><tr>";
                                echo "<td colspan='4' class='odd' align='center'>" . $text . "</td>";
                                echo "</tr></table></td>";
                                echo "</tr>";
                                }
                        }
                else // that is, $numrows = 0, there's no columns yet
                        {
                        echo "<tr>";
                        echo "<td class='head' align='center' colspan= '5'>"._MD_A_NOAPPROVEPOST."</td>";
                        echo "</tr>";

                        }

        echo "</table>\n";
        echo "</fieldset>";

echo "<br><br>";

xoops_cp_footer();
break;
}

?>