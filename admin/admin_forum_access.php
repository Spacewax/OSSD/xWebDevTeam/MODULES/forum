<?php
// $Id: admin_forum_access.php,v 1.4 2004/05/29 17:11:48 praedator Exp $
//  ------------------------------------------------------------------------ //
//                    xcGallery - XOOPS Gallery Modul                        //
//                    Copyright (c) 2003 Derya Kiran                         //
//                           meeresstille@gmx.de                             //
//         http://www.myxoopsforge.org/modules/xfmod/project/?xcgal          //
//  ------------------------------------------------------------------------ //
//  Based on Coppermine Photo Gallery 1.10                                   //
//  (http://coppermine.sourceforge.net/)                                     //
//  developed by Gr�gory DEMAR                                               //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //

include "admin_header.php";

if (isset($_GET['forum_id'])) {$forum = $_GET['forum_id'];}
if (isset($_POST['forum_id'])) {$forum = $_POST['forum_id'];}

$member_handler =& xoops_gethandler('member');
$groups =&$member_handler->getGroups();
function synchronize(){
	global $xoopsDB, $groups,$member_handler;
	    $group_exist2=array();
	foreach ($groups as $gr){
	    $group_exist2[$gr->getVar('groupid')] = FALSE;
        }
	$result = $xoopsDB->query("SELECT * FROM ".$xoopsDB->prefix("bb_group_access")." WHERE 1 ORDER BY group_id");

      while($myrow = $xoopsDB->fetchArray($result)) {
            $group_exist1 = FALSE;
                foreach ($groups as $group){
                if ($myrow['forum_groupid'] == $group->getVar('groupid')){
                    if ($myrow['group_name']!= $group->getVar('name')){
                        $xoopsDB->query("UPDATE ".$xoopsDB->prefix("bb_group_access")." SET group_name = '".$group->getVar('name')."' WHERE forum_groupid = '".(int)$myrow['forum_groupid']."'");
                    }
                    $group_exist1 = TRUE;
                    $group_exist2[$group->getVar('groupid')] = TRUE;
                }
                
            }
            if (!$group_exist1){
                $xoopsDB->query("DELETE FROM ".$xoopsDB->prefix("bb_group_access")." WHERE forum_groupid = '".(int)$myrow['forum_groupid']."'");
                }
      }
     // for ($i = 0; $i < count($group_exist2); $i++) {
        foreach($group_exist2 as $key=>$value){
          if (!$value){
              $group = &$member_handler->getGroup($key);
              $result=$xoopsDB->query("INSERT INTO ".$xoopsDB->prefix("bb_group_access")." VALUES (".$key.", '".$group->getVar('name')."', ".$forum.", 0, 1, 1, 1, 1, 1, 1, 1, 1, ".$key.")");
              }
              
      }
      

}
function display_group_list()
{
	global $xoopsDB, $tdstyle,$buffer,$groups,$forum;
	
	$count = count($groups);
	$result = $xoopsDB->query("SELECT * FROM ".$xoopsDB->prefix("bb_group_access")." WHERE forum_id=$forum ORDER BY group_id");

    if (!$xoopsDB->getRowsNum($result)) {
	    
	    //print_r($groups);
	    for ($i = 0; $i < $count; $i++) {
	        $id = $groups[$i]->getVar('groupid');
            $nextid = $xoopsDB->genId($xoopsDB->prefix("bb_group_access")."_forum_access_id_seq");

	        if (XOOPS_GROUP_ADMIN == $id ){
                $result=$xoopsDB->queryF("INSERT INTO ".$xoopsDB->prefix("bb_group_access")." VALUES (".$nextid.", '".$groups[$i]->getVar('name')."', ".$forum.", 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)");
            }
            elseif (XOOPS_GROUP_USERS == $id){
                $result=$xoopsDB->queryF("INSERT INTO ".$xoopsDB->prefix("bb_group_access")." VALUES (".$nextid.", '".$groups[$i]->getVar('name')."', ".$forum.", 0, 1, 1, 1, 1, 0, 0, 0, 0, 2)");

            }
            elseif (XOOPS_GROUP_ANONYMOUS == $id) {
                $result=$xoopsDB->queryF("INSERT INTO ".$xoopsDB->prefix("bb_group_access")." VALUES (".$nextid.", '".$groups[$i]->getVar('name')."', ".$forum.", 0, 1, 1, 1, 1, 0, 0, 0, 0, 3)");

            }
            else {
                $result=$xoopsDB->queryF("INSERT INTO ".$xoopsDB->prefix("bb_group_access")." VALUES (".$nextid.", '".$groups[$i]->getVar('name')."', ".$forum.", 0, 1, 1, 1, 1, 0, 0, 0, 0, ".$id.")");

            }
            
        }

	    redirect_header('admin_forum_access.php?forum='.$forum.'',2,_AM_GRPMGR_EMPTY);
	   exit;
	}

	$field_list = array('can_view', 'can_post', 'can_reply', 'can_edit', 'can_delete', 'can_addpoll', 'can_vote', 'can_attach');
    $tdstyle ="even";
    echo $buffer;
	while($group=$xoopsDB->fetchArray($result)){
		$group['group_name'] = $group['group_name'];
        if ($tdstyle== "even") $tdstyle = "odd";
        else $tdstyle = "even";
			echo <<< EOT
	<tr>
 		<td class="{$tdstyle}">
			<input type="hidden" name="group_id[]" value="{$group['group_id']}">
			{$group['group_name']}
		</td>
EOT;

		foreach ($field_list as $field_name){
			$value = $group[$field_name];
			$yes_selected = ($value == 1) ? 'selected="selected"' : '';
			$no_selected  = ($value == 0) ? 'selected="selected"' : '';
			echo <<< EOT
		<td class="{$tdstyle}" align="center">
			<select name="{$field_name}_{$group['group_id']}" class="listbox">
EOT;
            echo "<option value='1' {$yes_selected}>"._YES."</option>";
            echo "<option value='0' {$no_selected}>"._NO."</option></select></td>";
		}
		echo "</tr>";
	} // while
	$xoopsDB->freeRecordSet($result);
}

function get_post_var($var)
{
	global $_POST;

	if(!isset($_POST[$var])) redirect_header('index.php',2,_AM_PARAM_MISSING." ($var)");
	return $_POST[$var];
}

function process_post_data()
{
	global $_POST, $xoopsDB;

	$field_list = array('can_view', 'can_post', 'can_reply', 'can_edit', 'can_delete', 'can_addpoll', 'can_vote', 'can_attach');

	$group_id_array = get_post_var('group_id');
	foreach ($group_id_array as $key => $group_id){
		$set_statment = '';
		foreach ($field_list as $field){
			if (!isset($_POST[$field.'_'.$group_id])) redirect_header('groupmgr.php',2,_AM_PARAM_MISSING." ({$field}_{$group_id})");
			if ($field == 'group_name') {
			    $set_statment .= $field ."='".addslashes($_POST[$field.'_'.$group_id])."',";
			} else {
				$set_statment .= $field ."='".(int)$_POST[$field.'_'.$group_id]."',";
			}
		}
		$set_statment = substr($set_statment, 0, -1);
		$xoopsDB->query("UPDATE ".$xoopsDB->prefix("bb_group_access")." SET $set_statment WHERE group_id = '$group_id' LIMIT 1");
	}
}

if (isset($_POST) && count($_POST)) {
    if (isset($_POST['synchronize'])) {
           synchronize();
	} elseif (isset($_POST['apply_modifs'])) {
		process_post_data();
	}
}

$sql = "SELECT * FROM ".$xoopsDB->prefix("bb_forums")." WHERE forum_id=$forum";
$result = $xoopsDB->query($sql);
if( $forum_result = $xoopsDB->fetchArray($result))
{

xoops_cp_header();
adminmenu(1,_MD_A_PERMISSIONS_FORUM.$forum_result['forum_name']);
echo "<fieldset><legend style='font-weight: bold; color: #900;'>" .  _MD_A_PERMISSIONS_FORUM  . "</legend>";
        echo"<br><br><table width='100%' border='0' cellspacing='1' class='outer'>"
        ."<tr><td class=\"odd\">";

ob_start();
$tform = new XoopsThemeForm( _MD_A_PERMISSIONS_FORUM.' '.$forum_result['forum_name'],"",xoops_getenv( 'PHP_SELF' ));
$member_handler =& xoops_gethandler('member');
$group_list =& $member_handler->getGroupList();

$gperm_handler = & xoops_gethandler( 'groupperm' );

$groups_forum_can_view = $gperm_handler -> getGroupIds('forum_can_view', $forum_id, $xoopsModule->getVar('mid'));
$groups_forum_can_view_checkbox = new XoopsFormCheckBox(_MD_A_CAN_VIEW, 'groups_forum_can_view[]', $groups_forum_can_view);
foreach ($group_list as $group_id => $group_name) {
   $groups_forum_can_view_checkbox->addOption($group_id, $group_name);
}
$tform -> addElement( $groups_forum_can_view_checkbox );
$groups_forum_can_post = $gperm_handler -> getGroupIds('forum_can_post', $forum_id, $xoopsModule->getVar('mid'));
$groups_forum_can_post_checkbox = new XoopsFormCheckBox(_MD_A_CAN_POST, 'groups_forum_can_post[]', $groups_forum_can_post);
foreach ($group_list as $group_id => $group_name) {
   $groups_forum_can_post_checkbox->addOption($group_id, $group_name);
}
$tform -> addElement( $groups_forum_can_post_checkbox );
$groups_forum_can_reply = $gperm_handler -> getGroupIds('forumy_can_reply', $forum_id, $xoopsModule->getVar('mid'));
$groups_forum_can_reply_checkbox = new XoopsFormCheckBox(_MD_A_CAN_REPLY, 'groups_forum_can_reply[]', $groups_forum_can_reply);
foreach ($group_list as $group_id => $group_name) {
   $groups_forum_can_reply_checkbox->addOption($group_id, $group_name);
}
$tform -> addElement( $groups_forum_can_reply_checkbox );
$groups_forum_can_edit = $gperm_handler -> getGroupIds('forumy_can_edit', $forum_id, $xoopsModule->getVar('mid'));
$groups_forum_can_edit_checkbox = new XoopsFormCheckBox(_MD_A_CAN_EDIT, 'groups_forum_can_edit[]', $groups_forum_can_edit);
foreach ($group_list as $group_id => $group_name) {
   $groups_forum_can_edit_checkbox->addOption($group_id, $group_name);
}
$tform -> addElement( $groups_forum_can_edit_checkbox );
$groups_forum_can_delete = $gperm_handler -> getGroupIds('forumy_can_delete', $forum_id, $xoopsModule->getVar('mid'));
$groups_forum_can_delete_checkbox = new XoopsFormCheckBox(_MD_A_CAN_DELETE, 'groups_forum_can_delete[]', $groups_forum_can_delete);
foreach ($group_list as $group_id => $group_name) {
   $groups_forum_can_delete_checkbox->addOption($group_id, $group_name);
}
$tform -> addElement( $groups_forum_can_delete_checkbox );
$groups_forum_can_addpoll = $gperm_handler -> getGroupIds('forumy_can_addpoll', $forum_id, $xoopsModule->getVar('mid'));
$groups_forum_can_addpoll_checkbox = new XoopsFormCheckBox(_MD_A_CAN_ADDPOLL, 'groups_forum_can_addpoll[]', $groups_forum_can_addpoll);
foreach ($group_list as $group_id => $group_name) {
   $groups_forum_can_addpoll_checkbox->addOption($group_id, $group_name);
}
$tform -> addElement( $groups_forum_can_addpoll_checkbox );
$groups_forum_can_attach = $gperm_handler -> getGroupIds('forumy_can_attach', $forum_id, $xoopsModule->getVar('mid'));
$groups_forum_can_attach_checkbox = new XoopsFormCheckBox(_MD_A_CAN_ATTACH, 'groups_forum_can_attach[]', $groups_forum_can_attach);
foreach ($group_list as $group_id => $group_name) {
   $groups_forum_can_attach_checkbox->addOption($group_id, $group_name);
}
$tform -> addElement( $groups_forum_can_attach_checkbox );
$tform->display();






echo "<table border='0' cellpadding='0' cellspacing='1' width='100%' class='outer'>
    <tr>
		<td class='head'>"._MD_A_GROUP_NAME."</td>
		<td class='head' align='center'>"._MD_A_CAN_VIEW."</td>
		<td class='head' align='center'>"._MD_A_CAN_POST."</td>
		<td class='head' align='center'>"._MD_A_CAN_REPLY."</td>
		<td class='head' align='center'>"._MD_A_CAN_EDIT."</td>
		<td class='head' align='center'>"._MD_A_CAN_DELETE."</td>
		<td class='head' align='center'>"._MD_A_CAN_ADDPOLL."</td>
        <td class='head' align='center'>"._MD_A_CAN_VOTE."</td>
		<td class='head' align='center'>"._MD_A_CAN_ATTACH."</td>
	</tr>
	<form method=\"post\" action=\"{$_SERVER['PHP_SELF']}\">";

$buffer = ob_get_contents();
ob_end_clean();

display_group_list();
if ($tdstyle == "even") $tdstyle = "odd";
else $tdsytle = "even";

echo "<tr><td colspan='9' class='{$tdstyle}'>"._MD_A_GRPMGR_SYN_NOTE."</td></tr>";
echo "<tr><td colspan='9' align='center' class='foot'>";
echo "<input type=\"hidden\" name=\"forum\" value=\"".$forum."\">";
echo "<input type=\"submit\" name=\"apply_modifs\" value=\""._MD_A_GRPMGR_APPLY."\" class=\"button\" />&nbsp;&nbsp;&nbsp;";
echo "<input type=\"submit\" name=\"synchronize\" value=\""._MD_A_GRPMGR_SYN."\" class=\"button\" />&nbsp;&nbsp;&nbsp;";
echo "<input type=\"submit\" name=\"addtoall\" value=\""._MD_A_GRPMGR_ADDTOALL."\" class=\"button\" />&nbsp;&nbsp;&nbsp;";
echo "</td></form></tr></table>";

echo"</td></tr></table>";
echo "</fieldset>";


xoops_cp_footer();
}
else
{
    redirect_header("./index.php", 1, _MD_A_DATABASEERROR);
    exit();
}

?>
