<?php
// $Id: admin_cat_manager.php,v 1.6 2004/05/29 17:11:47 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //

include('admin_header.php');

include_once(XOOPS_ROOT_PATH . "/modules/" . $xoopsModule->dirname() . "/class/class.category.php");

$op = '';

if (isset($_GET['op'])) $op = $_GET['op'];
if (isset($_POST['op'])) $op = $_POST['op'];
if (isset($_POST['default'])) $op = 'default';
if (isset($_GET['cat_id'])) $cat_id = $_GET['cat_id'];
if (isset($_POST['cat_id'])) $cat_id = $_POST['cat_id'];

/**
* newCategory()
*
* @param integer $catid
* @return
*/
function newCategory(){
	editCategory();
}

/**
* editCategory()
*
* @param integer $catid
* @return
*/
function editCategory($cat_id = 0){
	$fc = new ForumCat($cat_id);
	$groups_cat_access = null;
	global $xoopsModule;
	
	if ($cat_id){
		$sform = new XoopsThemeForm(_MD_A_EDITCATEGORY . " " . $fc->cat_title, "op", xoops_getenv('PHP_SELF'));
		
	}else{
		$sform = new XoopsThemeForm(_MD_A_CREATENEWCATEGORY, "op", xoops_getenv('PHP_SELF'));
		$fc->cat_title = '';
		$fc->cat_image = 'blank.gif';
		$fc->cat_description = '';
		$fc->cat_order = 0;
		$fc->cat_state = 0;
		$fc->cat_showdescript = 1;
		$fc->cat_url = 'http://www.xoops.org';
	}
	// READ PERMISSIONS
	$member_handler =& xoops_gethandler('member');
	$group_list =& $member_handler->getGroupList();
	
	$gperm_handler = & xoops_gethandler( 'groupperm' );
	$groups_cat_access = $gperm_handler -> getGroupIds('forum_cat_access', $cat_id, $xoopsModule->getVar('mid'));
	
	$groups_cat_access_checkbox = new XoopsFormCheckBox(_MD_A_ACCESSLEVEL, 'groups_cat_access[]', $groups_cat_access);
	foreach ($group_list as $group_id => $group_name) {
		$groups_cat_access_checkbox->addOption($group_id, $group_name);
	}
	$sform -> addElement( $groups_cat_access_checkbox );
	
	$sform->addElement(new XoopsFormText(_MD_A_SETCATEGORYORDER, 'cat_order', 5, 10, $fc->cat_order), false);
	$sform->addElement(new XoopsFormText(_MD_A_CATEGORY, 'title', 50, 80, $fc->cat_title), true);
	$sform->addElement(new XoopsFormDhtmlTextArea(_MD_A_CATEGORYDESC, 'catdescript', $fc->cat_description, 10, 60), true);
	
	$displaydescription_radio = new XoopsFormRadioYN( _MD_A_SHOWDESC, 'show', $fc->cat_showdescript,'' . _YES . '', ' ' . _NO . '' );
	$sform -> addElement( $displaydescription_radio );
	
	$setvisible_select = new XoopsFormSelect(_MD_A_STATE, "state", $fc->cat_state);
	$setvisible_select->addOptionArray(array('0' => _MD_A_PUBLIC, '1' => _MD_A_HIDDEN));
	$sform->addElement($setvisible_select);
	
	$imgdir = "/modules/" . $xoopsModule->dirname() . "/images/category";
	if (!isset($fc->cat_image)) $fc->cat_image = 'blank.gif';
	$graph_array = &XoopsLists::getImgListAsArray(XOOPS_ROOT_PATH . $imgdir);
	$indeximage_select = new XoopsFormSelect('', 'indeximage',$fc->cat_image);
	$indeximage_select->addOptionArray($graph_array);
	$indeximage_select->setExtra("onchange='showImgSelected(\"image\",\"indeximage\", \"" . $imgdir . "\", \"\", \"".XOOPS_URL ."\")'");
	$indeximage_tray = new XoopsFormElementTray(_MD_A_SPONSORIMAGE, '&nbsp;');
	$indeximage_tray->addElement($indeximage_select);
	$indeximage_tray->addElement(new XoopsFormLabel('', "<br /><imgsrc='" . XOOPS_URL . $imgdir. "/" . $fc->cat_image . "'name='image' id='image' alt='' />"));
	$sform->addElement($indeximage_tray);
	
	$sform->addElement(new XoopsFormText(_MD_A_SPONSORLINK, 'sponurl', 50, 80, $fc->cat_url), true);
	$sform->addElement(new XoopsFormHidden('cat_id', $cat_id));
	
	$button_tray = new XoopsFormElementTray('', '');
	$button_tray->addElement(new XoopsFormHidden('op', 'save'));
	
	$butt_save = new XoopsFormButton('', '', _SUBMIT, 'submit');
	$butt_save->setExtra('onclick="this.form.elements.op.value=\'save\'"');
	$button_tray->addElement($butt_save);
	if ($cat_id){
		$butt_delete = new XoopsFormButton('', '', _CANCEL, 'submit');
		$butt_delete->setExtra('onclick="this.form.elements.op.value=\'default\'"');
		$button_tray->addElement($butt_delete);
	}
	$sform->addElement($button_tray);
	$sform->display();
}

switch($op){
	
	
	case "manage":
	
	
	$fc = new ForumCat();
	$categorys = $fc->getFirstChild();
	if (empty($categorys)){
		xoops_cp_header();
		adminmenu(2,_MD_A_CREATENEWCATEGORY);
		echo "<fieldset><legend style='font-weight: bold; color: #900;'>" . _MD_A_CREATENEWCATEGORY  . "</legend>";
		echo "<br />";
		
		newCategory();
		
		echo "</fieldset>";
		xoops_cp_footer();
		
		break;
	}
	xoops_cp_header();
	
	adminmenu(2,_MD_A_CATADMIN);
	echo "<fieldset><legend style='font-weight: bold; color: #900;'>" .  _MD_A_CATADMIN . "</legend>";
	echo"<br />";
	echo "<a style='border: 1px solid #5E5D63; color: #000000; font-family: verdana, tahoma, arial, helvetica, sans-serif; font-size: 1em; padding: 4px 8px; text-align:center;' href='admin_cat_manager.php'>"._MD_A_CREATENEWCATEGORY."</a><br /><br />";
	
	
	echo "<table border='0' cellpadding='4' cellspacing='1' width='100%' class='outer'>";
	echo "<tr align='center'>";
	echo "<td class='bg3'>"._MD_A_CATEGORY1."</td>";
	echo "<td class='bg3'>"._MD_A_VISIBLE."</td>";
	echo "<td class='bg3'>"._MD_A_EDIT."</td>";
	echo "<td class='bg3'>"._MD_A_DELETE."</td>";
	echo "</tr>";
	
	
	foreach($categorys as $onecat){
		if (!$onecat->cat_state){
			$status = _MD_A_PUBLIC;
		}else{
			$status = _MD_A_HIDDEN;
		}
		$cat_edit_link = "<a href=\"admin_cat_manager.php?op=mod&cat_id=" . $onecat->cat_id . "\"><img src=\"../images/edit.gif\"></a>";
		$cat_del_link = "<a href=\"admin_cat_manager.php?op=del&cat_id=" . $onecat->cat_id . "\"><img src=\"../images/delete.gif\"></a>";
		
		echo "<tr class='odd' align='left'>";
		echo "<td width='60%'>" . $onecat->textLink() . "</td>";
		echo "<td width='10%' align='center' >" . $status . "</td>";
		echo "<td width='10%' align='center' >".$cat_edit_link."</td>";
		echo "<td width='10%' align='center' >".$cat_del_link."</td>";
		echo "</tr>";
	}
	echo "</table>";
	echo "</fieldset>";
	xoops_cp_footer();
	break;
	
	
	case "mod":
	$fc = new ForumCat($cat_id);
	xoops_cp_header();
	adminmenu(2,_MD_A_EDITCATEGORY.$fc->cat_title );
	echo "<fieldset><legend style='font-weight: bold; color: #900;'>" .  _MD_A_EDITCATEGORY  . "</legend>";
	echo"<br />";
	
	
	editCategory($cat_id);
	
	echo "</fieldset>";
	xoops_cp_footer();
	break;
	
	case "del":
	if (isset($_POST['confirm']) != 1){
		xoops_cp_header();
		xoops_confirm( array( 'op' => 'del', 'cat_id' => intval( $_GET['cat_id'] ), 'confirm' => 1 ), 'admin_cat_manager.php', _MD_A_WAYSYWTDTTAL );
		xoops_cp_footer();
	}else{
		$fc = new ForumCat($_POST['cat_id']);
		$fc->delete();
		
		
		redirect_header("admin_cat_manager.php", 1, _MD_A_CATEGORYDELETED);
		exit();
		
	}
	exit();
	
	
	case "save":
	
	if ($cat_id){
		$fc = new ForumCat($cat_id);
		$message=_MD_A_CATEGORYUPDATED;
	}else{
		$fc = new ForumCat();
		$message=_MD_A_CATEGORYCREATED;
	}
	
	$fc->setcat_title($_POST['title']);
	$fc->setcat_image($_POST['indeximage']);
	$fc->setcat_order($_POST['cat_order']);
	$fc->setcat_description($_POST['catdescript']);
	$fc->setcat_state($_POST['state']);
	$fc->setcat_url($_POST['sponurl']);
	$fc->setcat_showdescript($_POST['show']);
	$fc->setcat_groups_cat_access($_POST['groups_cat_access']);
	
	$fc->store();
	redirect_header("admin_cat_manager.php", 1, $message);
	exit();
	
	
	case "default":
	default:
	xoops_cp_header();
	adminmenu(2,_MD_A_CREATENEWCATEGORY);
	echo "<fieldset><legend style='font-weight: bold; color: #900;'>" . _MD_A_CREATENEWCATEGORY  . "</legend>";
	echo "<br />";
	
	newCategory();
	
	echo "</fieldset>";
	xoops_cp_footer();
}
?>