<?php
// $Id: admin_mod_user.php,v 1.3 2004/05/29 17:11:48 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //
include 'admin_header.php';

$op = '';

if ( isset( $_GET['op'] ) ) $op = $_GET['op'];
if ( isset( $_POST['op'] ) ) $op = $_POST['op'];
if (isset($_GET['uid'])) $uid = $_GET['uid'];
if (isset($_POST['uid'])) $uid = $_POST['uid'];
if (isset($_GET['id'])) $id = $_GET['id'];
if (isset($_POST['id'])) $id = $_POST['id'];


/**
 * newUser()
 *
 * @param integer $catid
 * @return
 */
function newUser(){
   editUser();
}

/**
 * editUser()
 *
 * @param integer $catid
 * @return
 */

function editUser($uid = 0){


    global $xoopsDB;
    $user=0;
    if ($uid){
        $sform = new XoopsThemeForm(_MD_A_EDITMODUSER . " " . getLinkedUnameFromId( $uid, 0 ), "op", xoops_getenv('PHP_SELF'));
        $sql = "SELECT * FROM " . $xoopsDB->prefix("bb_moderate_user") . " WHERE uid = '".$uid."'";
        $result = $xoopsDB ->query($sql);
        $user = $xoopsDB ->fetchArray($result);
        $button_tray = new XoopsFormElementTray('', '');
        $button_tray->addElement(new XoopsFormHidden('op', 'modsave'));
        $sform->addElement(new XoopsFormText(_MD_A_USERUID, 'uid', 5, 10, $uid), false);
        $sform->addElement(new XoopsFormDhtmlTextArea(_MD_A_REASON, 'reason', $user['reason'], 10, 60), true);

        $butt_save = new XoopsFormButton('', '', _SUBMIT, 'submit');
        $butt_save->setExtra('onclick="this.form.elements.op.value=\'modsave\'"');
        $button_tray->addElement($butt_save);

    }else{
        $sform = new XoopsThemeForm(_MD_A_CREATENEWMODUSER, "op", xoops_getenv('PHP_SELF'));

        $button_tray = new XoopsFormElementTray('', '');
        $button_tray->addElement(new XoopsFormHidden('op', 'save'));
        $sform->addElement(new XoopsFormText(_MD_A_USERUID, 'uid', 5, 10, $uid), false);
        $sform->addElement(new XoopsFormDhtmlTextArea(_MD_A_REASON, 'reason', $user['reason'], 10, 60), true);

        $butt_save = new XoopsFormButton('', '', _SUBMIT, 'submit');
        $butt_save->setExtra('onclick="this.form.elements.op.value=\'save\'"');
        $button_tray->addElement($butt_save);

    }
    
    if ($uid){
        $butt_delete = new XoopsFormButton('', '', _CANCEL, 'submit');
        $butt_delete->setExtra('onclick="this.form.elements.op.value=\'default\'"');
        $button_tray->addElement($butt_delete);
    }
    $sform->addElement(new XoopsFormHidden('id', $user['id']));
    $sform->addElement($button_tray);
    $sform->display();
}

switch ( $op )
        {

        case "del":
          if (isset($_POST['confirm']) != 1){
             xoops_cp_header();
             xoops_confirm( array( 'op' => 'del', 'uid' => intval( $_GET['uid'] ), 'confirm' => 1 ), 'admin_mod_user.php', _MD_A_MODWAYSYWTDTTAL );
             xoops_cp_footer();
          }else{
            $sql = "DELETE FROM " . $xoopsDB->prefix("bb_moderate_user") . " WHERE uid=" . $uid . "";
            $xoopsDB->query( $sql );

            redirect_header("admin_mod_user.php", 1, _MD_A_USERDELETED);
            exit();

        }
        exit();
        

        case "mod":

        xoops_cp_header();
        adminmenu(8,_MD_A_EDITMODUSER." - ".getLinkedUnameFromId( $uid, 0 ));
        echo "<fieldset><legend style='font-weight: bold; color: #900;'>" . _MD_A_EDITMODUSER  . "</legend>";
        echo "<br><br><table width='100%' border='0' cellspacing='1' class='outer'><tr><td class=\"odd\">";

        editUser($uid);

        echo"</td></tr></table>";
        echo "</fieldset>";
        xoops_cp_footer();
        break;

        case "add":

        xoops_cp_header();
        adminmenu(8,_MD_A_CREATENEWMODUSER);
        echo "<fieldset><legend style='font-weight: bold; color: #900;'>" . _MD_A_CREATENEWMODUSER  . "</legend>";
        echo "<br><br><table width='100%' border='0' cellspacing='1' class='outer'><tr><td class=\"odd\">";

        newUser();

        echo"</td></tr></table>";
        echo "</fieldset>";
        xoops_cp_footer();
        break;


        case "save":

        if ($uid)
                        {
                        $nextid = $xoopsDB->genId($xoopsDB->prefix("bb_moderate_user")."_moderate_user_id_seq");

                        if ($xoopsDB->queryF( "INSERT INTO " . $xoopsDB -> prefix( "bb_moderate_user" ) . "  VALUES (".$nextid.",'".$uid."', '".$_POST['reason']."')" ) )
                                {
                                redirect_header( "admin_mod_user.php", 1, _MD_A_USERADDED );

                                }
                        else
                                {
                                redirect_header( "index.php", 1, _MD_A_USERNOTUPDATED );
                                }
                        }

                exit();
                

        case "modsave":
        if ( $uid )
                        {

                        if ( $xoopsDB->queryF( "UPDATE " . $xoopsDB -> prefix( "bb_moderate_user" ) . " SET uid = '".$uid."', reason = '".$_POST['reason']."' WHERE id = '".$id."'" ) )
                                {
                                redirect_header( "admin_mod_user.php", 1, _MD_A_USERUPDATED );
                                }
                        else
                                {
                                redirect_header( "index.php", 1, _MD_A_USERNOTUPDATED );
                                }
                        }
                exit();
               

        case "default":
        default:


xoops_cp_header();

adminmenu(8,"" );

echo "<br><br>";

 
echo "<fieldset><legend style='font-weight: bold; color: #900;'>" . _MD_A_USERTOBEMODERATE . "</legend><br />";
echo "<a style='border: 1px solid #5E5D63; color: #000000; font-family: verdana, tahoma, arial, helvetica, sans-serif; font-size: 1em; padding: 4px 8px; text-align:center;' href='admin_mod_user.php?op=add'>"._MD_A_ADDMODUSER."</a><br /><br />";
$sql = "SELECT * FROM " . $xoopsDB->prefix("bb_moderate_user") . " ";
$result = $xoopsDB ->query($sql);
$numrows = $xoopsDB ->getRowsNum($result);

echo "<table width='100%' cellspacing=1 cellpadding=3 border=0 class = outer>";
        echo "<tr>";
        echo "<td width='30%' class='bg3' align='center'><b>" . _MD_A_USER . "</b></td>";
        echo "<td width='50%' class='bg3' align='center'><b>" . _MD_A_REASON . "</b></td>";
        echo "<td width='20%' class='bg3' align='center'><b>" . _MD_A_ACTION . "</b></td>";
        echo "</tr>";

        if ( $numrows > 0 ) // That is, if there ARE columns in the system
                        {
                        while ( list( $id, $uid, $reason ) = $xoopsDB -> fetchRow($result) )
                                {
                                $user = getLinkedUnameFromId( $uid, 0 );
                                $modify = "<a href='admin_mod_user.php?op=mod&uid=" . $uid . "'><img src=" . XOOPS_URL . "/modules/" . $xoopsModule->dirname() . "/images/edit.gif ALT='"._EDIT."'></a>";
                                $delete = "<a href='admin_mod_user.php?op=del&uid=" . $uid . "'><img src=" . XOOPS_URL . "/modules/" . $xoopsModule->dirname() . "/images/delete.gif ALT='"._DELETE."'></a>";

                                echo "<tr>";
                                echo "<td class='head' align='center'>" . $user . "</td>";
                                echo "<td class='even' align='left'>" . $reason . "</td>";
                                echo "<td class='even' align='center'> $modify $delete </td>";
                                echo "</tr>";
                                }
                        }
                else // that is, $numrows = 0, there's no columns yet
                        {
                        echo "<tr>";
                        echo "<td class='head' align='center' colspan= '3'>"._MD_A_NOUSER."</td>";
                        echo "</tr>";

                        }




        echo "</table>\n";
        echo "</fieldset>";

xoops_cp_footer();
break;
}

?>