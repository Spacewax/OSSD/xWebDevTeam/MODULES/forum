<?php
// $Id: admin_forum_manager.php,v 1.6 2004/05/29 17:11:48 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //

include 'admin_header.php';
include_once XOOPS_ROOT_PATH . '/modules/' . $xoopsModule ->dirname() . '/class/class.category.php';
include_once XOOPS_ROOT_PATH . '/modules/' . $xoopsModule ->dirname() . '/class/class.forum.php';


$op = '';
$confirm='';


if (isset($_GET['op'])) $op = $_GET['op'];
if (isset($_POST['op'])) $op = $_POST['op'];
if (isset($_POST['default'])) $op = 'default';
if (isset($_GET['forum_id'])) $forum_id = $_GET['forum_id'];
if (isset($_POST['forum_id'])) $forum_id = $_POST['forum_id'];
/**
* newForum()
*
* @param integer $catid
* @return
*/
function newForum(){
	editForum();
}

/**
* editForum()
*
* @param integer $catid
* @return
*/
function editForum($forum_id = 0){
	$ff = new Forums($forum_id);
	
	$groups_forum_access = null;
	$groups_forum_can_post = null;
	$groups_forum_can_view = null;
	$groups_forum_can_reply = null;
	$groups_forum_can_edit = null;
	$groups_forum_can_delete = null;
	$groups_forum_can_addpoll = null;
	$groups_forum_can_attach = null;
		
	Global $xoopsDB, $xoopsModule;
	
	
	$mytree = new XoopsTree($xoopsDB->prefix("bb_categories"), "cat_id", "0");
	
	if ($forum_id){
		$sform = new XoopsThemeForm(_MD_A_EDITTHISFORUM . " " . $ff->forum_name, "op", xoops_getenv('PHP_SELF'));
		
	}else{
		$sform = new XoopsThemeForm(_MD_A_CREATENEWFORUM, "op", xoops_getenv('PHP_SELF'));
		
		$ff->forum_order = 0;
		$ff->forum_name = '';
		$ff->forum_desc = '';
		$ff->forum_moderator = '1';
		
		$ff->forum_type = '0';
		$ff->allow_html = '1';
		$ff->allow_sig = '1';
		$ff->allow_polls = '1';
		$ff->show_name = '0';
		$ff->show_icons = '1';
		$ff->show_smilies = '1';
		$ff->hot_threshold = 10;
		$ff->posts_per_page = 10;
		$ff->topics_per_page = 20;
		
		$ff->allow_attachments = '1';
		$ff->attach_maxkb = 1000;
		$ff->attach_ext = 'zip|gif|jpg';
		
	}
	// READ PERMISSIONS
	$member_handler =& xoops_gethandler('member');
	$group_list =& $member_handler->getGroupList();
	
	$gperm_handler = & xoops_gethandler( 'groupperm' );
	$groups_forum_access = $gperm_handler -> getGroupIds('global_forum_access', $forum_id, $xoopsModule->getVar('mid'));
	
	$groups_access_checkbox = new XoopsFormCheckBox(_MD_A_ACCESSLEVEL, 'groups_forum_access[]', $groups_forum_access);
	foreach ($group_list as $group_id => $group_name) {
		$groups_access_checkbox->addOption($group_id, $group_name);
	}
	$sform -> addElement( $groups_access_checkbox );
	
	$sform->addElement(new XoopsFormText(_MD_A_SET_FORUMORDER, 'forum_order', 5, 10, $ff->forum_order), false);
	$sform->addElement(new XoopsFormText(_MD_A_FORUMNAME, 'forum_name', 50, 80, $ff->forum_name), true);
	$sform->addElement(new XoopsFormDhtmlTextArea(_MD_A_FORUMDESCRIPTION, 'forum_desc', $ff->forum_desc, 10, 60), true);
	
	$sform->addElement(new XoopsFormSelectUser(_MD_A_MODERATOR, 'forum_moderator', false, explode(" ",$ff->forum_moderator), 5, true));
	
	ob_start();
	$sform->addElement(new XoopsFormHidden('cat_id', 'cat_id'));
	$mytree->makeMySelBox("cat_title", "cat_id",$ff->cat_id);
	$sform->addElement(new XoopsFormLabel(_MD_A_CATEGORY, ob_get_contents()));
	ob_end_clean();
	
	$setvisible_select = new XoopsFormSelect(_MD_A_STATE, "forum_type", $ff->forum_type);
	$setvisible_select->addOptionArray(array('0' => _MD_A_PUBLIC, '1' => _MD_A_PRIVATE));
	$sform->addElement($setvisible_select);
	
	$allowhtml_radio = new XoopsFormRadioYN( _MD_A_ALLOWHTML, 'allow_html', $ff->allow_html,'' . _YES . '', ' ' . _NO . '' );
	$sform -> addElement( $allowhtml_radio );
	
	$allowsig_radio = new XoopsFormRadioYN( _MD_A_ALLOWSIGNATURES, 'allow_sig', $ff->allow_sig,'' . _YES . '', ' ' . _NO . '' );
	$sform -> addElement( $allowsig_radio );
	
	$allowpolls_radio = new XoopsFormRadioYN( _MD_A_ALLOWPOLLS, 'allow_polls', $ff->allow_polls,'' . _YES . '', ' ' . _NO . '' );
	$sform -> addElement( $allowpolls_radio );
	
	$allowshowname_radio = new XoopsFormRadioYN( _MD_A_SHOWNAME, 'show_name', $ff->show_name,'' . _YES . '', ' ' . _NO . '' );
	$sform -> addElement( $allowshowname_radio );
	$allowshowicons_radio = new XoopsFormRadioYN( _MD_A_SHOWICONSPANEL, 'show_icons', $ff->show_icons,'' . _YES . '', ' ' . _NO . '' );
	$sform -> addElement( $allowshowicons_radio );
	$allowshowsmilies_radio = new XoopsFormRadioYN( _MD_A_SHOWSMILIESPANEL, 'show_smilies', $ff->show_smilies,'' . _YES . '', ' ' . _NO . '' );
	$sform -> addElement( $allowshowsmilies_radio );
	
	
	
	$sform->addElement(new XoopsFormText(_MD_A_HOTTOPICTHRESHOLD, 'hot_threshold', 5, 10, $ff->hot_threshold), true);
	$sform->addElement(new XoopsFormText(_MD_A_POSTPERPAGE, 'posts_per_page', 5, 10, $ff->posts_per_page), true);
	$sform->addElement(new XoopsFormText(_MD_A_TOPICPERFORUM, 'topics_per_page', 5, 10, $ff->topics_per_page), true);
	
	$allowattach_radio = new XoopsFormRadioYN( _MD_A_ALLOW_ATTACHMENTS, 'allow_attachments', $ff->allow_attachments,'' . _YES . '', ' ' . _NO . '' );
	$sform -> addElement( $allowattach_radio );
	$sform->addElement(new XoopsFormText(_MD_A_ATTACHMENT_SIZE, 'attach_maxkb', 5, 10, $ff->attach_maxkb), true);
	$sform->addElement(new XoopsFormText(_MD_A_ALLOWED_EXTENSIONS, 'attach_ext', 50, 512, $ff->attach_ext), true);
	
	
	$sform->addElement(new XoopsFormLabel('', '<strong>'._MD_A_PERMISSIONS_TO_THIS_FORUM.'</strong>'));
	$member_handler =& xoops_gethandler('member');
	$group_list =& $member_handler->getGroupList();
	
	$gperm_handler = & xoops_gethandler( 'groupperm' );
	
	$groups_forum_can_view = $gperm_handler -> getGroupIds('forum_can_view', $forum_id, $xoopsModule->getVar('mid'));
	$groups_forum_can_view_checkbox = new XoopsFormCheckBox(_MD_A_CAN_VIEW, 'groups_forum_can_view[]', $groups_forum_can_view);
	foreach ($group_list as $group_id => $group_name) {
		$groups_forum_can_view_checkbox->addOption($group_id, $group_name);
	}
	$sform -> addElement( $groups_forum_can_view_checkbox );
	$groups_forum_can_post = $gperm_handler -> getGroupIds('forum_can_post', $forum_id, $xoopsModule->getVar('mid'));
	$groups_forum_can_post_checkbox = new XoopsFormCheckBox(_MD_A_CAN_POST, 'groups_forum_can_post[]', $groups_forum_can_post);
	foreach ($group_list as $group_id => $group_name) {
		$groups_forum_can_post_checkbox->addOption($group_id, $group_name);
	}
	$sform -> addElement( $groups_forum_can_post_checkbox );
	$groups_forum_can_reply = $gperm_handler -> getGroupIds('forum_can_reply', $forum_id, $xoopsModule->getVar('mid'));
	$groups_forum_can_reply_checkbox = new XoopsFormCheckBox(_MD_A_CAN_REPLY, 'groups_forum_can_reply[]', $groups_forum_can_reply);
	foreach ($group_list as $group_id => $group_name) {
		$groups_forum_can_reply_checkbox->addOption($group_id, $group_name);
	}
	$sform -> addElement( $groups_forum_can_reply_checkbox );
	$groups_forum_can_edit = $gperm_handler -> getGroupIds('forum_can_edit', $forum_id, $xoopsModule->getVar('mid'));
	$groups_forum_can_edit_checkbox = new XoopsFormCheckBox(_MD_A_CAN_EDIT, 'groups_forum_can_edit[]', $groups_forum_can_edit);
	foreach ($group_list as $group_id => $group_name) {
		$groups_forum_can_edit_checkbox->addOption($group_id, $group_name);
	}
	$sform -> addElement( $groups_forum_can_edit_checkbox );
	$groups_forum_can_delete = $gperm_handler -> getGroupIds('forum_can_delete', $forum_id, $xoopsModule->getVar('mid'));
	$groups_forum_can_delete_checkbox = new XoopsFormCheckBox(_MD_A_CAN_DELETE, 'groups_forum_can_delete[]', $groups_forum_can_delete);
	foreach ($group_list as $group_id => $group_name) {
		$groups_forum_can_delete_checkbox->addOption($group_id, $group_name);
	}
	$sform -> addElement( $groups_forum_can_delete_checkbox );
	$groups_forum_can_addpoll = $gperm_handler -> getGroupIds('forum_can_addpoll', $forum_id, $xoopsModule->getVar('mid'));
	$groups_forum_can_addpoll_checkbox = new XoopsFormCheckBox(_MD_A_CAN_ADDPOLL, 'groups_forum_can_addpoll[]', $groups_forum_can_addpoll);
	foreach ($group_list as $group_id => $group_name) {
		$groups_forum_can_addpoll_checkbox->addOption($group_id, $group_name);
	}
	$sform -> addElement( $groups_forum_can_addpoll_checkbox );
	$groups_forum_can_vote = $gperm_handler -> getGroupIds('forum_can_vote', $forum_id, $xoopsModule->getVar('mid'));
	$groups_forum_can_vote_checkbox = new XoopsFormCheckBox(_MD_A_CAN_VOTE, 'groups_forum_can_vote[]', $groups_forum_can_vote);
	foreach ($group_list as $group_id => $group_name) {
		$groups_forum_can_vote_checkbox->addOption($group_id, $group_name);
	}
	$sform -> addElement( $groups_forum_can_vote_checkbox );
	$groups_forum_can_attach = $gperm_handler -> getGroupIds('forum_can_attach', $forum_id, $xoopsModule->getVar('mid'));
	$groups_forum_can_attach_checkbox = new XoopsFormCheckBox(_MD_A_CAN_ATTACH, 'groups_forum_can_attach[]', $groups_forum_can_attach);
	foreach ($group_list as $group_id => $group_name) {
		$groups_forum_can_attach_checkbox->addOption($group_id, $group_name);
	}
	$sform -> addElement( $groups_forum_can_attach_checkbox );
	
	
	
	$sform->addElement(new XoopsFormHidden('forum_id', $forum_id));
	$button_tray = new XoopsFormElementTray('', '');
	$button_tray->addElement(new XoopsFormHidden('op', 'save'));
	
	
	// No ID for category -- then it's new category, button says 'Create'
	if ( !$forum_id )
	{
		$butt_create = new XoopsFormButton( '', '', _MD_A_CREATEFORUM, 'submit' );
		$butt_create->setExtra('onclick="this.form.elements.op.value=\'save\'"');
		$button_tray->addElement( $butt_create );
		
		$butt_clear = new XoopsFormButton( '', '', _MD_A_CLEAR, 'reset' );
		$button_tray->addElement( $butt_clear );
		
		$butt_cancel = new XoopsFormButton( '', '', _CANCEL, 'button' );
		$butt_cancel->setExtra('onclick="history.go(-1)"');
		$button_tray->addElement( $butt_cancel );
	}
	else // button says 'Update'
	{
		$butt_create = new XoopsFormButton( '', '', _MD_A_EDIT, 'submit' );
		$butt_create->setExtra('onclick="this.form.elements.op.value=\'save\'"');
		$button_tray->addElement( $butt_create );
		
		$butt_cancel = new XoopsFormButton( '', '', _CANCEL, 'button' );
		$butt_cancel->setExtra('onclick="history.go(-1)"');
		$button_tray->addElement( $butt_cancel );
	}
	
	$sform->addElement($button_tray);
	$sform->display();
}

switch ($op) {
	
	case 'moveforum':
	
	
	if(isset($_POST['save']) && $_POST['save'] != "" )
	{
		if (isset($_GET['forum'])) $forum = intval($_GET['forum']);
		if (isset($_POST['forum'])) $forum = intval($_POST['forum']);
		
		$bMoved = 0;
		$errString = '';
		// Look for subforums
		$sql = "SELECT * from ".$xoopsDB->prefix('bb_forums')." WHERE parent_forum=$forum";
		if ($result = $xoopsDB->query($sql))
		{
			if ($xoopsDB->getRowsNum($result) == 0)
			{
				$sql_move = "UPDATE ".$xoopsDB->prefix('bb_forums')." SET cat_id=".$_POST['cat_id'].", parent_forum=".$_POST['parent_forum']." WHERE forum_id=$forum";
				if($xoopsDB->query($sql_move))
				$bMoved = 1;
			}
			else
			{
				// Are we trying to move this
				if ($_POST['parent_forum'] != 0)
				{
					$errString = "This forum cannot be made as a subforum.<br>Multi-level subforums are not allowed.";
				}
				else
				{
					$sql_move = "UPDATE ".$xoopsDB->prefix('bb_forums')." SET cat_id=".$_POST['cat_id'].", parent_forum=".$_POST['parent_forum']." WHERE forum_id=$forum";
					if($xoopsDB->query($sql_move))
					{
						$bMoved = 1;
						while($row = $xoopsDB->fetchArray($result))
						{
							$sql_move_sub = "UPDATE ".$xoopsDB->prefix('bb_forums')." SET cat_id=".$_POST['cat_id']." WHERE forum_id=".$row['forum_id'];
							$xoopsDB->query($sql_move_sub);
						}
					}
				}
			}
		}
		
		$sql = "UPDATE ".$xoopsDB->prefix('bb_forums')." SET cat_id=".$_POST['cat_id'].", parent_forum=".$_POST['parent_forum']." WHERE forum_id=$forum";
		if (!$bMoved)
		{
			redirect_header('./admin_forum_manager.php?mode=manage',2,_MD_A_MSG_ERR_FORUM_MOVED.'<br>'.$errString);
		}
		else
		{
			redirect_header('./admin_forum_manager.php?mode=manage',2,_MD_A_MSG_FORUM_MOVED);
		}
		die();
	}
	else
	{
		xoops_cp_header();
		adminmenu(1,"");
		
		if (isset($_GET['forum'])) $forum = intval($_GET['forum']);
		if (isset($_POST['forum'])) $forum = intval($_POST['forum']);
		
		$sql   = "SELECT * FROM ".$xoopsDB->prefix('bb_forums')." WHERE forum_id=$forum";
		if (!$result = $xoopsDB->query($sql))
		{
			
			xoops_cp_footer();
			exit();
		}
		if (!$myrow = $xoopsDB->fetchArray($result))
		{
			echo _MD_A_NOSUCHFORUM;
		}
		$name = $myts->htmlSpecialChars($myrow['forum_name']);
		$desc = $myts->htmlSpecialChars($myrow['forum_desc']);
		
		$sql = "select * from ".$xoopsDB->prefix('bb_categories');
		$cat_list="<select name=\"cat_id\" onchange='document.forummove.submit();'>";
		$cat_list.='<option value="0">'._MD_A_SELECT.'</option>';
		if (!$result = $xoopsDB->query($sql))
		{
			echo "no";
			xoops_cp_footer();
			exit();
		}
		while ($row = $xoopsDB->fetchArray($result))
		{
			$selected = '';
			if (isset($_POST['cat_id']) && $_POST['cat_id'] == $row['cat_id']) $selected = 'selected';
			$cat_list .= "<option value='".$row['cat_id']."' $selected>".$row['cat_title']."</option>";
		}
		$cat_list.='</select>';
		
		$sf_list='<select name="parent_forum">';
		$sf_list.='<option value="0" selected>'._MD_A_SELECT.'</option>';
		$sf_list.='<option value="0">'._NONE.'</option>';
		if(isset($_POST['cat_id']))
		{
			$sql = "SELECT * FROM ".$xoopsDB->prefix('bb_forums')." WHERE cat_id=".$_POST['cat_id']." AND forum_id!=$forum";
			if ($result = $xoopsDB->query($sql))
			while ($row = $xoopsDB->fetchArray($result))
			{
				$sf_list .= "<option value='".$row['forum_id']."'>".$row['forum_name']."</option>";
			}
		}
		$sf_list.='</select>';
		
		echo '<form action="./admin_forum_manager.php" method="post" name="forummove" id="forummove">';
		echo '<input type="hidden" name="mode" value="moveforum" />';
		echo '<input type="hidden" name="forum" value='.$forum.' />';
		echo '<table border="0" cellpadding="1" cellspacing="0" align="center" valign="top" width="95%"><tr>';
		echo '<td class="bg2">';
		echo '<table border="0" cellpadding="1" cellspacing="1" width="100%"><tr class="bg3">';
		echo '<td align="center" colspan="2"><b>'. _MD_A_MOVETHISFORUM.'</b></td>';
		echo '</tr>';
		echo '<tr><td class="bg1">'. _MD_A_MOVE2CAT.'</td><td class="bg1">'. $cat_list.'</td></tr>';
		echo '<tr><td class="bg1">'. _MD_A_MAKE_SUBFORUM_OF.'</td><td class="bg1">'. $sf_list.'</td></tr>';
		echo '<tr><td colspan="2" align="center"><input type="submit" name="save" value='. _SAVE.' class="button"></td></tr>';
		echo '</form></table></td></tr></table>';
	}
	xoops_cp_footer();
	break;
	
	
	
	case 'sync':
	if (isset($_POST['submit'])) {
		flush();
		sync(null, "all forums");
		flush();
		sync(null, "all topics");
		redirect_header("./index.php", 1, _MD_A_SYNCHING);
	} else {
		
		xoops_cp_header();
		adminmenu(3,_MD_A_SYNCFORUM);
		echo '<fieldset><legend style="font-weight: bold; color: #900;">' .  _MD_A_SYNCFORUM . '</legend>';
		echo '<br><br><table width="100%" border="0" cellspacing="1" class="outer"><tr><td class=\"odd\">';
		echo '<table border="0" cellpadding="1" cellspacing="1" width="100%">';
		echo '<tr class="bg3" align="left">';
		echo '<td>'. _MD_A_CLICKBELOWSYNC.'</td>';
		echo '</tr>';
		echo '<tr class="bg1" align="center">';
		echo '<td><form action='. $_SERVER['PHP_SELF'].' method="post">';
		echo '<input type="hidden" name="op" value='. $op.'><input type="submit" name="submit" value='. _MD_A_SYNCFORUM.'></form></td>';
		echo '</td>';
		echo '</tr>';
		echo '</table></td></tr></table>';
		
	}
	
	echo "</fieldset>";
	xoops_cp_footer();
	break;
	
	case "save":
	
	if ($forum_id){
		$ff = new Forums($forum_id);
		$message=_MD_A_FORUMUPDATE;
		
	}else{
		$ff = new Forums();
		$message=_MD_A_FORUMCREATED;
		/*
		$tags = array();
		$tags['FORUM_URL'] = XOOPS_URL . '/modules/' . $xoopsModule->getVar('dirname') . '/viewforum.php?forum=' . $nextid;
		$tags['FORUM_NAME'] = $name;
		$tags['FORUM_DESCRIPTION'] = $desc;
		$notification_handler =& xoops_gethandler('notification');
		$notification_handler->triggerEvents('global', 0, 'new_forum', $tags); */
	}
	
	$ff->setforum_name($_POST['forum_name']);
	$ff->setforum_desc($_POST['forum_desc']);
	$ff->setforum_order($_POST['forum_order']);
	$ff->set_groups_forum_access($_POST['groups_forum_access']);
	$ff->set_groups_forum_can_post($_POST['groups_forum_can_post']);
	$ff->set_groups_forum_can_view($_POST['groups_forum_can_view']);
	$ff->set_groups_forum_can_reply($_POST['groups_forum_can_reply']);
	$ff->set_groups_forum_can_edit($_POST['groups_forum_can_edit']);
	$ff->set_groups_forum_can_delete($_POST['groups_forum_can_delete']);
	$ff->set_groups_forum_can_addpoll($_POST['groups_forum_can_addpoll']);
	$ff->set_groups_forum_can_vote($_POST['groups_forum_can_vote']);
	$ff->set_groups_forum_can_attach($_POST['groups_forum_can_attach']);
	$ff->setforum_moderator(saveNewbbAccess($_POST['forum_moderator']));
	$ff->setcat_id($_POST['cat_id']);
	$ff->setforum_type($_POST['forum_type']);
	$ff->setallow_html($_POST['allow_html']);
	$ff->setallow_sig($_POST['allow_sig']);
	$ff->setallow_polls($_POST['allow_polls']);
	$ff->setallow_attachments($_POST['allow_attachments']);
	$ff->setshow_name($_POST['show_name']);
	$ff->setshow_icons($_POST['show_icons']);
	$ff->setshow_smilies($_POST['show_smilies']);
	$ff->setattach_maxkb($_POST['attach_maxkb']);
	$ff->setattach_ext($_POST['attach_ext']);
	$ff->sethot_threshold($_POST['hot_threshold']);
	$ff->settopics_per_page($_POST['topics_per_page']);
	$ff->setposts_per_page($_POST['posts_per_page']);
	$ff->store();
	
	redirect_header("admin_forum_manager.php?op=mod&forum_id=$forum_id", 1, $message);
	exit();
	
	case "mod":
	
	$ff = new Forums($forum_id);
	xoops_cp_header();
	adminmenu(1,_MD_A_EDITTHISFORUM.$ff->forum_name );
	echo "<fieldset><legend style='font-weight: bold; color: #900;'>" .  _MD_A_EDITTHISFORUM  . "</legend>";
	echo"<br><br><table width='100%' border='0' cellspacing='1' class='outer'><tr><td class=\"odd\">";
	
	
	editForum($forum_id);
	
	echo"</td></tr></table>";
	echo "</fieldset>";
	xoops_cp_footer();
	break;
	
	case "del":
	
	if (isset($_POST['confirm']) != 1){
		xoops_cp_header();
		xoops_confirm( array( 'op' => 'del', 'forum_id' => intval( $_GET['forum_id'] ), 'confirm' => 1 ), 'admin_forum_manager.php', _MD_A_TWDAFAP );
		xoops_cp_footer();
	}else{
		$ff = new Forums($_POST['forum_id']);
		$sql = "SELECT post_id FROM ".$xoopsDB->prefix("bb_posts")." WHERE forum_id = $_POST[forum_id]";
		if ( !$r = $xoopsDB->query($sql) ) {
			redirect_header("./index.php", 1);
			exit();
		}
		if ( $xoopsDB->getRowsNum($r) > 0 ) {
			$sql = "DELETE FROM ".$xoopsDB->prefix("bb_posts_text")." WHERE ";
			$looped = false;
			while ( $ids = $xoopsDB->fetchArray($r) ) {
				if ( $looped == true ) {
					$sql .= " OR ";
				}
				$sql .= "post_id = ".$ids["post_id"]." ";
				$looped = true;
			}
			if ( !$r = $xoopsDB->query($sql) ) {
				redirect_header("./index.php", 1);
				exit();
			}
			$sql = sprintf("DELETE FROM %s WHERE forum_id = %u", $xoopsDB->prefix("bb_posts"), $_POST['forum_id']);
			if ( !$r = $xoopsDB->query($sql) ) {
				redirect_header("./index.php", 1);
				exit();
			}
		}
		// RMV-NOTIFY
		xoops_notification_deletebyitem ($xoopsModule->getVar('mid'), 'forum',$_POST['forum_id']);
		// Get list of all topics in forum, to delete them too
		$sql = sprintf("SELECT topic_id FROM %s WHERE forum_id = %u", $xoopsDB->prefix("bb_topics"), $_POST['forum_id']);
		if ($r = $xoopsDB->query($sql)) {
			while ($row = $xoopsDB->fetchArray($r)) {
				xoops_notification_deletebyitem ($xoopsModule->getVar('mid'), 'thread', $row['topic_id']);
			}
		}
		$sql = sprintf("DELETE FROM %s WHERE forum_id = %u", $xoopsDB->prefix("bb_topics"), $_POST['forum_id']);
		if ( !$r = $xoopsDB->query($sql) ) {
			redirect_header("./index.php", 1);
			exit();
		}
		
		$ff->delete();
		
		$sql = sprintf("DELETE FROM %s WHERE forum_id = %u", $xoopsDB->prefix("bb_forum_group_access"), $_POST['forum_id']);
		if ( !$r = $xoopsDB->query($sql) ) {
			redirect_header("./index.php", 1);
			exit();
		}
		$sql = sprintf("DELETE FROM %s WHERE forum_id = %u", $xoopsDB->prefix("bb_forum_access"), $_POST['forum_id']);
		if ( !$r = $xoopsDB->query($sql) ) {
			redirect_header("./index.php", 1);
			exit();
		}
		redirect_header("admin_forum_manager.php", 1, _MD_A_FORUMREMOVED);
		exit();
		
	}
	exit();
	
	case 'manage':
	xoops_cp_header();
	adminmenu(1,_MD_A_FORUM_MANAGER);
	echo "<fieldset><legend style='font-weight: bold; color: #900;'>" .  _MD_A_FORUM_MANAGER . "</legend>";
	echo"<br />";
	
	echo "<table border='0' cellpadding='4' cellspacing='1' width='100%' class='outer'>";
	echo "<tr align='center'>";
	echo "<td class='bg3'>"._MD_A_NAME."</td>";
	echo "<td class='bg3'>"._MD_A_EDIT."</td>";
	echo "<td class='bg3'>"._MD_A_DELETE."</td>";
	echo "<td class='bg3'>"._MD_A_ADD."</td>";
	echo "<td class='bg3'>"._MD_A_MOVE."</td>";
	echo "</tr>";
	
	
	
	$sql = "SELECT * FROM ".$xoopsDB->prefix("bb_categories")." ORDER BY cat_order";
	$tbl_cat = $xoopsDB->query($sql);
	for($iCat=0; $iCat<$xoopsDB->getRowsNum($tbl_cat); $iCat++)
	{
		
		// Display the Category
		$cat_row = $xoopsDB->fetchArray($tbl_cat);
		$cat_link = "<a href=\"".$forumUrl['root']."index.php?viewcat=".$cat_row['cat_id']."\">".$cat_row['cat_title']."</a>";
		$cat_edit_link = "<a href=\"admin_cat_manager.php?op=mod&cat_id=".$cat_row['cat_id']."\"><img src=\"../images/edit.gif\"></a>";
		$cat_del_link = "<a href=\"admin_cat_manager.php?op=del&cat_id=".$cat_row['cat_id']."\"><img src=\"../images/delete.gif\"></a>";
		$forum_add_link = "<a href=\"admin_forum_manager.php?op=addforum&cat_id=".$cat_row['cat_id']."\"><img src=\"".$forumImage['new_forum']."\"></a>";
		
		echo "<tr class='even' align='left'>";
		echo "<td width='100%'><b>".$cat_link."</b></td>";
		echo "<td align='center'>".$cat_edit_link."</td>";
		echo "<td align='center'>".$cat_del_link."</td>";
		echo "<td align='center'>".$forum_add_link."</td>";
		echo "<td></td>";
		echo "</tr>";
		
		// Display the forums belonging to this category
		$sql = "SELECT * from ".$xoopsDB->prefix("bb_forums")." WHERE cat_id=".$cat_row['cat_id']." ORDER BY forum_order";
		$tbl_forums = $xoopsDB->query($sql);
		for($iForum=0; $iForum<$xoopsDB->getRowsNum($tbl_forums); $iForum++)
		{
			$f_row = $xoopsDB->fetchArray($tbl_forums);
			
			if ($f_row['parent_forum'] > 0)
			{
				$f_link = "&nbsp;<a href=\"".$forumUrl['root']."viewforum.php?forum=".$f_row['forum_id']."\">-->".$f_row['forum_name']."</a>";
				$f_edit_link = "<a href=\"admin_forum_manager.php?op=mod&forum_id=".$f_row['forum_id']."\"><img src=\"../images/edit.gif\"></a>";
				$f_del_link = "<a href=\"admin_forum_manager.php?op=del&forum_id=".$f_row['forum_id']."\"><img src=\"../images/delete.gif\"></a>";
				$sf_add_link = "";
				$f_move_link =  "<a href=\"admin_forum_manager.php?op=moveforum&forum_id=".$f_row['forum_id']."\"><img src=\"".$forumImage['movetopic']."\"></a>";
			}
			else
			{
				$f_link = "&nbsp;<a href=\"".$forumUrl['root']."viewforum.php?forum=".$f_row['forum_id']."\">".$f_row['forum_name']."</a>";
				$f_edit_link = "<a href=\"admin_forum_manager.php?op=mod&forum_id=".$f_row['forum_id']."\"><img src=\"../images/edit.gif\"></a>";
				$f_del_link = "<a href=\"admin_forum_manager.php?op=del&forum_id=".$f_row['forum_id']."\"><img src=\"../images/delete.gif\"></a>";
				$sf_add_link = "<a href=\"admin_forum_manager.php?op=addsubforum&cat_id=".$f_row['cat_id']."&parent_forum=".$f_row['forum_id']."\"><img src=\"".$forumImage['new_subforum']."\"></a>";
				$f_move_link =  "<a href=\"admin_forum_manager.php?op=moveforum&forum_id=".$f_row['forum_id']."\"><img src=\"".$forumImage['movetopic']."\"></a>";
			}
			
			echo "<tr class='odd' align='left'>";
			echo "<td><b>".$f_link."</b></td>";
			echo "<td align='center'>".$f_edit_link."</td>";
			echo "<td align='center'>".$f_del_link."</td>";
			echo "<td align='center'>".$sf_add_link."</td>";
			echo "<td align='center'>".$f_move_link."</td>";
			echo "</tr>";
			
		}
		
	}
	
	echo "</table>";
	echo "</fieldset>";
	xoops_cp_footer();
	break;
	case "default":
	default:
	xoops_cp_header();
	adminmenu(1,_MD_A_CREATENEWFORUM);
	echo "<fieldset><legend style='font-weight: bold; color: #900;'>" . _MD_A_CREATENEWFORUM  . "</legend>";
	echo "<br />";
	
	newForum();
	
	echo "</fieldset>";
	xoops_cp_footer();
	
}
?>