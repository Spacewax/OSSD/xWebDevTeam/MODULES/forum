<?php
// $Id: admin_forum_reorder.php,v 1.6 2004/05/29 17:11:48 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //

include 'admin_header.php';
include_once XOOPS_ROOT_PATH . '/modules/' . $xoopsModule ->dirname() . '/class/class.category.php';
include_once XOOPS_ROOT_PATH . '/modules/' . $xoopsModule ->dirname() . '/class/class.forum.php';

if ( isset($_POST['cat_orders'])) $cat_orders = $_POST['cat_orders'];
if ( isset($_POST['orders'])) $orders = $_POST['orders'];
if ( isset($_POST['cat'])) $cat = $_POST['cat'];
if ( isset($_POST['forum'])) $forum = $_POST['forum'];

  if ( isset($_POST['submit']) && $_POST['submit'] != "" ) {

        for ( $i = 0; $i < count( $cat_orders ); $i++ )
        {
            $sql="update " . $xoopsDB -> prefix( "bb_categories" ) . " set cat_order = " . $cat_orders[$i] . " WHERE cat_id=$cat[$i]";
            if ( !$result = $xoopsDB->query($sql) )
            {
             return _MD_A_ERROR;
            }
        }

        for ( $i = 0; $i < count( $orders ); $i++ )
        {
            $sql="update " . $xoopsDB -> prefix( "bb_forums" ) . " set forum_order = " . $orders[$i] . " WHERE forum_id=$forum[$i]" ;
            if ( !$result = $xoopsDB->query($sql) )
            {
             return _MD_A_ERROR;
            }

        }
        redirect_header( "admin_forum_reorder.php", 1, _MD_A_BOARDREORDER );

    }
    else
    {
        $orders = array();
        $cat_orders = array();
        $forum = array();
        $cat = array();

        xoops_cp_header();
        adminmenu(4,_MD_A_SETFORUMORDER);
        echo "<fieldset><legend style='font-weight: bold; color: #900;'>" .  _MD_A_SETFORUMORDER. "</legend>";
        echo"<br><br><table width='100%' border='0' cellspacing='1' class='outer'>"
        ."<tr><td class=\"odd\">";
        $tform = new XoopsThemeForm( _MD_A_SETFORUMORDER,"","");
        $tform->display();
        echo "<form name='reorder' METHOD='post'>";
        echo "<table border='0' width='100%' cellpadding = '2' cellspacing ='1' class = 'outer'>";
        echo "<tr>";
        echo "<td class = 'head' align='center' width=3% height =16 ><b>" . _MD_A_REORDERID . "</b>";
        echo "</td><td class = 'head' align='left' width=30%><b>" . _MD_A_REORDERTITLE . "</b>";
        echo "</td><td class = 'head' align='center' width=5%><b>" . _MD_A_REORDERWEIGHT . "</b>";
        echo "</td></tr>";
        $fc = new ForumCat();
        $categorys = $fc -> getFirstChild();

        foreach( $categorys as $onecat )
        {
            echo "<tr>";
            echo "<td align='left' class = 'head'>" . $onecat->cat_id . "</td>";
            echo "<input type='hidden' name='cat[]' value='" . $onecat -> cat_id . "' />";
            echo "<td align='left' nowrap='nowrap' class = 'head' >" . $onecat -> cat_title . "</td>";
            echo "<td align='right' class = 'head'>";
            echo "<input type='text' name='cat_orders[]' value='" . $onecat->cat_order . "' size='5' maxlenght='5'>";
            echo "</td>";
            echo "</tr>";

            $sql = "SELECT * FROM " . $xoopsDB->prefix("bb_forums") . " WHERE cat_id=" .$onecat->cat_id." order by cat_id, forum_order";
            $result = $xoopsDB->query($sql);
            if ( $myrow = $xoopsDB->fetchArray($result) ) {
            do {

            if ($myrow['parent_forum'] >0)
            {
            echo "<tr>";
            echo "<td align='right' class = even></td>";
            echo "<input type='hidden' name='forum[]' value='" . $myrow['forum_id']. "' />";
            echo "<td align='left' nowrap='nowrap' class = odd>";
            echo "<table width='100%'><tr>";
            echo "<td width='3%' align='right' nowrap='nowrap' class = even>" . $myrow['forum_id']. "</td>";
            echo "<td width='80%' align='left' nowrap='nowrap' class = odd>-->&nbsp;" . $myrow['forum_name']. "</td>";
            echo "<td width= '5%' align='right' nowrap='nowrap' class = odd>";
            echo "<input type='text' name='orders[]' value='" . $myrow['forum_order']. "' size='5' maxlenght='5'></td>";
            echo "</td></tr></table>";
            echo "<td align='left' class = even>";
            echo "</td>";
            echo "</tr>";
            }
            else
            {
            echo "<tr>";
            echo "<td align='right' class = even>" . $myrow['forum_id']. "</td>";
            echo "<input type='hidden' name='forum[]' value='" . $myrow['forum_id']. "' />";
            echo "<td align='left' nowrap='nowrap' class = odd>" . $myrow['forum_name']. "</td>";
            echo "<td align='left' class = even>";
            echo "<input type='text' name='orders[]' value='" . $myrow['forum_order']. "' size='5' maxlenght='5'>";
            echo "</td>";
            echo "</tr>";
            }
          } while ( $myrow = $xoopsDB->fetchArray($result) ); }
        }
        echo "<tr><td class='even' align='center' colspan='6'>";

        echo "<input type='submit' name='submit' value='" . _SUBMIT . "' />";

        echo "</td></tr>";
        echo "</table>";
        echo "</form>";
     }

echo"</td></tr></table>";
echo "</fieldset>";
xoops_cp_footer();
?>