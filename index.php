<?php
// $Id: index.php,v 1.6 2004/05/29 17:11:48 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //

include "header.php";
include_once(XOOPS_ROOT_PATH . "/modules/" . $xoopsModule->dirname() . "/class/class.category.php");
include_once(XOOPS_ROOT_PATH . "/modules/" . $xoopsModule->dirname() . "/class/class.permission.php");

$xoopsOption['template_main']= 'newbb_index.html';
include XOOPS_ROOT_PATH."/header.php";

if ($xoopsModuleConfig['wol_enabled'])
{
	include_once XOOPS_ROOT_PATH.'/modules/' . $xoopsModule->dirname() . '/class/class.whoisonline.php';
	$whosonline = new WhosOnline(0);
	$xoopsTpl->assign('whois', $whosonline->show_online_list());
}


if (isset($_GET['mark_read']) && $_GET['mark_read'] == 2)
{
	$result = $xoopsDB->query("SELECT forum_id FROM ".$xoopsDB->prefix("bb_forums"));
	$forum_lastview = !empty($HTTP_COOKIE_VARS['newbb_forum_lastview']) ? unserialize($HTTP_COOKIE_VARS['newbb_forum_lastview']) : array();
	while ( list($forum_id)=$xoopsDB->fetchRow($result) )
	{
		$forum_lastview[$forum_id] = time();
	}
	
	setcookie("newbb_forum_lastview", serialize($forum_lastview), time()+365*24*3600, $forumCookie['path'], $forumCookie['domain'], $forumCookie['secure']);
	$url=XOOPS_URL . "/modules/newbb/index.php";
	redirect_header($url,2, _MD_ALL_FORUM_MARKED);
}
$xoopsTpl->assign(array("lang_welcomemsg" => sprintf(_MD_WELCOME,$xoopsConfig['sitename']), "total_topics" => get_total_topics(), "total_posts" => get_total_posts(0, 'all'), "total_user" => get_total_users(),"lang_lastvisit" => sprintf(_MD_LASTVISIT,formatTimestamp($last_visit)), "lang_currenttime" => sprintf(_MD_TIMENOW,formatTimestamp(time(),"m"))));

$viewcat = (!empty($_GET['cat'])) ? intval($_GET['cat']) : 0;

$fc = new ForumCat();
$category = $fc->getFirstChild();
$getpermission = new Permissions();
$catpermission = $getpermission->getPermissions("global");
$forumpermission = $getpermission->getPermissions("forum");
foreach($category as $onecat){
	
	$sql = 'SELECT f.*, u.uname, u.uid, p.topic_id, p.post_time, p.subject, p.icon FROM '.$xoopsDB->prefix('bb_forums').' f LEFT JOIN '.$xoopsDB->prefix('bb_posts').' p ON p.post_id = f.forum_last_post_id LEFT JOIN '.$xoopsDB->prefix('users').' u ON u.uid = p.uid';
	if ( $viewcat != 0 ) {
		$sql .= ' WHERE f.cat_id = '.$viewcat.'';
		$sql .= ' AND f.parent_forum = 0';
		$xoopsTpl->assign('forum_index_title', sprintf(_MD_FORUMINDEX,$xoopsConfig['sitename']));
	} else {
		$xoopsTpl->assign('forum_index_title', '');
		$sql .= ' WHERE f.parent_forum =0';
		$sql .= ' ORDER BY f.cat_id, f.forum_order';
	}
	if ( !$result = $xoopsDB->query($sql) ) {
		exit("Error");
	}
	// Read 'lastread' cookie, if exists
	$topic_lastread = !empty($HTTP_COOKIE_VARS['newbb_topic_lastread']) ? unserialize($HTTP_COOKIE_VARS['newbb_topic_lastread']) : array();
	// Read 'lastview' cookie, if exists
	$forum_lastview = !empty($HTTP_COOKIE_VARS['newbb_forum_lastview']) ? unserialize($HTTP_COOKIE_VARS['newbb_forum_lastview']): array();
	
	$forums = array(); // RMV-FIX
	while ( $forum_data = $xoopsDB->fetchArray($result) ) {
		
		if ( $forum_data['cat_id'] == $onecat->cat_id ) {
			if ($forum_data['post_time']) {
				$forum_lastpost_time = formatTimestamp($forum_data['post_time']);
				$last_post_icon = '<a href="'.XOOPS_URL.'/modules/' . $xoopsModule->dirname() . '/viewtopic.php?post_id='.$forum_data['forum_last_post_id'].'&amp;topic_id='.$forum_data['topic_id'].'&amp;forum='.$forum_data['forum_id'].'#forumpost'.$forum_data['forum_last_post_id'].'">';
				if ( $forum_data['icon'] ) {
					$last_post_icon .= '<img src="'.XOOPS_URL.'/images/subject/'.$forum_data['icon'].'" border="0" alt="" />';
				} else {
					$last_post_icon .= '<img src="'.XOOPS_URL.'/images/subject/icon1.gif" width="15" height="15" border="0" alt="" />';
				}
				$last_post_icon .= '</a>';
				$forum_lastpost_icon = $last_post_icon;
				if ( $forum_data['uid'] != 0 && $forum_data['uname'] ){
					$forum_lastpost_user = ''._MD_BY.'&nbsp;<a href="'.XOOPS_URL.'/userinfo.php?uid='.$forum_data['uid'].'">' . $myts->htmlSpecialChars($forum_data['uname']).'</a>';
				} else {
					$forum_lastpost_user = $xoopsConfig['anonymous'];
				}
				$forum_lastread = !empty($topic_lastread[$forum_data['topic_id']]) ? $topic_lastread[$forum_data['topic_id']] : false;
				if (isset($forum_lastview[$forum_data['forum_id']]) and $forum_lastview[$forum_data['forum_id']]>$forum_data['post_time'])
				{
					if ( $forum_data['forum_type'] == 1 )
					{
						$forum_folder = $forumImage['locked_forum'];
					}else{$forum_folder = $forumImage['folder_forum'];}
				}
				elseif ( $forum_data['post_time'] > $forum_lastread && !empty($forum_data['topic_id'])) {
					if ( $forum_data['forum_type'] == 1 )
					{
						$forum_folder = $forumImage['locked_forum_newposts'];
					}else{$forum_folder = $forumImage['newposts_forum'];}
				} else {
					if ( $forum_data['forum_type'] == 1 )
					{
						$forum_folder = $forumImage['locked_forum'];
					}else{$forum_folder = $forumImage['folder_forum'];}
				}
			} else {
				// no forums, so put empty values
				$forum_lastpost_time = "";
				$forum_lastpost_icon = "";
				$forum_lastpost_user = _MD_NOPOSTS;
				if ( $forum_data['forum_type'] == 1 ) {
					$forum_folder = $forumImage['locked_forum'];
				} else {
					$forum_folder = $forumImage['folder_forum'];
				}
			}
			
			
			if (isset($forumpermission[$forum_data['forum_id']]['global_forum_access'])) {
				$forumpermission=  1;
			}
			else
			{
				$forumpermission = 0;
			}
			
			if (empty($forum_data['forum_moderator'])){$forum_data['forum_moderator'] = 0;}
			if ($forum_data['forum_type'] == 0 ){
				$forumpermissions = 1;
			}
			else
			{
				$forumpermissions = 0;
				if ($forumpermission) {$forumpermissions = 1;}
			}
			if ( is_object($xoopsUser) ) {
				$xoopsModule = XoopsModule::getByDirname("newbb");
				$getmod = new Permissions();
				
				if ( $xoopsUser->isAdmin($xoopsModule->mid()) || $getmod->is_moderator($xoopsUser->uid(),$forum_data['forum_moderator']) ) {
					$forumpermissions = 1;
				}
			}
			
			
			$total_posts  = $forum_data["forum_posts"];
			$total_topics = $forum_data["forum_topics"];
			$sf_links     = '';
			$sf_links2    = '';
			if($forum_data['subforum_count'] > 0)
			{
				$sf_sql = 'SELECT f.*, u.uname, u.uid, p.topic_id, p.post_time, p.subject, p.icon
                 FROM '.$xoopsDB->prefix('bb_forums').' f
                 LEFT JOIN '.$xoopsDB->prefix('bb_posts').' p
                 ON p.post_id = f.forum_last_post_id
                 LEFT JOIN '.$xoopsDB->prefix('users').' u
                 ON u.uid = p.uid WHERE parent_forum='.$forum_data['forum_id'].'';
				if ($sf_result = $xoopsDB->query($sf_sql))
				{
					$SubForumAdded = false;
					
					while ($r = $xoopsDB->fetchArray($sf_result))
					{
						
						if (isset($forumpermission[$r['forum_id']]['global_forum_access'])) {
							$subforumpermission=  1;
						}
						else
						{
							$subforumpermission = 0;
						}
						
						if (empty($r['forum_moderator'])){$r['forum_moderator'] = 0;}
						if ($r['forum_type'] == 0 ){
							$subforumpermissions = 1;
						}
						else
						{
							$subforumpermissions = 0;
							if ($subforumpermission) {$subforumpermissions = 1;}
						}
						if ( is_object($xoopsUser) ) {
							$xoopsModule = XoopsModule::getByDirname("newbb");
							$getmod = new Permissions();
							
							if ( $xoopsUser->isAdmin($xoopsModule->mid()) || $getmod->is_moderator($xoopsUser->uid(),$forum_data['forum_moderator']) ) {
								$subforumpermissions = 1;
							}
						}
						if ($subforumpermissions == 1){
							$sf_links .= "<a href='./viewforum.php?forum=".$r['forum_id']."' title='".$r['forum_desc']."'>".$r['forum_name']."</a>&nbsp;&nbsp;";
							$SubForumAdded = false;
						}
						else
						{
							$SubForumAdded = true;
						}
						if (!$SubForumAdded)
						{
							$sf_links2 = "<br><br><img src='".XOOPS_URL."/modules/" . $xoopsModule->dirname() . "/images/arrow.gif' border='0' alt='' />&nbsp;"._MD_SUBFORUMS."&nbsp;&nbsp;";
							$sf_links2=$sf_links2.$sf_links;
							$SubForumAdded = true;
						}
						$last_post_datetime = $forum_data['post_time'];
						$total_posts  += $r['forum_posts'];
						$total_topics += $r['forum_topics'];
						if ($r['post_time'] > $last_post_datetime)
						{
							$last_post_datetime = $r['post_time'];
							$r['subject'] = $myts->htmlSpecialChars($r['subject']);
							$forum_lastpost_time  = formatTimestamp($r['post_time'], "m") . "";
							$forum_lastpost_icon = "<a href='".XOOPS_URL."/modules/" . $xoopsModule->dirname() . "/viewtopic.php?post_id=".$r['forum_last_post_id']."&amp;topic_id=".$r['topic_id']."&amp;forum=".$r['forum_id']."#".$r['forum_last_post_id']."'>";
							if ($r['icon']) {
								$forum_lastpost_icon .= "<img src='".XOOPS_URL."/images/subject/".$r['icon']."' border='0' alt='' />";
							} else {
								$forum_lastpost_icon .= "<img src='".XOOPS_URL."/images/subject/icon1.gif' border='0' alt='' />";
							}
							$forum_lastpost_user = "</a>"._MD_BY."&nbsp;";
							if ($r['uid'] != 0 && $r['uname']) {
								$forum_lastpost_user .= "<a href='".XOOPS_URL."/userinfo.php?uid=".$r['uid']."'>" . $myts->htmlSpecialChars($r['uname'])."</a>";
							} else {
								$forum_lastpost_user .= $xoopsConfig['anonymous'];
							}
						}
					}
				}
			}
			$forums[] = array( 'forum_id' => $forum_data['forum_id'],
			'forum_name' => $forum_data['forum_name'],
			'forum_desc' => $forum_data['forum_desc'],
			'forum_posts' => $total_posts,
			'forum_topics' => $total_topics,
			'forum_type' => $forum_data['forum_type'],
			'forum_lastpost_time' => $forum_lastpost_time,
			'forum_lastpost_icon' => $forum_lastpost_icon,
			'forum_lastpost_user' => $forum_lastpost_user,
			'forum_folder' => $forum_folder,
			'forum_moderators' => get_moderators($forum_data['forum_id']),
			'forum_permission' => $forumpermissions,
			'subforum' => $sf_links2
			);
		}
	}
	
	
	if ( is_object($xoopsUser) ) {
		$xoopsModule = XoopsModule::getByDirname("newbb");
		if ( $xoopsUser->isAdmin($xoopsModule->mid()) ) {
			$xoopsTpl->assign('forum_index_cpanel',"<b>( <a href='admin/index.php' target='_blank'>Admin CP</a> ) ");
			
		}
	}
	
	if (isset($catpermission[$onecat->cat_id]['forum_cat_access'])) {
		$permission=  1;
	}
	else
	{
		$permission = 0;
	}
	
	$categories = array('cat_id' => $onecat->cat_id,
	'cat_title' => $myts->htmlSpecialChars($onecat->cat_title),
	'cat_description' => $myts->htmlSpecialChars($onecat->cat_description),
	'permission' => $permission,
	'forums' => $forums
	);
	
	$xoopsTpl->append("categories", $categories);
	
}

$xoopsTpl->assign(array("img_hotfolder" => $forumImage['newposts_forum'], "img_folder" => $forumImage['folder_forum'], "img_locked_nonewposts" => $forumImage['locked_forum'],"img_locked_newposts" => $forumImage['locked_forum_newposts']));
include_once XOOPS_ROOT_PATH.'/footer.php';

?>