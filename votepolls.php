<?php
// ------------------------------------------------------------------------- //
//                XOOPS - PHP Content Management System                      //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
// Based on:								     //
// myPHPNUKE Web Portal System - http://myphpnuke.com/	  		     //
// PHP-NUKE Web Portal System - http://phpnuke.org/	  		     //
// Thatware - http://thatware.org/					     //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
// ------------------------------------------------------------------------- //

// File by xtremdj in order to vote a polls in the newbb forum of xoops RC3

include("header.php");

include_once($forumPath['path']."class/forumpoll.php");
include_once($forumPath['path']."class/forumpolloption.php");
include_once($forumPath['path']."class/forumpolllog.php");
include_once($forumPath['path']."class/forumpollrenderer.php");

if ( !empty($_POST['poll_id']) ) {
	$poll_id = intval($_POST['poll_id']);
} elseif (!empty($HTTP_GET_VARS['poll_id'])) {
	$poll_id = intval($HTTP_GET_VARS['poll_id']);
}

if ( !empty($_POST['option_id']) ) {
	$mail_author = false;
	$poll = new ForumPoll($poll_id);
	
		if ( $xoopsUser ) {
			if ( ForumPollLog::hasVoted($poll_id, $REMOTE_ADDR, $xoopsUser->getVar("uid")) ) {
				$msg = _PL_ALREADYVOTED;
				cookie("bb_polls[$poll_id]", 1);
			} else {
				$poll->vote($_POST['option_id'], '', $xoopsUser->getVar("uid"));
				$poll->updateCount();
				$msg = _PL_THANKSFORVOTE;
				cookie("bb_polls[$poll_id]", 1);
			}
		} else {
			if ( ForumPollLog::hasVoted($poll_id, $REMOTE_ADDR) ) {
				$msg = _PL_ALREADYVOTED;
				cookie("bb_polls[$poll_id]", 1);
			} else {
				$poll->vote($_POST['option_id'], $REMOTE_ADDR);
				$poll->updateCount();
				$msg = _PL_THANKSFORVOTE;
				cookie("bb_polls[$poll_id]", 1);
			}
		}

	redirect_header($forumPath['url']."viewtopic.php?topic_id=$topic_id&forum=$forum&poll_id=$poll_id", 1, $msg);
	exit();
}
redirect_header($forumPath['url']."viewtopic.php?topic_id=$topic_id&forum=$forum", 1, "You must choose an option !!");
?>
