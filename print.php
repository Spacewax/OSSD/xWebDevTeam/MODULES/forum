<?php
// $Id: print.php,v 1.2 2004/05/16 23:29:35 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
// ------------------------------------------------------------------------- //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //

include 'header.php';

$forum = isset($_GET['forum']) ? intval($_GET['forum']) : 0;
$topic_id = isset($_GET['topic_id']) ? intval($_GET['topic_id']) : 0;
if ( empty($forum) ) {
	redirect_header('index.php',2,_MD_ERRORFORUM);
	exit();
} elseif ( empty($topic_id) ) {
	redirect_header('viewforum.php?forum='.$forum,2,_MD_ERRORTOPIC);
	exit();
}


$sql = 'SELECT forum_type, forum_access FROM '.$xoopsDB->prefix('bb_forums').' WHERE forum_id = '.$forum;

if ( !$result = $xoopsDB->query($sql) ) {
	redirect_header('viewforum.php?forum='.$forum,2,_MD_ERROROCCURED);
	exit();
}

if ( !$forumdata = $xoopsDB->fetchArray($result) ) {
	redirect_header('viewforum.php?forum='.$forum,2,_MD_FORUMNOEXIST);
	exit();
}

if ( $forumdata['forum_type'] == 1 ) {
	// this is a private forum.
	$accesserror = 0;
	if ( $xoopsUser ) {
		if ( !$xoopsUser->isAdmin($xoopsModule->mid()) ) {
			if ( !check_priv_forum_auth($xoopsUser->getVar('uid'), $forum, false) ) {
				$accesserror = 1;
			}
		} else {
			$isadminormod = 1;
		}
	} else {
		$accesserror = 1;
	}
	if ( $accesserror == 1 ) {
		redirect_header("index.php",2,_MD_NORIGHTTOACCESS);
		exit();
	}
	$can_post = 1;
	$show_reg = 1;
} else {
	// this is not a priv forum
	if ( $forumdata['forum_access'] == 1 ) {
		// this is a reg user only forum
		if ( $xoopsUser ) {
			$can_post = 1;
		} else {
			$show_reg = 1;
		}
	} elseif ( $forumdata['forum_access'] == 2 ) {
		// this is an open forum
		$can_post = 1;
	} else {
		// this is an admin/moderator only forum
		if ( $xoopsUser ) {
			if ( $xoopsUser->isAdmin($xoopsModule->mid()) || is_moderator($forum, $xoopsUser->getVar('uid')) ) {
				$can_post = 1;
				$isadminormod = 1;
			}
		}
	}
}


function PrintPage($topic_id)
{
	global $xoopsDB, $xoopsConfig, $myts, $eh, $mytree, $xoopsModuleConfig, $xoopsModule,$XoopsUser;
    $user = new XoopsUser();
	
	//CABECERA
	echo "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'>\n";
    echo "<html>\n<head>\n";
    echo "<title>" . $xoopsConfig['sitename'] . "</title>\n";
    echo "<meta http-equiv='Content-Type' content='text/html; charset=" . _CHARSET . "' />\n";
    echo "<meta name='AUTHOR' content='" . $xoopsConfig['sitename'] . "' />\n";
    echo "<meta name='COPYRIGHT' content='Copyright (c) 2004 by " . $xoopsConfig['sitename'] . "' />\n";
    echo "<meta name='DESCRIPTION' content='" . $xoopsConfig['slogan'] . "' />\n";
    echo "<meta name='GENERATOR' content='" . XOOPS_VERSION . "' />\n\n\n";
    echo "<body bgcolor='#ffffff' text='#000000' onload='window.print()'>";

	
	//PRIMER BUCLE PILLO TODOS LOS POSTS
	$result = $xoopsDB->query("select post_id, post_time, uid, subject FROM ".$xoopsDB->prefix("bb_posts")."  where topic_id=$topic_id ORDER BY post_time ASC");
    while(list($post_id, $post_time, $uid, $subject) = $xoopsDB->fetchRow($result)) 
	{
		
		$user_name=$user->getUnameFromId($uid);
		$time=formatTimestamp($post_time, "m");
		//SEGUNDO BUCLE, PARA CADA POST SACO TODOS SUS DATOS
		$result2 = $xoopsDB->query("select post_text FROM ".$xoopsDB->prefix("bb_posts_text")."  where post_id=$post_id ");
		while(list($post_text) = $xoopsDB->fetchRow($result2)) 
		{
			echo "<table border=1 cellpadding=2 cellspacing=1 align=center width=100%>";
			echo "
				<tr>
					<td width = 25%><b>"._MD_DATE."</b></td><td>$time</td></tr>
				<tr>
					<td width = 25%><b>"._MD_USERNAME.":</b></td> <td>$user_name</td></tr>
				<tr>
					<td width = 25%><b>"._MD_POST."</b></td><td>$post_text</td></tr>";

			echo "</table>";
			echo "<br />";
		}
	}
	echo "</body></html>";
}

PrintPage($topic_id);

?>