<?php
// $Id: class.whoisonline.php,v 1.3 2004/05/29 17:11:48 praedator Exp $  
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //

include_once XOOPS_ROOT_PATH."/class/xoopstree.php";

class WhosOnline
{
	var $forum_id;
    var $db;


	function WhosOnline($forum_id)
        {
         
		$this->forum_id = $forum_id;

		$this->remove_expired();
		$this->remove_exisiting();
		$this->add_entry();
	}
	
	function remove_expired()
	{
		$db =& Database::getInstance();

		$sql =  sprintf('DELETE FROM '.$db->prefix('bb_whoisonline').' where timestamp < '.(strtotime("now")-300).'');
                
		if(!$result = $db->queryF($sql))
                {
                  die ( $sql."<br>".$db->error() );
                }
	}

	function remove_exisiting()
	{
		global $xoopsUser, $_SERVER;
		$db =& Database::getInstance();

		$sql = '';
		if ($xoopsUser)
		{
			// remove entry for this user
			$sql = sprintf("DELETE FROM ".$db->prefix("bb_whoisonline")." where user_id='".$xoopsUser->getVar("uid")."'");
		}
		else
		{
			// remove entry for this ip address
			$sql = sprintf("DELETE FROM ".$db->prefix("bb_whoisonline")." where user_ip='".$_SERVER["REMOTE_ADDR"]."'");
		}
                if(!$result = $db->queryF($sql)) die ( $sql."<br>".$db->error() );
	}

	function add_entry()
	{
		global $xoopsUser, $_SERVER;
		$db =& Database::getInstance();

		$sql = "";
		if ($xoopsUser)
                {
                                     $sql = sprintf("INSERT INTO %s (forum_id, user_id, timestamp) VALUES ('%u', %u, '%u')", $db->prefix("bb_whoisonline"), $this->forum_id, $xoopsUser->getVar('uid'), strtotime("now"));

                }
		else
		{
                                      $sql = sprintf("INSERT INTO %s (forum_id, user_ip, timestamp) VALUES ('%u','%s' , '%u')", $db->prefix("bb_whoisonline"), $this->forum_id,$_SERVER['REMOTE_ADDR'], strtotime("now") );

				}

            	if(!$result = $db->queryF($sql)){ die ( $sql."<br>".$db->error() );}
	}

	function show_online_list()
	{
		if ($this->forum_id == 0){
			$content=$this->show_online_all();
                        return $content;
                }
                else
                {
			$content=$this->show_online_forum();
                        return $content;
                }
	}

	function show_online_forum()
	{
		global $xoopsUser, $forumWidth, $xoopsModule, $xoopsModuleConfig, $forumImage;
        include_once(XOOPS_ROOT_PATH . "/modules/" . $xoopsModule->dirname() . "/class/class.permission.php");
        $db =& Database::getInstance();

		// Total users
		$sql = "select count(*) from ".$db->prefix("bb_whoisonline")." where forum_id=".$this->forum_id."";
		$num_total = $db->getRowsNum($db->query($sql));

$content= "<table border='0' cellpadding='0' cellspacing='0' align='center' valign='top' width='.$forumWidth.'><tr><td class='head'>";
$content.="<table border='0' cellpadding='4' cellspacing='1' width='100%'>";
$content.="<tr class='even' align='left'>";
$content.="<td align='left' nowrap='nowrap' colspan=2><b>". _MD_USERS_ONLINE."  ".$num_total." "._MD_BROWSING_FORUM."<br>";
$content.="</b></td></tr> <tr class='odd' align='left'><td align='middle'><img src=".$forumImage['whosonline']."></td>";
$content.="<td width='100%'>";



		// Count of anonymous users (ip address)
		$sql = "select * from ".$db->prefix("bb_whoisonline")." where forum_id=".$this->forum_id." AND user_ip!=''";
		$num_anon = $db->getRowsNum($db->query($sql));

		// Registered Users
		$sql = "select user_id from ".$db->prefix("bb_whoisonline")." where forum_id=".$this->forum_id." AND user_id!=''";
		$result = $db->query($sql);
		$num_reg = $db->getRowsNum($result);
        $user_id="";


		//echo sprintf(_MD_TOTAL_ONLINE, ($num_anon+$num_reg));
		$content.="&nbsp;[ <font color=".$xoopsModuleConfig['wol_admin_col'].">"._MD_ADMINISTRATOR."</font> ]";
		$content.="&nbsp;[ <font color=".$xoopsModuleConfig['wol_mod_col'].">"._MD_MODERATOR."</font> ]";
		$content.="<br>";

		$content.=$num_anon.' '._MD_ANONYMOUS_USERS;
                $content.="<br>";
		$content.=$num_reg.' '._MD_REGISTERED_USERS;
                while ($row = $db->fetchArray($result))
                {

                        $user_id[]=$row;
                }
                        $user_count = count($user_id);
                        for ( $i = 0; $i < $user_count; $i++ ){
                        $user = new XoopsUser($user_id[$i]['user_id']);

                        if (!$user){}

                        else
                        {

                        $perm = new Permissions();
                        if($user->isAdmin($xoopsModule->mid()))
                        {
                                $content.=" <a href='".XOOPS_URL."/userinfo.php?uid=".$user->getVar('uid')."'><font color='".$xoopsModuleConfig['wol_admin_col']."'>".$user->getVar('uname')."</font></a>";
                        }

                        else if ($perm->is_moderator($this->forum_id, $user->getVar('uid')))
                        {
                                $content.=" <a href='".XOOPS_URL."/userinfo.php?uid=".$user->getVar('uid')."'><font color='".$xoopsModuleConfig['wol_mod_col']."'>".$user->getVar('uname')."</font></a>";
                        }
                        else
                        {
                                       $content.=" <a href='".XOOPS_URL."/userinfo.php?uid=".$user->getVar('uid')."'>".$user->getVar('uname')."</a>";
                        }
                        }
                        }


$content.="</td></tr></table></table>";

return $content;
	}

	function show_online_all()
	{
		global $xoopsUser, $forumWidth, $xoopsModule, $xoopsModuleConfig, $forumImage;
        include_once(XOOPS_ROOT_PATH . "/modules/" . $xoopsModule->dirname() . "/class/class.permission.php");
        $db =& Database::getInstance();
		// Total users
		$sql = "select count(*) from ".$db->prefix("bb_whoisonline");
		$num_total = $db->getRowsNum($db->query($sql));

$content= "<table border='0' cellpadding='0' cellspacing='0' align='center' valign='top' width='.$forumWidth.'><tr><td class='head'>";
$content.="<table border='0' cellpadding='4' cellspacing='1' width='100%'>";
$content.="<tr class='even' align='left'>";
$content.="<td align='left' nowrap='nowrap' colspan=2><b>". _MD_USERS_ONLINE."  ".$num_total." "._MD_BROWSING_FORUM."<br>";
$content.="</b></td></tr> <tr class='odd' align='left'><td align='middle'><img src=".$forumImage['whosonline']."></td>";
$content.="<td width='100%'>";


		// Count of anonymous users (ip address)
		$sql = "select * from ".$db->prefix("bb_whoisonline")." where user_ip!=''";
		$num_anon = $db->getRowsNum($db->query($sql));

		// Registered Users
		$sql = "select user_id from ".$db->prefix("bb_whoisonline")." where user_id!=''";
        $result = $db->query($sql);
        $num_reg = $db->getRowsNum($result);
        $user_id="";



		$content.=sprintf(_MD_TOTAL_ONLINE, ($num_anon+$num_reg));
		$content.="&nbsp;[ <font color='".$xoopsModuleConfig['wol_admin_col']."'>"._MD_ADMINISTRATOR."</font> ]";
		$content.="&nbsp;[ <font color='".$xoopsModuleConfig['wol_mod_col']."'>"._MD_MODERATOR."</font> ]";
		$content.="<br>";

		$content.=$num_anon." "._MD_ANONYMOUS_USERS."<br>";
		$content.=$num_reg." "._MD_REGISTERED_USERS;

		while ($row = $db->fetchArray($result))
		{

                        $user_id[]=$row;
                }
                        $user_count = count($user_id);
                        for ( $i = 0; $i < $user_count; $i++ ){
                        $user = new XoopsUser($user_id[$i]['user_id']);

                        if (!$user){}

                        else
                        {

			$perm = new Permissions();
			if($user->isAdmin($xoopsModule->mid()))
                        {
				$content.=" <a href='".XOOPS_URL."/userinfo.php?uid=".$user->getVar('uid')."'><font color='".$xoopsModuleConfig['wol_admin_col']."'>".$user->getVar('uname')."</font></a>";
                        }
                        else if ($perm->is_moderator($this->forum_id, $user->getVar('uid')))
                        {
                        	$content.=" <a href='".XOOPS_URL."/userinfo.php?uid=".$user->getVar('uid')."'><font color='".$xoopsModuleConfig['wol_mod_col']."'>".$user->getVar('uname')."</font></a>";
                        }
                        else
                        {
                               	$content.=" <a href='".XOOPS_URL."/userinfo.php?uid=".$user->getVar('uid')."'>".$user->getVar('uname')."</a>";
                        }
                        }
                        }



$content.= "</td></tr></table></table>";

return $content;
	}
}

?>