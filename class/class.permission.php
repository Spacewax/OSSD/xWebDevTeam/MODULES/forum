<?php
// $Id: class.permission.php,v 1.4 2004/05/29 17:23:31 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //

class Permissions
{
	var $db;

	/*
	* Checks if a user (user_id) is a moderator of a perticular forum (forum_id)
	* Retruns 1 if TRUE, 0 if FALSE or Error
	*/
	function is_moderator($user_id, $forum_mods)
	{
		
		$ret = false;
		$forum_mods = explode(" ", $forum_mods);
		for ($i=0; $i<count($forum_mods); $i++)
		{
			if (@in_array($forum_mods[$i], $user_id))
			{
				$ret = true;
			}
			else
			{
				$ret = false;
			}
		}
		
		
		return $ret;
	}
	
	/**
	* Saves permissions for the selected topic
	*
	*  saveItemPermissions()
	*
	* @param array $groups : group with granted permission
	* @param integer $itemID : topicID on which we are setting permissions
	* @return boolean : TRUE if the no errors occured
	**/
	function saveItemPermissions($groups, $itemID)
	{
		global $xoopsModule;
		
		$result = true;
		$module_id = $xoopsModule->getVar('mid')   ;
		$gperm_handler =& xoops_gethandler('groupperm');
		
		// First, if the permissions are already there, delete them
		$gperm_handler->deleteByModule($module_id, 'item_read', $itemID);
		
		// Save the new permissions
		if (count($groups) > 0) {
			foreach ($groups as $group_id) {
				$gperm_handler->addRight('item_read', $itemID, $group_id, $module_id);
			}
		}
		return $result;
	}
	
	/**
	* Saves permissions for the selected category
	*
	*  saveCategory_Permissions()
	*
	* @param array $groups : group with granted permission
	* @param integer $categoryID : categoryID on which we are setting permissions for Categories and Forums
	* @param string $perm_name : name of the permission
	* @return boolean : TRUE if the no errors occured
	**/
	
	function saveCategory_Permissions($groups, $categoryID, $perm_name)
	{
		global $xoopsModule;
		
		$result = true;
		$module_id = $xoopsModule->getVar('mid')   ;
		$gperm_handler =& xoops_gethandler('groupperm');
		
		// First, if the permissions are already there, delete them
		$gperm_handler->deleteByModule($module_id, $perm_name, $categoryID);
		
		// Save the new permissions
		if (count($groups) > 0) {
			foreach ($groups as $group_id) {
				$gperm_handler->addRight($perm_name, $categoryID, $group_id, $module_id);
			}
		}
		return $result;
	}
	
	/*
	* Returns permissions for a certain type
	*
	* @param string $type "global", "forum" or "topic" (should perhaps have "post" as well - but I don't know)
	* @param int $id id of the item (forum, topic or possibly post) to get permissions for
	*
	* @return array
	*/
	function getPermissions($type = "global", $id = null) {
		global $xoopsDB, $xoopsUser, $xoopsModule;
		//Get group permissions handler
		$gperm_handler =& xoops_gethandler('groupperm');
		//Get user's groups
		$groups = $xoopsUser ? $xoopsUser->getGroups() : array(XOOPS_GROUP_ANONYMOUS);
		//Create string of groupid's separated by commas, inserted in a set of brackets
		$groupstring = "(";
		foreach ($groups as $key => $groupid) {
			if (isset($counter)) {
				$groupstring .= ",";
			}
			$groupstring .=  $groupid;
			$counter = 1;
		}
		$groupstring .= ")";
		
		//Create criteria for getting only the permissions regarding this module and this user's groups
		$criteria = new CriteriaCompo(new Criteria('gperm_modid', $xoopsModule->getVar('mid')));
		$criteria->add(new Criteria('gperm_groupid', $groupstring, 'IN'));
		if ($id != null) {
			if (is_array($id)) {
				$counter = 0;
				$idstring = "(";
				foreach ($id as $thisid) {
					if ($counter) {
						$idstring .= ",";
					}
					$idstring .=  intval($thisid);
					$counter = 1;
				}
				$idstring .= ")";
				$criteria->add(new Criteria('gperm_itemid', $idstring, 'IN'));
			}
			else {
				$criteria->add(new Criteria('gperm_itemid', intval($id)));
			}
		}
		
		
		switch ($type) {
			case "topic":
			$gperm_names = "'forum_can_post', 'forum_can_view', 'forum_can_reply', 'forum_can_edit', 'forum_can_delete', 'forum_can_addpoll', 'forum_can_vote', 'forum_can_attach'";
			break;
			
			case "forum":
			$gperm_names = "'global_forum_access'";
			break;
			
			case "global":
			$gperm_names = "'forum_cat_access'";
			break;
		}
		
		//Add criteria for gpermnames
		$criteria->add(new Criteria('gperm_name', "(".$gperm_names.")", 'IN'));
		//Get all permission objects in this module and for this user's groups
		$userpermissions =& $gperm_handler->getObjects($criteria, true);
		
		$permissions = array();
		//Set the granted permissions to 1
		foreach ($userpermissions as $gperm_id => $gperm) {
			$permissions[$gperm->getVar('gperm_itemid')][$gperm->getVar('gperm_name')] = 1;
		}
		
		//Return the permission array
		return $permissions;
	}
	
	function permission_table($permission_set,$forumid=0)
	{
		$perm ="<table><tr><td align=left><font size='-3'>";
		if (isset($permission_set[$forumid]['forum_can_post'])) {
			$perm .= _MD_CAN_POST;
		}
		else
		{
			$perm .= _MD_CANNOT_POST;
		}
		if (isset($permission_set[$forumid]['forum_can_reply'])) {
			$perm .= _MD_CAN_REPLY;
		}
		else
		{
			$perm .= _MD_CANNOT_REPLY;
		}
		if (isset($permission_set[$forumid]['forum_can_edit'])) {
			$perm .= _MD_CAN_EDIT;
		}
		else
		{
			$perm .= _MD_CANNOT_EDIT;
		}
		if (isset($permission_set[$forumid]['forum_can_delete'])) {
			$perm .= _MD_CAN_DELETE;
		}
		else
		{
			$perm .= _MD_CANNOT_DELETE;
		}
		if (isset($permission_set[$forumid]['forum_can_addpoll'])) {
			$perm .= _MD_CAN_ADDPOLL;
		}
		else
		{
			$perm .= _MD_CANNOT_ADDPOLL;
		}
		if (isset($permission_set[$forumid]['forum_can_vote'])) {
			$perm .= _MD_CAN_VOTE;
		}
		else
		{
			$perm .= _MD_CANNOT_VOTE;
		}
		if (isset($permission_set[$forumid]['forum_can_attach'])) {
			$perm .= _MD_CAN_ATTACH;
		}
		else
		{
			$perm .= _MD_CANNOT_ATTACH;
		}
		
		$perm .= "</font></td></tr></table>";
		
		return $perm;
	}
	
	
}
?>
