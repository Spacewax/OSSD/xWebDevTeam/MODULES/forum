<?php
// $Id: class.forumpost.php,v 1.3 2004/05/29 17:11:48 praedator Exp $  //  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //

include_once(XOOPS_ROOT_PATH."/class/module.errorhandler.php");
include_once XOOPS_ROOT_PATH."/class/xoopstree.php";

class ForumPosts
{
	var $post_id;
	var $topic_id;
	var $forum_id;
	var $post_time;
	var $poster_ip;
	var $order;
	var $subject;
	var $post_text;
	var $pid;
        var $allow_html    = 0;
        var $allow_smileys = 1;
        var $allow_bbcode  = 1;
        var $type;
	var $uid;
	var $icon;
	var $attachsig;
	var $prefix;
	var $db;
	var $istopic = false;
	var $islocked = false;
	var $upload;
	var $attachment = '';
        var $istopic = 0;
        var $approved = 1;

function ForumPosts($id=null){
$this->db =& Database::getInstance();
if ( is_array($id) ) {
        $this->makePost($id);
} elseif ( isset($id) ) {
	$this->getPost(intval($id));
}
}

function getTopicId() {
	return isset($this->topic_id) ? $this->topic_id : 0;
}

        //---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function setTopicId($value) {
        $this->topic_id = $value;
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function setOrder($value) {
        $this->order = $value;
}



function getAllPostsPrint($perpage=0, $start=0) {
$db =& Database::getInstance();
$sql = "SELECT * FROM ".$db->prefix('bb_posts')."
        WHERE
        topic_id = ".$this->topic_id."
        ORDER BY
        ".$this->order."";

$result  = $db->query($sql, $perpage, $start);
$numrows = $db->getRowsNum($result);
$ret     = array();

if ( $numrows == 0 ) {
        return $ret;
}

while ( $myrow = $db->fetchArray($result) ) {
        $ret[] = new ForumPosts($myrow);
}

return $ret;
}
//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function getTopPosts() {
$db =& Database::getInstance();


$sql = "SELECT p.*, t.post_text FROM ".$db->prefix('bb_posts')." p,
        ".$db->prefix('bb_posts_text')." t
        WHERE
        p.topic_id = ".$this->topic_id."
        AND
        p.pid = ".$this->pid."
        AND t.post_id = p.post_id
        AND p.approved='1'
        ORDER BY
        ".$this->order."";

$ret     = array();
$result  = $db->query($sql);
$numrows = $db->getRowsNum($result);

if ( $numrows == 0 ) {
        return $ret;
}

while ( $myrow = $db->fetchArray($result) ) {
        $ret[] = new ForumPosts($myrow);
}

return $ret;

}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function getPostTree($pid) {
$db =& Database::getInstance();

$mytree = new XoopsTree($db->prefix('bb_posts'), "post_id", "pid");
$parray = array();
$parray = $mytree->getChildTreeArray($pid, "post_id");
$ret = array();

foreach ( $parray as $post ) {
        $ret[] = new ForumPosts($post);
}

return $ret;
}

// 2004-1-12 GIJOE <gij@peak.ne.jp> Added routine to move to the correct
// starting position within a topic thread
function &getAllPosts($topic_id, $order="ASC", $perpage=0, &$start, $post_id=0){
$db =& Database::getInstance();
if( $order == "DESC" ) {
	$operator_for_position = '>' ;
} else {
	$order = "ASC" ;
	$operator_for_position = '<' ;
}
if ($perpage <= 0) {
        $perpage = 10;
}
if (empty($start)) {
	$start = 0;
}
if (!empty($post_id)) {
	$result = $db->query("SELECT COUNT(post_id) FROM ".$db->prefix('bb_posts')." WHERE topic_id=$topic_id AND p.approved='1' AND post_id $operator_for_position $post_id");
	list($position) = $db->fetchRow($result);
	$start = intval($position / $perpage) * $perpage;
}
$sql = 'SELECT p.*, t.post_text FROM '.$db->prefix('bb_posts').' p, '.$db->prefix('bb_posts_text')." t WHERE p.topic_id=$topic_id AND p.post_id = t.post_id AND p.approved='1' ORDER BY p.post_id $order";
$result = $db->query($sql,$perpage,$start);
$ret = array();
while ($myrow = $db->fetchArray($result)) {
	$ret[] = new ForumPosts($myrow);
}
return $ret;
}

function delete_attachment(){
global $xoopsModule,$xoopsModuleConfig;
$sql = "SELECT * FROM ".$this->db->prefix("bb_posts")." WHERE post_id= $this->post_id";
    if (!$result = $this->db->query($sql)) {
         die(_MD_COULDNOTREMOVE);
    }
    while ($o_attach = mysql_fetch_object($result)) {
       if ($o_attach->attachment) {
           $attachment_csvdat = explode("|",$o_attach->attachment);
           unlink(XOOPS_ROOT_PATH.'/modules/'. $xoopsModule->dirname() .'/'.$xoopsModuleConfig['dir_attachments'].'/attachments/'.$attachment_csvdat[1]);
       }
    }
    $sql = sprintf("UPDATE %s SET attachment='' WHERE post_id = %u", $this->db->prefix("bb_posts"), $this->post_id);
    if ( !$result = $this->db->query($sql) ) {
         echo "Could not delete attachment.";
    }
}

function setParent($value){
	$this->pid=$value;
}

function setSubject($value){
	$this->subject=$value;
}

function setText($value){
	$this->post_text=$value;
}

function setUid($value){
	$this->uid=$value;
}

function setForum($value){
	$this->forum_id=$value;
}
function setType($value='user') {
global $myts;
        $this->type = $value;
        $myts->setType($value);
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function setIp($value) {
        $this->poster_ip = $value;
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function setHtml($value=0) {
        $this->allow_html = intval($value);
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function setSmileys($value=0) {
        $this->allow_smileys = intval($value);
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function setBBcode($value=0) {
        $this->allow_bbcode = intval($value);
}
//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function setIcon($value) {
        $this->icon = $value;
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function setNotify($value) {
        $this->notify = intval($value);
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function setAttachsig($value) {
        $this->attachsig = intval($value);
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function setApproved($value){
                $this->approved=$value;
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
//function setAttachment($value) {
//        $this->attachment = $value;
//}

function setAttachment($value) {
     global $xoopsModule,$xoopsModuleConfig;
     if ($this->attachment!='')
     {
        $sql = "SELECT * FROM ".$this->db->prefix("bb_posts")." WHERE post_id= $this->post_id";
        if (!$result = $this->db->query($sql))
        {
             die(_MD_COULDNOTREMOVE);
        }
        while ($o_attach = mysql_fetch_object($result))
        {
          if ($o_attach->attachment)
          {
               $attachment_csvdat = explode("|",$o_attach->attachment);
               unlink(XOOPS_ROOT_PATH.'/modules/'. $xoopsModule->dirname() .'/'.$xoopsModuleConfig['dir_attachments'].'/'.$attachment_csvdat[1]);
          }
        }
        $sql = sprintf("UPDATE %s SET attachment='' WHERE post_id = %u", $this->db->prefix("bb_posts"), $this->post_id);
        if ( !$result = $this->db->query($sql) )
        {
            echo "Could not delete attachment.";
        }
     }
     $this->attachment = $value;
}

	function store() {
		global $myts, $xoopsModule,$xoopsModuleConfig;

		$myts =& MyTextSanitizer::getInstance();
		$subject =$myts->censorString($this->subject);
		$post_text =$myts->censorString($this->post_text);
		$subject = $myts->addSlashes($subject);
		$post_text = $myts->addSlashes($post_text);

		if ( empty($this->post_id) )
		{
			if ( empty($this->topic_id) ) {
				$this->topic_id = $this->db->genId($this->db->prefix("bb_topics")."_topic_id_seq");
				$datetime = time();
				$sql = "INSERT INTO ".$this->db->prefix("bb_topics")." (topic_id, topic_title, topic_poster, forum_id, topic_time) VALUES (".$this->topic_id.",'$subject', ".$this->uid.", ".$this->forum_id.", $datetime)";
                if ( !$result = $this->db->query($sql) ) {
                     if ($this->attachment) {
                        $attachment_csvdat = explode("|",$this->attachment);
                        unlink(XOOPS_ROOT_PATH.'/modules/'. $xoopsModule->dirname() .'/'.$xoopsModuleConfig['dir_attachments'].'/'.$attachment_csvdat[1]);
                     }

                    return false;
                   }
				if ( $this->topic_id == 0 ) {
					$this->topic_id = $this->db->getInsertId();
				}
			}
			if ( !isset($this->nohtml) || $this->nohtml != 1 ) {
				$this->nohtml = 0;
			}
			if ( !isset($this->nosmiley) || $this->nosmiley != 1 ) {
				$this->nosmiley = 0;
			}
			if ( !isset($this->attachsig) || $this->attachsig != 1 ) {
				$this->attachsig = 0;
			}
			$this->post_id = $this->db->genId($this->db->prefix("bb_posts")."_post_id_seq");
			$datetime = time();
			

			$sql = sprintf("INSERT INTO %s (post_id, pid, topic_id, forum_id, post_time, uid, poster_ip, subject, nohtml, nosmiley, icon, attachsig,attachment) VALUES (%u, %u, %u, %u, %u, %u, '%s', '%s', %u, %u, '%s', %u, '%s')", $this->db->prefix("bb_posts"), $this->post_id, $this->pid, $this->topic_id, $this->forum_id, $datetime, $this->uid, $this->poster_ip, $subject, $this->nohtml, $this->nosmiley, $this->icon, $this->attachsig, $this->attachment);
	


			if ( !$result = $this->db->query($sql) ) {
				
                if ($this->attachment) {
                   $attachment_csvdat = explode("|",$this->attachment);
                   unlink(XOOPS_ROOT_PATH.'/modules/'. $xoopsModule->dirname() .'/'.$xoopsModuleConfig['dir_attachments'].'/'.$attachment_csvdat[1]);
                 }
                ErrorHandler::show('0022');

				return false;
   			} else {
				if ( $this->post_id == 0 ) {
					$this->post_id = $this->db->getInsertId();
				}
				$sql = sprintf("INSERT INTO %s (post_id, post_text) VALUES (%u, '%s')", $this->db->prefix("bb_posts_text"), $this->post_id, $post_text);
   				if ( !$result = $this->db->query($sql) ) {
					$sql = sprintf("DELETE FROM %s WHERE post_id = %u", $this->db->prefix("bb_posts"), $this->post_id);
					$this->db->query($sql);
   					return false;
   				}
   			}
			if ( $this->pid == 0 ) {
				$sql = sprintf("UPDATE %s SET topic_last_post_id = %u, topic_time = %u WHERE topic_id = %u", $this->db->prefix("bb_topics"), $this->post_id, $datetime, $this->topic_id);
   				if ( !$result = $this->db->query($sql) ) {
   				}
				$sql = sprintf("UPDATE %s SET forum_posts = forum_posts+1, forum_topics = forum_topics+1, forum_last_post_id = %u WHERE forum_id = %u", $this->db->prefix("bb_forums"), $this->post_id, $this->forum_id);
				$result = $this->db->query($sql);
   				if ( !$result ) {
   				}
			} else {
				$sql = "UPDATE ".$this->db->prefix("bb_topics")." SET topic_replies=topic_replies+1, topic_last_post_id = ".$this->post_id.", topic_time = $datetime WHERE topic_id =".$this->topic_id."";
   				if ( !$result = $this->db->query($sql) ) {
   				}
				$sql = "UPDATE ".$this->db->prefix("bb_forums")." SET forum_posts = forum_posts+1, forum_last_post_id = ".$this->post_id." WHERE forum_id = ".$this->forum_id."";
				$result = $this->db->query($sql);
   				if ( !$result ) {
   				}
			}
		}else{
			if ( $this->istopic() ) {
				$sql = "UPDATE ".$this->db->prefix("bb_topics")." SET topic_title = '$subject' WHERE topic_id = ".$this->topic_id."";
				if ( !$result = $this->db->query($sql) ) {
			 		return false;
			 	}
			}
			if ( !isset($this->nohtml) || $this->nohtml != 1 ) {
				$this->nohtml = 0;
			}
			if ( !isset($this->nosmiley) || $this->nosmiley != 1 ) {
				$this->nosmiley = 0;
			}
			if ( !isset($this->attachsig) || $this->attachsig != 1 ) {
				$this->attachsig = 0;
			}
			$sql = "UPDATE ".$this->db->prefix("bb_posts")." SET subject='".$subject."', nohtml=".$this->nohtml.", nosmiley=".$this->nosmiley.", icon='".$this->icon."', attachsig=".$this->attachsig.", attachment= '".$this->attachment."' WHERE post_id=".$this->post_id."";
			$result = $this->db->query($sql);
			if ( !$result ) {
				return false;
			} else {

				$sql = "UPDATE ".$this->db->prefix("bb_posts_text")." SET post_text = '".$post_text."' WHERE post_id =".$this->post_id."";
				$result = $this->db->query($sql);
				
				if ( !$result ) {
			     		if ($this->attachment) 
							{unlink(XOOPS_ROOT_PATH.'/modules/'. $xoopsModule->dirname() .'/'.$xoopsModuleConfig['dir_attachments'].'/attachments/'.$attachment_pseudoname);}
						else{unlink(XOOPS_ROOT_PATH.'/modules/'. $xoopsModule->dirname() .'/'.$xoopsModuleConfig['dir_attachments'].'/'.$attachment_csvdat[1]);}
					ErrorHandler::show('0022');
					return false;
				}

			}
		}
                build_rss();
		return $this->post_id;
	}

function getPost($id) {
	$sql = 'SELECT p.*, t.post_text, tp.topic_status FROM '.$this->db->prefix('bb_posts').' p LEFT JOIN '.$this->db->prefix('bb_posts_text').' t ON p.post_id=t.post_id LEFT JOIN '.$this->db->prefix('bb_topics').' tp ON tp.topic_id=p.topic_id WHERE p.post_id='.$id;
	$array = $this->db->fetchArray($this->db->query($sql));
	$this->post_id = $array['post_id'];
	$this->pid = $array['pid'];
	$this->topic_id = $array['topic_id'];
	$this->forum_id = $array['forum_id'];
	$this->post_time = $array['post_time'];
	$this->uid = $array['uid'];
	$this->poster_ip = $array['poster_ip'];
	$this->subject = $array['subject'];
	$this->nohtml = $array['nohtml'];
	$this->nosmiley = $array['nosmiley'];
	$this->icon = $array['icon'];
	$this->attachsig = $array['attachsig'];
	$this->post_text = $array['post_text'];
        $this->attachment = $array['attachment'];
        $this->approved = $array['approved'];
        if ($array['pid'] == 0) {
		$this->istopic = true;
	}
	if ($array['topic_status'] == 1) {
		$this->islocked = true;
	}
}

function makePost($array){
	foreach($array as $key=>$value){
		$this->$key = $value;
	}
}

function delete_one() {
global $xoopsDB, $xoopsModule,$xoopsModuleConfig;
        if ($this->istopic()) {
            $this->delete();
            return;
        }

        $sql = "SELECT * FROM ".$this->db->prefix("bb_posts")." WHERE post_id= $this->post_id";
        if (!$result = $this->db->query($sql)) {
            die(_MD_COULDNOTREMOVE);
        }
        $postdata = $xoopsDB->fetchArray($result);
        $Pid=$postdata["pid"];

        $this->delete_attachment();

        $sql = sprintf("DELETE FROM %s WHERE post_id = %u", $this->db->prefix("bb_posts"), $this->post_id);
        if ( !$result = $this->db->query($sql) ) {
            return false;
        }
        $sql = sprintf("DELETE FROM %s WHERE post_id = %u", $this->db->prefix("bb_posts_text"), $this->post_id);
        if ( !$result = $this->db->query($sql) ) {
            echo "Could not remove posts text for Post ID:".$this->post_id.".<br />";
        }
        if ( !empty($this->uid) ) {
            $sql = sprintf("UPDATE %s SET posts=posts-1 WHERE uid = %u", $this->db->prefix("users"), $this->uid);
            if ( !$result = $this->db->query($sql) ) {
                echo "Could not update user posts.";
            }
        }

        $mytree = new XoopsTree($this->db->prefix("bb_posts"), "post_id", "pid");
        $arr = $mytree->getAllChild($this->post_id);
        for ( $i = 0; $i < count($arr); $i++ ) {
            $sql = sprintf("UPDATE %s SET pid = %u WHERE post_id = %u && pid=".$this->post_id, $this->db->prefix("bb_posts"), $Pid, $arr[$i]['post_id']);
            if ( !$result = $this->db->query($sql) ) {
                echo "Could not update post ".$arr[$i]['post_id']."";
            }
        }
}
function delete() {
        global $xoopsDB, $xoopsModule,$xoopsModuleConfig;
        $sql = "SELECT * FROM ".$this->db->prefix("bb_posts")." WHERE post_id= $this->post_id";
        if (!$result = $this->db->query($sql)) {
            //die(_MD_COULDNOTREMOVE);
            return;
        }

        $mytree = new XoopsTree($this->db->prefix("bb_posts"), "post_id", "pid");
        $arr = $mytree->getAllChild($this->post_id);
        for ( $i = 0; $i < count($arr); $i++ ) {
            $post = new ForumPosts($arr[$i]["post_id"]);
            $post -> delete();
            unset($post);
        }

        $this->delete_attachment();

        $sql = sprintf("DELETE FROM %s WHERE post_id = %u", $this->db->prefix("bb_posts"), $this->post_id);
        if ( !$result = $this->db->query($sql) ) {
            return false;
        }
        $sql = sprintf("DELETE FROM %s WHERE post_id = %u", $this->db->prefix("bb_posts_text"), $this->post_id);
        if ( !$result = $this->db->query($sql) ) {
            echo "Could not remove posts text for Post ID:".$this->post_id.".<br />";
        }
        if ( !empty($this->uid) ) {
            $sql = sprintf("UPDATE %s SET posts=posts-1 WHERE uid = %u", $this->db->prefix("users"), $this->uid);
            if ( !$result = $this->db->query($sql) ) {
                echo "Could not update user posts.";
            }
        }

        if ($this->istopic()) {
            $sql = sprintf("DELETE FROM %s WHERE topic_id = %u", $this->db->prefix("bb_topics"), $this->topic_id);
            if ( !$result = $this->db->query($sql) ) {
                echo "Could not delete topic.";
            }
        }
}

function subject($format="Show") {
	$myts =& MyTextSanitizer::getInstance();
	$smiley = 1;
	if ( $this->nosmiley() ) {
		$smiley = 0;
	}
	switch ( $format ) {
		case "Show":
			$subject= $myts->htmlSpecialChars($this->subject,$smiley);
			break;
		case "Edit":
			$subject = $myts->htmlSpecialChars($this->subject);
			break;
		case "Preview":
			$subject = $myts->htmlSpecialChars($myts->stripSlashesGPC($this->subject,$smiley));
			break;
		case "InForm":
			$subject = $myts->htmlSpecialChars($myts->stripSlashesGPC($this->subject));
			break;
	}
	return $subject;
}

function attachment($format="Show"){
        $myts =& MyTextSanitizer::getInstance();
        switch ( $format ) {
            case "Show":
                $text = $this->attachment;
                break;
            case "Edit":
                $text = $myts->htmlSpecialChars($this->attachment);
                break;
        }
        return $text;
}

function text($format="Show"){
	$myts =& MyTextSanitizer::getInstance();
	$smiley = 1;
	$html = 1;
	$forumcodes = 1;
	if ( $this->nohtml() ) {
		$html = 0;
	}
	if ( $this->nosmiley() ) {
		$smiley = 0;
	}
	switch ( $format ) {
		case "Show":
			$text = $myts->displayTarea($this->post_text,$html,$smiley,$forumcodes);
			break;
		case "Edit":
			$text = $myts->htmlSpecialChars($this->post_text);
			break;
		case "Preview":
			$text = $myts->previewTarea($this->post_text,$html,$smiley,$forumcodes);
			break;
		case "InForm":
			$text = $myts->previewTareaInForm($this->post_text);
			break;
		case "Quotes":
			$text = $myts->htmlSpecialChars($this->post_text);
			break;
	}
	return $text;
}

function postid() {
	return $this->post_id;
}
function posttime(){
	return $this->post_time;
}
function uid(){
        return $this->uid;
}
function uname(){
	return XoopsUser::getUnameFromId($this->uid);
}
function posterip(){
return $this->poster_ip;
}

	//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function parent() {
        return intval($this->pid);
}

function topic(){
	return $this->topic_id;
}
function nohtml(){
	return $this->nohtml;
}
function nosmiley(){
	return $this->nosmiley;
}
function icon(){
	return $this->icon;
}
function forum(){
	return $this->forum_id;
}
function attachsig(){
	return $this->attachsig;
}
function prefix(){
	return $this->prefix;
}
function istopic() {
	if ($this->istopic) {
		return true;
	}
	return false;
}
function islocked() {
	if ($this->islocked) {
		return true;
	}
	return false;
}


//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function showPost($isadmin, $permissions, $topic_status, $topic_sticky, $allow_sig) {
global $xoopsConfig, $xoopsModule, $xoopsUser, $myts, $xoopsTpl, $forumUrl, $forumImage;

$att_text = "";
$post_text = $this->text();
if ( $this->uid != 0 ) {
   $eachposter = new XoopsUser($this->uid());
   $poster_rank = $eachposter->rank();
    if ( $poster_rank['image'] != "" ) {
        $poster_rank['image'] = "<img src='".XOOPS_UPLOAD_URL."/".$poster_rank['image']."' alt='' />";
    }
    if ( $eachposter->isActive() ) {

        $poster_status = $eachposter->isOnline() ? _MD_ONLINE : '';
        $profile_image = "<a href='".XOOPS_URL."/userinfo.php?uid=".$eachposter->getVar('uid')."'><img src=\"".XOOPS_URL."/images/icons/profile.gif\" alt='"._PROFILE."' /></a>";

        if ($xoopsUser) {
                $pm_image =  "<a href=\"javascript:openWithSelfMain('".XOOPS_URL."/pmlite.php?send2=1&amp;to_userid=".$eachposter->getVar('uid')."', 'pmlite', 450, 380);\"><img src=\"".XOOPS_URL."/images/icons/pm.gif\" alt='".sprintf(_SENDPMTO, $eachposter->uname())."' /></a>";
        }
        else
        {
                $pm_image = "";
        }

        if ( $isadmin || ($xoopsUser && $eachposter->getVar('user_viewemail')) ) {
                $email_image = "<a href='mailto:".$eachposter->getVar('email')."'><img src=\"".XOOPS_URL."/images/icons/email.gif\" alt='".sprintf(_SENDEMAILTO, $eachposter->uname('E'))."' /></a>";
        }
        else
        {
                $email_image = "";
        }
        if ($eachposter->getVar('url') != '') {
                $www_image = "<a href='".$eachposter->getVar('url')."' target='_blank'><img src=\"".XOOPS_URL."/images/icons/www.gif\" alt='"._VISITWEBSITE."' target='_blank' /></a>";
        }
        else
        {
                $www_image = "";
        }

        if ( $xoopsUser && ($eachposter->getVar('user_icq') != '') ) {
               $icq_image="<img src='http://web.icq.com/whitepages/online?icq=".$eachposter->getVar('user_icq')."&img=5' alt='"._MD_ICQ."'>";
               // $icq_image = "<a href='http://wwp.icq.com/scripts/search.dll?to=".$eachposter->getVar('user_icq')."'><img src=\"".XOOPS_URL."/images/icons/icq_add.gif\" alt='"._MD_ADDTOLIST."' /></a>";
        }
        else
        {
                $icq_image = "";
        }

        if ( $xoopsUser && ($eachposter->getVar('user_aim') != '') ) {
                $aim_image = "<a href='aim:goim?screenname=".$eachposter->getVar('user_aim')."&message=Hi+".$eachposter->getVar('user_aim')."+Are+you+there?'><img src=\"".XOOPS_URL."/images/icons/aim.gif\" alt='aim' /></a>";
        }
        else
        {
                $aim_image = "";
        }

        if ( $xoopsUser && ($eachposter->getVar('user_yim') != '') ) {
                $yim_image = "<a href='http://edit.yahoo.com/config/send_webmesg?.target=".$eachposter->getVar('user_yim')."&.src=pg'><img src=\"".XOOPS_URL."/images/icons/yim.gif\" alt='yim' /></a>";
        }
        else
        {
                $yim_image = "";
        }

        if ( $xoopsUser && ($eachposter->getVar('user_msnm') != '') ) {
                $msnm_image = "<a href='".XOOPS_URL."/userinfo.php?uid=".$eachposter->getVar('uid')."'><img src=\"".XOOPS_URL."/images/icons/msnm.gif\" alt='msnm' /></a>";
        }
        else
        {
                $msnm_image = "";
        }

        if (!$eachposter){
                $posterarr = array('poster_uid' =>0, 'poster_uname' => $xoopsConfig['anonymous'], 'poster_avatar' => '', 'poster_from' => '', 'poster_regdate' => '', 'poster_postnum' => '', 'poster_sendpmtext' => '', 'poster_rank_title' => '', 'poster_rank_image' => '');
                $xoopsTpl->assign('user_pmlink', '');
        }
        if ($this->attachment() != '') {

                     $att = $this->attachment();
                     $attachment_csv = explode("|",$att);
                     $size=number_format ($attachment_csv[2]/1024,2);
                     $filetyp = explode('.', $attachment_csv[0]);
                     $extensions=array("jpg","gif","png","bmp");
                     if (@in_array($filetyp[1], $extensions)){
                         $filetyp = $filetyp[count($filetyp)-1];
                         $att_text .='<b>'._MD_THIS_FILE_WAS_ATTACHED_TO_THIS_POST.'</b>&nbsp;<img src="'.XOOPS_URL.'/modules/'.$xoopsModule->dirname().'/images/filetypes/'.$filetyp.'.gif" alt="'.$filetyp.'" border="0"></img><b>&nbsp; '.$attachment_csv[0].'</b>';
                         $att_text .='<hr size="1" noshade="noshade">';
                         $att_text .='<br><img src="'.XOOPS_URL.'/modules/'.$xoopsModule->dirname().'/attachments/'.$attachment_csv[1].'"alt="" onmousewheel="java script:return newbb_img(event,this)" onload="java script:if(this.width>screen.width-200)this.width=screen.width-200">';
                     }
                     else
                     {
                      $filetyp = $filetyp[count($filetyp)-1];
                      $att_text .='<br><br>';
                      $att_text .='<table width="80%" bordercolor="#000000" border="1" cellspacing="1">';
                      $att_text .='<tr><td colspan="3" class="head"><b>'._MD_ATTACHMENT.'</b></td></tr>';
                      $att_text .='<tr class="even"><td width="50%"><b>'._MD_THIS_FILE_WAS_ATTACHED_TO_THIS_POST.'</b></td><td><b>'._MD_FILESIZE.'</b></td><td><b>'._MD_HITS.'</b></td></tr>';
                      $att_text .='<tr bgcolor="#ffffff"><td><a href="'.XOOPS_URL.'/modules/'.$xoopsModule->dirname().'/dl_attachment.php?attachid='.$this->postid().'" target="new"> <img src="'.XOOPS_URL.'/modules/'.$xoopsModule->dirname().'/images/filetypes/'.$filetyp.'.gif" alt="'.$filetyp.'" border="0"> '.$attachment_csv[0].'</a></td>';
                      $att_text .= '<td> '.$size.' KB</td>';
                      $att_text .= '<td> '.$attachment_csv[3].' </td>';
                      $att_text .='</tr></table>';
                    }
                    }

                    $RPG = $eachposter->getVar('posts');
                    $level = get_user_level($eachposter->getVar('uid'));
                    $RPG_HP = "<br />Level: ".$level['LEVEL']."<br />HP : ".$level['HP']." / ".$level['HP_MAX']."<br /><table width='100%' border='0' cellspacing='0' cellpadding='0' bordercolor='#000000'><tr><TD width=3 height=13><IMG height=13 src='images/rpg/img_left.gif' width=3></TD><TD width='100%' background=images/rpg/img_backing.gif height=13><img src='images/rpg/orange.gif' width='".$level['HP_WIDTH']."%' height='9'><img src='images/rpg/hp.gif' height='9'></td><TD width=3 height=13><IMG height=13 src='images/rpg/img_right.gif' width=3></TD></tr></table>";
                    $RPG_MP = "MP : ".$level['MP']." / ".$level['MP_MAX']."<br><table width='100%' border='0' cellspacing='0' cellpadding='0' bordercolor='#000000'><tr><TD width=3 height=13><IMG height=13 src='images/rpg/img_left.gif' width=3></TD><TD width='100%' background=images/rpg/img_backing.gif height=13><img src='images/rpg/green.gif' width='".$level['MP_WIDTH']."%' height='9'><img src='images/rpg/mp.gif' height='9'></td><TD width=3 height=13><IMG height=13 src='images/rpg/img_right.gif' width=3></TD></tr></table>";
                    $RPG_EXP = "EXP : ".$level['EXP']."<br><table width='100%' border='0' cellspacing='0' cellpadding='0' bordercolor='#000000'><tr><TD width=3 height=13><IMG height=13 src='images/rpg/img_left.gif' width=3></TD><TD width='100%' background=images/rpg/img_backing.gif height=13><img src='images/rpg/blue.gif' width='".$level['EXP']."%' height='9'><img src='images/rpg/exp.gif' height='9'></td><TD width=3 height=13><IMG height=13 src='images/rpg/img_right.gif' width=3></TD></tr></table>";
                    $posterarr =  array('poster_uid' => $eachposter->getVar('uid'), 'poster_uname' => '<a href="'.XOOPS_URL.'/userinfo.php?uid='.$eachposter->getVar('uid').'">'.$eachposter->getVar('uname').'</a>', 'poster_avatar' => $eachposter->getVar('user_avatar'), 'poster_from' => $eachposter->getVar('user_from'), 'poster_regdate' => formatTimestamp($eachposter->getVar('user_regdate'), 's'), 'poster_postnum' => $RPG .$RPG_HP .$RPG_MP .$RPG_EXP, 'poster_sendpmtext' => sprintf(_SENDPMTO,$eachposter->getVar('uname')), 'poster_rank_title' => $poster_rank['title'], 'poster_rank_image' => $poster_rank['image'], 'poster_groups' => GetGroupsNames($eachposter->getVar('uid')), 'poster_status' => $poster_status,
                    'user_pmlink' => $pm_image, 'user_profile' => $profile_image,'user_icqlink' => $icq_image,'user_email' => $email_image,'user_www' => $www_image, 'user_msnm' => $msnm_image,'user_aim' => $aim_image,'user_yim' => $yim_image);
                    if ( 1 == $allow_sig && $this->attachsig() == 1 && $eachposter->attachsig() == 1 ) {
                        $myts =& MytextSanitizer::getInstance();
                        $post_text .= "<p><br />----------------<br />". $myts->makeTareaData4Show($eachposter->getVar("user_sig", "N"), 0, 1, 1)."</p>";
                    }
                } else {
                        $posterarr = array('poster_uid' =>0, 'poster_uname' => $xoopsConfig['anonymous'], 'poster_avatar' => '', 'poster_from' => '', 'poster_regdate' => '', 'poster_postnum' => '', 'poster_sendpmtext' => '', 'poster_rank_title' => '', 'poster_rank_image' => '');
                }
            } else {
                        $posterarr = array('poster_uid' =>0, 'poster_uname' => $xoopsConfig['anonymous'], 'poster_avatar' => '', 'poster_from' => '', 'poster_regdate' => '', 'poster_postnum' => '', 'poster_sendpmtext' => '', 'poster_rank_title' => '', 'poster_rank_image' => '');
             }



        if ( $xoopsUser ) {
        $xoopsTpl->assign('viewer_userid', $xoopsUser->getVar('uid'));
        if ( !empty($isadmin) ) {
                // yup, the user is admin
                // the forum is locked?
                if ( $topic_status != 1 ) {
                        // nope
                        $xoopsTpl->assign('topic_lock_image', '<a href="'.$forumUrl['root'].'topicmanager.php?mode=lock&amp;topic_id='.$this->topic_id.'&amp;forum='.$this->forum_id.'"><img src="'.$forumImage['locktopic'].'" alt="'._MD_LOCKTOPIC.'" /></a>');
                } else {
                        // yup, it is..
                        $xoopsTpl->assign('topic_lock_image', '<a href="'.$forumUrl['root'].'topicmanager.php?mode=unlock&amp;topic_id='.$this->topic_id.'&amp;forum='.$this->forum_id.'"><img src="'.$forumImage['unlocktopic'].'" alt="'._MD_UNLOCKTOPIC.'" /></a>');
                }
                $xoopsTpl->assign('topic_move_image', '<a href="'.$forumUrl['root'].'topicmanager.php?mode=move&amp;topic_id='.$this->topic_id.'&amp;forum='.$this->forum_id.'"><img src="'.$forumImage['movetopic'].'" alt="'._MD_MOVETOPIC.'" /></a>');
                $xoopsTpl->assign('topic_delete_image', '<a href="'.$forumUrl['root'].'topicmanager.php?mode=del&amp;topic_id='.$this->topic_id.'&amp;forum='.$this->forum_id.'"><img src="'.$forumImage['deltopic'].'" alt="'._MD_DELETETOPIC.'" /></a>');
                // is the topic sticky?
                if ( $topic_sticky != 1 ) {
                        // nope, not yet..
                        $xoopsTpl->assign('topic_sticky_image', '<a href="'.$forumUrl['root'].'topicmanager.php?mode=sticky&amp;topic_id='.$this->topic_id.'&amp;forum='.$this->forum_id.'"><img src="'.$forumImage['sticky'].'" alt="'._MD_STICKYTOPIC.'" /></a>');
                } else {
                        // yup it is sticking..
                        $xoopsTpl->assign('topic_sticky_image', '<a href="'.$forumUrl['root'].'topicmanager.php?mode=unsticky&amp;topic_id='.$this->topic_id.'&amp;forum='.$this->forum_id.'"><img src="'.$forumImage['unsticky'].'" alt="'._MD_UNSTICKYTOPIC.'" /></a>');
                }
                // need to set this also
                $xoopsTpl->assign('viewer_is_admin', true);
        } else {
                // nope, the user is not a forum admin..
                $xoopsTpl->assign('viewer_is_admin', false);
        }
} else {
        // nope, the user is not a forum admin, not even registered
        $xoopsTpl->assign(array('viewer_is_admin' => false, 'viewer_userid' => 0));
}
        $posticon = $this->icon();
        if ( isset($posticon) && $posticon != '' ) {
            $post_image = '<a name="'.$this->postid().'"><img src="'.XOOPS_URL.'/images/subject/'.$this->icon().'" alt="" /></a>';
        } else {
            $post_image =  '<a name="'.$this->postid().'"><img src="'.XOOPS_URL.'/images/icons/posticon.gif" alt="" /></a>';
        }
            $xoopsTpl->append('topic_posts', array_merge($posterarr, array('post_id' => $this->postid(), 'post_parent_id' => $this->parent(), 'post_date' => formatTimestamp($this->posttime(), 'm'), 'post_poster_ip'=> $this->posterip(), 'post_image' => $post_image, 'post_title' => $this->subject(), 'post_text' => $post_text.$att_text)));
unset($eachposter);
}


//---------------------------------------------------------------------------------------//
function showPostForPrint($viewmode, $order, $can_post, $topic_status, $allow_sig, $adminview=0) {
global $xoopsConfig, $xoopsUser, $myts, $xoopsModule, $forumImage;

$post_date    = formatTimestamp($this->posttime(), "m");

if ( $this->uid != 0 ) {
        $poster = new XoopsUser($this->uid());
        if ( !$poster->isActive() ) {
                $poster = 0;
        }
        } else {
                $poster = 0;
        }

if ( isset($this->icon) && $this->icon != "" ) {
        $subject_image = "<a name='".$this->postid()."' id='".$this->postid()."'><img src='".XOOPS_URL."/images/subject/".$this->icon."' alt='' /></a>";
        } else {
                $subject_image =  "<a name='".$this->postid()."' id='".$this->postid()."'><img src='".$forumImage['posticon']."' alt='' /></a>";
        }

$text = $this->text();

        $poster_name = ($poster) ? $poster->uname() : $xoopsConfig['anonymous'];

echo"<table align='left' border='0' width='640' cellpadding='0' cellspacing='1' bgcolor='#000000'><tr><td>
<table border='0' width='640' cellpadding='5' cellspacing='1' bgcolor='#FFFFFF'>
<tr><td width='100%' align='left'>
<h3><img src='".$forumImage['folder']."' align='absmiddle' alt='' />
&nbsp;$poster_name :</h3></td>
<td align='right' nowrap><i>$post_date</i></td></tr>
<tr><td width='100%' colspan='2' align='left'>
$subject_image&nbsp;".$text."</td></tr></table></td></tr></table>";

}



//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function showTreeItem($viewmode, $order) {
global $forumImage, $xoopsConfig, $xoopsUser;
$prefix = str_replace(".", "&nbsp;", $this->prefix());
$date   = formatTimestamp($this->posttime(), "m");

if ($this->icon() != "") {
        $icon = "<img src='".XOOPS_URL."/images/subject/".$this->icon()."' alt='' />";
        } else {
        $icon =  '<a name="'.$this->postid().'"><img src="'.XOOPS_URL.'/images/icons/no_posticon.gif" alt="" /></a>';
       }

$subject = '<a href="viewtopic.php?viewmode=thread&amp;topic_id='.$this->topic().'&amp;forum='.$this->forum().'&amp;post_id='.$this->postid().'#'.$this->postid().'">'.$this->subject().'</a>';

$treeposter = new XoopsUser($this->uid());
if ( $treeposter->isActive() ) {
   $user =  array('poster_uid' => $treeposter->getVar('uid'), 'poster_uname' => '<a href="'.XOOPS_URL.'/userinfo.php?uid='.$treeposter->getVar('uid').'">'.$treeposter->getVar('uname').'</a>');
} else {
   $user = array('poster_uid' =>0, 'poster_uname' => $xoopsConfig['anonymous']);
}

$tree=array('icon' => $icon,
            'prefix' => $prefix,
            'date' => $date,
            'poster' => $user,
            'forum' => $this->forum(),
            'topic_id' => $this->topic(),
            'post_id' => $this->postid(),
            'parent' => $this->parent(),
            'viewmode' => $viewmode,
            'order' => $order,
            'subject' => $subject,
            'uid' => $this->uid(),
            'uname' => $this->uname()
);
unset($treeposter);
return $tree;

}


}

?>