<?php
// $Id: forumpolloption.php,v 1.2 2004/05/16 23:29:35 praedator Exp $
// ------------------------------------------------------------------------- //
//               E-Xoops: Content Management for the Masses                  //
//                       < http://www.e-xoops.com >                          //
// ------------------------------------------------------------------------- //
// Original Author: Kazumi Ono
// Author Website : http://www.mywebaddons.com/ , http://www.myweb.ne.jp
// License Type   : GPL: See /manual/LICENSES/GPL.txt
// ------------------------------------------------------------------------- //

include_once(XOOPS_ROOT_PATH."/class/xoopsobject.php");

class ForumPollOption extends XoopsObject {


	function ForumPollOption($id=NULL) {
	$this->XoopsObject();
	$this->initVar("option_id", "int", NULL, false);
	$this->initVar("poll_id", "int", NULL, false);
	$this->initVar("option_text", "textbox", NULL, true, 255, true);
	$this->initVar("option_count", "int", 0, false);
	$this->initVar("option_color", "other", NULL, false);
	if ( !empty($id) ) {
		if ( is_array($id) ) {
			$this->set($id);
			} else {
				$this->load(intval($id));
			}
		}
	}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function store() {
global $db, $forumTable;

if ( !$this->isCleaned() ) {
	if ( !$this->cleanVars() ) {
		return false;
	}
}

foreach ( $this->cleanVars as $k=>$v ) {
	$$k = $v;
}

if ( empty($option_id) ) {
	$option_id = $db->genId($forumTable['poll_option']."_option_id_seq");
	$sql = "
		INSERT INTO ".$forumTable['poll_option']." SET
		option_id=$option_id,
		poll_id=$poll_id,
		option_text='$option_text',
		option_count=$option_count,
		option_color='$option_color'";

	} else {
		$sql = "UPDATE ".$forumTable['poll_option']." SET option_text='$option_text', option_count='$option_count', option_color='$option_color'  WHERE option_id=".$option_id."";
	}

if ( !$result = $db->query($sql) ) {
	$this->setErrors(_NOTUPDATED);
	return false;
}

if ( empty($option_id) ) {
	return $db->insert_id();
}

return $option_id;
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function load($id) {
global $db, $forumTable;

$sql   = "SELECT * FROM ".$forumTable['poll_option']." WHERE option_id=".$id."";
$myrow = $db->fetch_array($db->query($sql));
$this->set($myrow);
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function delete() {
global $db, $forumTable;

$sql = "DELETE FROM ".$forumTable['poll_option']." WHERE option_id=".$this->getVar("option_id")."";
if ( !$db->query($sql) ) {
	return false;
}

return true;
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function updateCount() {
global $db, $forumTable;

$votes = ForumPollLog::getTotalVotesByOptionId($this->getVar("option_id"));
$sql   = "UPDATE ".$forumTable['poll_option']." SET option_count=$votes WHERE option_id=".$this->getVar("option_id")."";
$db->query($sql);
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function &getAllByPollId($poll_id) {
global $db, $forumTable;

$poll_id = intval($poll_id);
$ret     = array();
$sql     = "
		SELECT
		*
		FROM
		".$forumTable['poll_option']."
		WHERE
		poll_id=$poll_id
		ORDER BY option_id";

$result = $db->query($sql);

while ( $myrow = $db->fetch_array($result) ) {
	$ret[] = new ForumPollOption($myrow);
}

return $ret;
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function deleteByPollId($poll_id) {
global $db, $forumTable;

$sql = "DELETE FROM ".$forumTable['poll_option']." WHERE poll_id=".intval($poll_id);
if ( !$db->query($sql) ) {
	return false;
}

return true;
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function resetCountByPollId($poll_id) {
global $db, $forumTable;

$sql = "UPDATE ".$forumTable['poll_option']." SET option_count=0 WHERE poll_id=".intval($poll_id);
if ( !$db->query($sql) ) {
	return false;
}

return true;
}
//---------------------------------------------------------------------------------------//
}
?>
