<?php

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
* @link{http://my.netscape.com/publish/formats/rss-spec-0.91.html}
*/
class xml_rss {

	var $buffer;
	var $count;

	var $channel_title;
	var $channel_link;
	var $channel_description;

	var $image_title;
	var $image_url;
	var $image_link;
	var $image_description;
	var $image_height;
	var $image_width;

	var $max_items;
	var $max_item_description;

	var $rss_file;

		function xml_rss($rss_file) {
		global $meta;

			$this->rss_file			= $rss_file;
			$this->channel_title		= $meta['title'];
			$this->channel_link		= XOOPS_URL . "/";
			$this->channel_description	= $meta['description'];
			$this->image_title		= $meta['title'];
			$this->image_url			= XOOPS_URL . "/images/button.gif";
			$this->image_link			= XOOPS_URL . "/";
			$this->image_description	= $meta['slogan'];
			$this->image_height		=  31;
			$this->image_width		=  88;
			$this->max_items			=  10;
			$this->max_item_description	= 200;
		}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function save() {

$content  = '<?xml version="1.0" encoding="'._CHARSET.'"?>';
$content .= "\n<rss version='0.92'>\n";
$content .= "<channel>\n";
$content .= "<title>".$this->cleanup($this->channel_title)."</title>\n";
$content .= "<link>".$this->channel_link."</link>\n";
$content .= "<description>".$this->cleanup($this->channel_description)."</description>\n";
$content .= "<image>\n";
$content .= "<title>".$this->cleanup($this->image_title)."</title>\n";
$content .= "<url>".$this->image_url."</url>\n";
$content .= "<link>".$this->image_link."</link>\n";
$content .= "<description>".$this->cleanup($this->image_description)."</description>\n";
$content .= "<width>".$this->image_width."</width>\n";
$content .= "<height>".$this->image_height."</height>\n";
$content .= "</image>\n";
$content .= $this->buffer;
$content .= "</channel>\n";
$content .= "</rss>";

if ( $fp = @fopen($this->rss_file, "w") ) {
	fwrite($fp, $content);
	fclose($fp);
	} else {
		return false;
	}

return true;
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function build($title, $link, $desc='') {

if ($this->count < $this->max_items) {

	if ( !empty($desc) ) {
	$description = $this->cleanup($desc, $this->max_item_description);
	}

	$this->buffer .= "<item>\n";
	$this->buffer .= "<title>".$this->cleanup($title)."</title>\n";
	$this->buffer .= "<link>".$link."</link>\n";

	if ( !empty($desc) ) {
		$this->buffer .= "<description>".$description."...</description>\n";
		}

	$this->buffer .= "</item>\n";

	$this->count++;
	}
}

//---------------------------------------------------------------------------------------//
/**
* Description
*
* @param type $var description
* @return type description
*/
function cleanup($text, $trim=100) {
global $myts;

	$clean = stripslashes($text);
	$clean = $myts->sanitizeForDisplay($clean,1,0,1);
	$clean = strip_tags($clean);
	$clean = substr($clean, 0, $trim);
	$clean = htmlspecialchars($clean, ENT_QUOTES);

return $clean;
}

}
?>