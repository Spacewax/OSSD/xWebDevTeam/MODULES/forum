<?php
// $Id: class.forum.php,v 1.3 2004/05/29 17:11:48 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //

include_once( XOOPS_ROOT_PATH . "/class/xoopstree.php" );
include_once( XOOPS_ROOT_PATH . "/class/module.errorhandler.php" );

class Forums
{
    var $db;
    var $table;
    var $forum_id;
    var $forum_name;
    var $forum_desc;
    var $groups_forum_access;
    var $groups_forum_can_post;
    var $groups_forum_can_view;
    var $groups_forum_can_reply;
    var $groups_forum_can_edit;
    var $groups_forum_can_delete;
    var $groups_forum_can_addpoll;
    var $groups_forum_can_attach;
    var $forum_moderator;
    var $forum_topics;
    var $forum_posts;
    var $forum_last_post_id;
    var $cat_id;
    var $forum_type;
    var $parent_forum;
    var $subforum_count;
    var $allow_html;
    var $allow_sig;
    var $show_name;
    var $show_icons;
    var $show_smilies;
    var $posts_per_page;
    var $hot_threshold;
    var $topics_per_page;
    var $allow_polls;
    var $allow_attachments;
    var $attach_maxkb;
    var $attach_ext;
    var $forum_order;

	function Forums( $forumid = 0)
    {
        $this -> db = & Database :: getInstance();
        $this -> table = $this -> db -> prefix( "bb_forums" );

        if ( is_array( $forumid ) )
        {
            $this -> makeForum( $forumid );
        }elseif ( $forumid != 0 )
        {
            $this -> loadForum( $forumid );
        }
        else
        {
            $this -> forum_id = $forumid;
        }
    }
    function set_groups_forum_access( $value )
	{
		$this -> groups_forum_access = $value;
	}
	
	function set_groups_forum_can_post( $value )
	{
		$this -> groups_forum_can_post = $value;
	}
	
	function set_groups_forum_can_view( $value )
	{
		$this -> groups_forum_can_view = $value;
	}
	
	function set_groups_forum_can_reply( $value )
	{
		$this -> groups_forum_can_reply = $value;
	}
	
	function set_groups_forum_can_edit( $value )
	{
		$this -> groups_forum_can_edit = $value;
	}
	
	function set_groups_forum_can_delete( $value )
	{
		$this -> groups_forum_can_delete = $value;
	}
	
	function set_groups_forum_can_addpoll( $value )
	{
		$this -> groups_forum_can_addpoll = $value;
	}
	
	function set_groups_forum_can_vote( $value )
	{
		$this -> groups_forum_can_vote = $value;
	}
	
	function set_groups_forum_can_attach( $value )
	{
		$this -> groups_forum_can_attach = $value;
	}
	
	function setforum_name( $forum_name )
    {
        $this -> forum_name = $forum_name;
    }
    function setforum_desc( $forum_desc )
    {
        $this -> forum_desc = $forum_desc;
    }

    function setforum_moderator( $forum_moderator )
    {
        $this -> forum_moderator = saveNewbbAccess( $forum_moderator );
    }

    function setcat_id( $cat_id )
    {
        $this -> cat_id = $cat_id;
    }
    function setforum_type( $forum_type )
    {
        $this -> forum_type = $forum_type;
    }
    function setallow_html( $allow_html )
    {
        $this -> allow_html = $allow_html;
    }
    function setallow_sig( $allow_sig )
    {
        $this -> allow_sig = $allow_sig;
    }
    function setshow_name( $show_name )
    {
        $this -> show_name = $show_name;
    }
    function setshow_icons( $show_icons )
    {
        $this -> show_icons = $show_icons;
    }
    function setshow_smilies( $show_smilies )
    {
        $this -> show_smilies = $show_smilies;
    }
    function setposts_per_page( $posts_per_page )
    {
        $this -> posts_per_page = $posts_per_page;
    }
    function sethot_threshold( $hot_threshold )
    {
        $this -> hot_threshold = $hot_threshold;
    }
    function settopics_per_page( $topics_per_page )
    {
        $this -> topics_per_page = $topics_per_page;
    }
    function setallow_polls( $allow_polls )
    {
        $this -> allow_polls = $allow_polls;
    }
    function setallow_attachments( $allow_attachments )
    {
        $this -> allow_attachments = $allow_attachments;
    }
    function setattach_maxkb( $attach_maxkb )
    {
        $this -> attach_maxkb = $attach_maxkb;
    }
    function setattach_ext( $attach_ext )
    {
        $this -> attach_ext = $attach_ext;
    }
    function setforum_order( $forum_order )
    {
        $this -> forum_order = $forum_order;
    }
    function setsubforum_count( $value )
    {
        $this -> subforum_count = $value;
    }
    function setparent_forum( $value )
    {
        $this -> parent_forum = $value;
    }
    
    function getForum( $forum_id )
    {
     
        $sql = "SELECT * FROM " . $this -> table . " WHERE forum_id=" . $forum_id . " ";
        $array = $this -> db -> fetchArray( $this -> db -> query( $sql ) );
        return $array;
    }

	function loadForum( $forum_id )
    {
     
        $sql = "SELECT * FROM " . $this -> table . " WHERE forum_id=" . $forum_id . " ";
        $array = $this -> db -> fetchArray( $this -> db -> query( $sql ) );
        $this -> makeForum( $array );
    }

    function makeForum( $array )
    {
        foreach( $array as $key => $value )
        {
            $this -> $key = $value;
        }
    }

    function store()
    {
        global $myts, $xoopsModule;
        include_once XOOPS_ROOT_PATH . '/modules/' . $xoopsModule ->dirname() . '/class/class.permission.php';
		
        $myts = & MyTextSanitizer :: getInstance();
        
	if ( isset( $this -> forum_name ) && $this -> forum_name != "" )
        {
            $forum_name = $myts -> addSlashes( $this -> forum_name );
        }

	if ( isset( $this -> forum_desc ) && $this -> forum_desc != "" )
        {
            $forum_desc = $myts -> addSlashes( $this -> forum_desc );
        }
      
        if ( !isset( $this -> cat_id ) || !is_numeric( $this -> cat_id ) )
        {
            $this -> cat_id = 0;
        }
        if ( !isset( $this -> forum_type ) )
        {
            $this -> forum_type ='1';
        }
        if ( !isset( $this -> allow_html ) )
        {
            $this ->  allow_html = '1';
        }
         if ( !isset( $this ->  allow_sig ))
        {
            $this -> allow_sig  = '1';
        }
         if ( !isset( $this -> allow_polls  ) )
        {
            $this -> allow_polls = '1';
        }
         if ( !isset( $this -> allow_attachments )  )
        {
            $this -> allow_attachments = '1';
        }
         if ( !isset( $this -> show_name )  )
        {
            $this -> show_name = '1';
        }
         if ( !isset( $this -> show_icons ) )
        {
            $this -> show_icons = '1';
        }
         if ( !isset( $this -> show_smilies ) )
        {
            $this -> show_smilies = '1';
        }
         if ( !isset( $this -> forum_order ) || !is_numeric( $this -> forum_order ) )
        {
            $this -> forum_order = 0;
        }
        if ( !isset( $this -> attach_maxkb ) || !is_numeric( $this -> attach_maxkb ) )
        {
            $this -> attach_maxkb = 0;
        }
        if ( isset( $this -> attach_ext ) && $this -> attach_ext != "" )
        {
            $attach_ext = $myts -> addSlashes( $this -> attach_ext );
        }
        if ( !isset( $this -> posts_per_page ) || !is_numeric( $this -> posts_per_page ) )
        {
            $this -> posts_per_page = 10;
        }
        if ( !isset( $this -> hot_threshold ) || !is_numeric( $this -> hot_threshold ) )
        {
            $this -> hot_threshold = 10;
        }
        if ( !isset( $this -> topics_per_page ) || !is_numeric( $this -> topics_per_page ) )
        {
            $this -> topics_per_page = 20;
        }

        $perm = new Permissions();
        if ( empty( $this -> forum_id ) )
        {
            $this -> forum_id = $this -> db -> genId( $this -> table . "_forum_id_seq" );
             $sql = "INSERT INTO " . $this -> table . " (forum_id, forum_name, forum_desc,parent_forum,
             forum_moderator, forum_topics,forum_posts,forum_last_post_id, cat_id, forum_type, allow_html, allow_sig, show_name,show_icons,show_smilies,posts_per_page,
             hot_threshold, topics_per_page, forum_order, allow_attachments,attach_maxkb, attach_ext, allow_polls,subforum_count)
             VALUES
             (" . $this -> forum_id . ", '" . $forum_name . "', '" . $forum_desc . "',0,
              '" .$this->forum_moderator ."',0,0,0, " . $this->cat_id . ",
              '" . $this->forum_type . "', '" . $this->allow_html . "', '" . $this->allow_sig . "','" . $this->show_name . "',
              '" . $this->show_icons . "','" . $this->show_smilies . "'," . $this->posts_per_page . ",
              " . $this->hot_threshold . ", " . $this->topics_per_page . ", " . $this->forum_order . ",
              '" . $this->allow_attachments . "', " . $this->attach_maxkb . ", '" . $this->attach_ext . "', '" . $this->allow_polls . "',0 )";
        $perm->saveCategory_Permissions($this->groups_forum_access, $this->forum_id, 'global_forum_access');
        $perm->saveCategory_Permissions($this->groups_forum_can_post, $this->forum_id, 'forum_can_post');
        $perm->saveCategory_Permissions($this->groups_forum_can_view, $this->forum_id, 'forum_can_view');
        $perm->saveCategory_Permissions($this->groups_forum_can_reply, $this->forum_id, 'forum_can_reply');
        $perm->saveCategory_Permissions($this->groups_forum_can_edit, $this->forum_id, 'forum_can_edit');
        $perm->saveCategory_Permissions($this->groups_forum_can_delete, $this->forum_id, 'forum_can_delete');
        $perm->saveCategory_Permissions($this->groups_forum_can_addpoll, $this->forum_id, 'forum_can_addpoll');
        $perm->saveCategory_Permissions($this->groups_forum_can_vote, $this->forum_id, 'forum_can_vote');
        $perm->saveCategory_Permissions($this->groups_forum_can_attach, $this->forum_id, 'forum_can_attach');
     
        }
        else
        {
            $sql = "UPDATE " . $this -> table . " SET forum_name='" . $forum_name . "', forum_desc='" . $forum_desc . "',
             forum_moderator='" .$this->forum_moderator ."',
             cat_id=" . $this->cat_id . ", forum_type='" . $this->forum_type . "', allow_html='" .$this->allow_html ."',
             allow_sig='" . $this->allow_sig . "',show_name='" . $this->show_name . "',show_icons='" . $this->show_icons . "',
             show_smilies='" . $this->show_smilies . "', posts_per_page=" . $this->posts_per_page . ", hot_threshold=" .$this->hot_threshold .",
             topics_per_page=" . $this->topics_per_page . ", forum_order=" . $this->forum_order . ", allow_attachments='" .$this->allow_attachments ."',
             attach_maxkb=" . $this->attach_maxkb . ", attach_ext='" . $attach_ext . "', allow_polls='" .$this->allow_polls ."'
             WHERE forum_id=" . $this -> forum_id . " ";
        $perm->saveCategory_Permissions($this->groups_forum_access, $this->forum_id, 'global_forum_access');
        $perm->saveCategory_Permissions($this->groups_forum_can_post, $this->forum_id, 'forum_can_post');
        $perm->saveCategory_Permissions($this->groups_forum_can_view, $this->forum_id, 'forum_can_view');
        $perm->saveCategory_Permissions($this->groups_forum_can_reply, $this->forum_id, 'forum_can_reply');
        $perm->saveCategory_Permissions($this->groups_forum_can_edit, $this->forum_id, 'forum_can_edit');
        $perm->saveCategory_Permissions($this->groups_forum_can_delete, $this->forum_id, 'forum_can_delete');
        $perm->saveCategory_Permissions($this->groups_forum_can_addpoll, $this->forum_id, 'forum_can_addpoll');
        $perm->saveCategory_Permissions($this->groups_forum_can_vote, $this->forum_id, 'forum_can_vote');
        $perm->saveCategory_Permissions($this->groups_forum_can_attach, $this->forum_id, 'forum_can_attach');
              
        }

        if ( !$result = $this -> db -> query( $sql ) )
        {
            echo $sql;
            ErrorHandler :: show( '0022' );

        }
        return true;
    }

    function delete()
    {
        $sql = "DELETE FROM " . $this -> table . " WHERE forum_id=" . $this -> forum_id . "";
        $this -> db -> query( $sql );
    }
    function cat_id()
    {
        return $this -> cat_id;
    }

    function pid()
    {
        return $this -> pid;
    }

    function forum_name( $format = "S" )
    {
        if ( !isset( $this -> forum_name ) ) return "";
        $myts = & MyTextSanitizer :: getInstance();
        switch( $format )
        {
        case "S":
            $forum_name = $myts -> htmlSpecialChars( $this -> forum_name );
            break;
        case "E":
            $forum_name = $myts -> htmlSpecialChars( $this -> forum_name );
            break;
        case "P":
            $forum_name = $myts -> htmlSpecialChars($myts->stripSlashesGPC( $this -> forum_name ));
            break;
        case "F":
            $forum_name = $myts -> htmlSpecialChars($myts->stripSlashesGPC( $this -> forum_name ));
            break;
        }
        return $forum_name;
    }

    function forum_desc( $format = "S" )
    {
        if ( !isset( $this -> forum_desc ) ) return "";
        $html = 1;
        $smiley = 1;
        $xcodes = 1;

        $myts = & MyTextSanitizer :: getInstance();
        switch( $format )
        {
        case "S":
            $forum_desc = $myts -> displayTarea( $this -> forum_desc, $html, $smiley, $xcodes );
            break;
        case "E":
            $forum_desc = $myts -> htmlSpecialChars( $this -> forum_desc );
            break;
        case "P":
            $forum_desc = $myts -> previewTarea( $this -> forum_desc, $html, $smiley, $xcodes );
            break;
        case "F":
            $forum_desc = $myts -> previewTareaInForm( $this -> forum_desc );
            break;
        }
        return $forum_desc;
    }


	function forum_order()
    {
        return $this -> forum_order;
    }
	
	

    function getFirstChild()
    {
        $ret = array();
        $xt = new XoopsTree( $this -> table, "forum_id", 0 );
        $forum_arr = $xt -> getFirstChild( $this -> forum_id, 'forum_order' );
        if ( is_array( $forum_arr ) && count( $forum_arr ) )
        {
            foreach( $forum_arr as $forum )
            {
                $ret[] = new Forums( $forum );
            }
        }
        return $ret;
    }

    function getAllChild()
    {
        $ret = array();
        $xt = new XoopsTree( $this -> table, "forum_id", "cat_id" );
        $forum_arr = $xt -> getAllChild( $this -> forum_id, orders );
        if ( is_array( $forum_arr ) && count( $forum_arr ) )
        {
            foreach( $forum_arr as $forum )
            {
                $ret[] = new Forums( $forum );
            }
        }
        return $ret;
    }

    function getChildTreeArray()
    {
        $ret = array();
        $xt = new XoopsTree( $this -> table, "forum_id", "cat_id" );
         $forum_arr = $xt -> getChildTreeArray( $this -> forum_id, "forum_order" );
        if ( is_array(  $forum_arr ) && count(  $forum_arr ) )
        {
            foreach(  $forum_arr as $forum )
            {
                $ret[] = new Forums( $forum );
            }
        }
        return $ret;
    }

    function getAllChildId( $sel_id = 0, $order = "", $parray = array() )
    {
        $sql = "SELECT forum_id FROM " . $this -> table . " WHERE forum_id=" . $sel_id . "";
        if ( $order != "" )
        {
            $sql .= " ORDER BY $order";
        }
        $result = $this -> db -> query( $sql );
        $count = $this -> db -> getRowsNum( $result );
        if ( $count == 0 )
        {
            return $parray;
        }
        while ( $row = $this -> db -> fetchArray( $result ) )
        {
            array_push( $parray, $row['forum_id'] );
            $parray = $this -> getAllChildId( $row['forum_id'], $order, $parray );
        }
        return $parray;
    }

    function isInChild( $sel_id )
    {
        if ( empty( $this -> forum_id ) ) return false;
        if ( $sel_id == $this -> forum_id ) return true;
        $child = $this -> getAllChildId();
        if ( in_array( $sel_id, $child ) ) return true;
        return false;
    }
    function countByForums( $forum_id = 0 )
    {
        $db = & Database :: getInstance();
        $sql = "SELECT COUNT(*) FROM " . $db -> prefix( "bb_forums" ) . "";
        if ( $forum_id != 0 )
        {
            $sql .= " WHERE forum_id=$forum_id";
        }
        $result = $db -> query( $sql );
        list( $count ) = $db -> fetchRow( $result );
        return $count;
    }
    function makeSelBox( $none = 0, $selcategory = -1, $selname = "", $onchange = "" )
    {
        $xt = new xoopstree( $this -> table, "forum_id", 0 );
        if ( $selcategory != -1 )
        {
            $xt -> makeMySelBox( "forum_name", "forum_name", $selcategory, $none, $selname, $onchange );
        }elseif ( !empty( $this -> forum_id ) )
        {
            $xt -> makeMySelBox( "forum_name", "forum_name", $this -> forum_id, $none, $selname, $onchange );
        }
        else
        {
            $xt -> makeMySelBox( "forum_name", "forum_name", 0, $none, $selname, $onchange );
        }
    }
    function getNicePathFromId( $funcURL )
    {
        $xt = new XoopsTree( $this -> table, "forum_id", "pid" );
        $ret = $xt -> getNicePathFromId( $this -> forum_id, "forum_name", $funcURL );
        return $ret;
    }

     function textLink()
    {
        global $xoopsModule;

        $ret = "<a href='" . XOOPS_URL . "/modules/" . $xoopsModule -> dirname() . "/viewforum.php?forum=" . $this -> forum_id() . "'>" . $this -> forum_name() . "</a>";
        return $ret;
    }

    function getAllTopics($forum,$startdate,$start,$sortname,$sortorder,$topics_per_page,$topic_lastread,$forum_lastview,$hot_threshold,$posts_per_page,$UserUid)
    {
     global $xoopsTpl, $xoopsConfig, $forumImage,$myts;
     $db = & Database :: getInstance();
     $sticky=0;
     $topic_att = "";
     $sql = 'SELECT t.*, u.uname, u2.uname as last_poster, p.post_time as last_post_time, p.icon FROM '.$db->prefix("bb_topics").' t LEFT JOIN '.$db->prefix("users").' u ON u.uid = t.topic_poster LEFT JOIN '.$db->prefix('bb_posts').' p ON p.post_id = t.topic_last_post_id LEFT JOIN '.$db->prefix("users").' u2 ON  u2.uid = p.uid WHERE t.forum_id = '.$forum.' AND (p.post_time > '.$startdate.' OR t.topic_sticky=1) ORDER BY topic_sticky DESC, '.$sortname.' '.$sortorder;
     if ( !$result = $db->query($sql,$topics_per_page,$start) ) {
        redirect_header('index.php',2,_MD_ERROROCCURED);
        exit();
     }


     while ( $myrow = $db->fetchArray($result) ) {
     $rating = number_format($myrow['rating'], 0) / 2;
     if ( $rating < 1 ) {
        $rateimg = "blank.gif";
     }
     else
     {
     $rateimg = "rate$rating.gif";
     }
     if ( empty($myrow['last_poster']) ) {
        $myrow['last_poster'] = $xoopsConfig['anonymous'];
     }
     if ( $myrow['topic_status'] == 1 ) {
        $image = $forumImage['locked_topic'];
     } else {
                if ( $myrow['topic_replies'] >= $hot_threshold ) {
                        if ( empty($topic_lastread[$myrow['topic_id']]) || ($topic_lastread[$myrow['topic_id']] < $myrow['last_post_time'] )) {
                                $image = $forumImage['hot_newposts_topic'];
                        } else {
                                if ($myrow['topic_poster']==$UserUid) $image = $forumImage['hot_user_folder_topic'];
                                else $image = $forumImage['hot_folder_topic'];
                        }

                        if(!empty($forum_lastview[$forum]) && $forum_lastview[$forum]>$myrow['last_post_time'])
                        {

                                if ($myrow['topic_poster']==$UserUid) $image = $forumImage['hot_user_folder_topic'];
                                else $image = $forumImage['hot_folder_topic'];
                        }

                } else {
                        if ( empty($topic_lastread[$myrow['topic_id']]) || ($topic_lastread[$myrow['topic_id']] < $myrow['last_post_time'] )) {
                                if ($myrow['topic_poster']==$UserUid) $image = $forumImage['newposts_user_topic'];
                                else $image = $forumImage['newposts_topic'];
                        } else {
                                if ($myrow['topic_poster']==$UserUid) $image = $forumImage['folder_user_topic'];
                                else $image = $forumImage['folder_topic'];
                        }
                        if(!empty($forum_lastview[$forum]) && $forum_lastview[$forum]>$myrow['last_post_time'])
                        {
                                if($myrow['topic_poster']==$UserUid) $image = $forumImage['folder_user_topic'];
                                else $image = $forumImage['folder_topic'];
                        }
                }
         }
        $pagination = '';
        $addlink = '';
        $topiclink = 'viewtopic.php?topic_id='.$myrow['topic_id'].'&amp;forum='.$forum;
        $totalpages = ceil(($myrow['topic_replies'] + 1) / $posts_per_page);
        if ( $totalpages > 1 ) {
                $pagination .= '&nbsp;&nbsp;&nbsp;<img src="'.XOOPS_URL.'/images/icons/posticon.gif" /> ';
                for ( $i = 1; $i <= $totalpages; $i++ ) {

                        if ( $i > 3 && $i < $totalpages ) {
                                $pagination .= "...";
                        } else {
                                $addlink = '&start='.(($i - 1) * $posts_per_page);
                                $pagination .= '[<a href="'.$topiclink.$addlink.'">'.$i.'</a>]';
                        }
                }
        }

        if ( $myrow['icon'] ) {
                if ( $myrow['topic_sticky'] == 1 ) {
                $topic_icon='<img src="'.$forumImage['folder_sticky'].'" align=absmiddle alt= "'._MD_TOPICSTICKY.'">';
                $stick=0;
                $sticky++;
                }
                else
                {
                $topic_icon = '<img src="'.XOOPS_URL.'/images/subject/'.$myrow['icon'].'" alt="" />';
                $stick=1;
                }
        } else {
                if ( $myrow['topic_sticky'] == 1 ) {

                $topic_icon='<img src="'.$forumImage['folder_sticky'].'" align=absmiddle alt= "'._MD_TOPICSTICKY.'">';
                $stick=0;
                $sticky++;
                }
                else
                {
                $topic_icon = '<img src="'.XOOPS_URL.'/images/icons/no_posticon.gif" alt="" />';
                $stick=1;
                }
        }
        if ( $myrow['topic_poster'] != 0 && $myrow['uname'] ) {
                $topic_poster = '<a href="'.XOOPS_URL.'/userinfo.php?uid='.$myrow['topic_poster'].'">'.$myrow['uname'].'</a>';
        } else {
                $topic_poster = $xoopsConfig['anonymous'];
        }

        $result2 = $db->query("SELECT attachment FROM ".$db->prefix("bb_posts")." WHERE topic_id = ".$myrow['topic_id']);
    if (!$result2){
        return false;
            }
                  while ($arr2 = $db->fetchArray($result2))
                {
                if ($arr2['attachment'] !='')
                        {
                                $topic_att='&nbsp;<img src="'.$forumImage['clip'].'" align=absmiddle alt= "'._MD_TOPICSHASATT.'">';
                         }
                }
                $xoopsTpl->append('topics', array('topic_icon'=>$topic_icon, 'stick'=>$stick,'topic_folder'=>$image, 'topic_title'=>$myts->htmlSpecialChars($myrow['topic_title']).$topic_att, 'topic_link'=>$topiclink, 'rating_img'=>$rateimg,'topic_page_jump'=>$pagination, 'topic_replies'=>$myrow['topic_replies'], 'topic_poster'=>$topic_poster, 'topic_views'=>$myrow['topic_views'], 'topic_last_posttime'=>formatTimestamp($myrow['last_post_time']), 'topic_last_poster'=>$myts->htmlSpecialChars($myrow['last_poster'])));
                $topic_att = "";

    }
    return $sticky;
    }





}

?>