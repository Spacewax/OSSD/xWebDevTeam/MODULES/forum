<?php
// $Id: class.category.php,v 1.3 2004/05/29 17:11:48 praedator Exp $
//  ------------------------------------------------------------------------ //
//                XOOPS - PHP Content Management System                      //
//                    Copyright (c) 2000 XOOPS.org                           //
//                       <http://www.xoops.org/>                             //
//  ------------------------------------------------------------------------ //
//  This program is free software; you can redistribute it and/or modify     //
//  it under the terms of the GNU General Public License as published by     //
//  the Free Software Foundation; either version 2 of the License, or        //
//  (at your option) any later version.                                      //
//                                                                           //
//  You may not change or alter any portion of this comment or credits       //
//  of supporting developers from this source code or any supporting         //
//  source code which is considered copyrighted (c) material of the          //
//  original comment or credit authors.                                      //
//                                                                           //
//  This program is distributed in the hope that it will be useful,          //
//  but WITHOUT ANY WARRANTY; without even the implied warranty of           //
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            //
//  GNU General Public License for more details.                             //
//                                                                           //
//  You should have received a copy of the GNU General Public License        //
//  along with this program; if not, write to the Free Software              //
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA //
//  ------------------------------------------------------------------------ //
// Author: Kazumi Ono (AKA onokazu)                                          //
// URL: http://www.myweb.ne.jp/, http://www.xoops.org/, http://jp.xoops.org/ //
// Project: The XOOPS Project                                                //
// ------------------------------------------------------------------------- //

include_once( XOOPS_ROOT_PATH . "/class/xoopstree.php" );
include_once( XOOPS_ROOT_PATH . "/class/module.errorhandler.php" );


class ForumCat
{
	var $db;
	var $table;
	var $cat_id;
	var $pid = 0;
	var $groups_cat_access;
	var $cat_title;
	var $cat_image;
	var $cat_description;
	var $cat_order;
	var $cat_state;
	var $cat_url;
	var $cat_showdescript;
	
	function ForumCat( $catid = 0 )
	{
		$this -> db = & Database :: getInstance();
		$this -> table = $this -> db -> prefix( "bb_categories" );
		
		if ( is_array( $catid ) )
		{
			$this -> makeCategory( $catid );
		}elseif ( $catid != 0 )
		{
			$this -> loadCategory( $catid );
		}
		else
		{
			$this -> cat_id = $catid;
		}
	}
	
	function setcat_title( $value )
	{
		$this -> cat_title = $value;
	}
	
	function setcat_image( $value )
	{
		$this -> cat_image = $value;
	}
	
	function setcat_description( $cat_description )
	{
		$this -> cat_description = $cat_description;
	}
	
	function setPid( $value )
	{
		$this -> pid = 0;
	}
	
	function setcat_order( $value )
	{
		$this -> cat_order = $value;
	}
	
	function setcat_groups_cat_access( $value )
	{
		$this -> groups_cat_access = $value;
	}
	
	function setcat_state( $value )
	{
		$this -> cat_state = $value;
	}
	
	function setcat_url( $value )
	{
		$this -> cat_url = $value;
	}
	
	function setcat_showdescript( $value )
	{
		$this -> cat_showdescript = $value;
	}
	
	function loadCategory( $cat_id )
	{
		$sql = "SELECT * FROM " . $this -> table . " WHERE cat_id=" . $cat_id . "";
		$array = $this -> db -> fetchArray( $this -> db -> query( $sql ) );
		$this -> makeCategory( $array );
	}
	
	function makeCategory( $array )
	{
		foreach( $array as $key => $value )
		{
			$this -> $key = $value;
		}
	}
	
	function store()
	{
		global $myts, $xoopsModule;
		include_once XOOPS_ROOT_PATH . '/modules/' . $xoopsModule ->dirname() . '/class/class.permission.php';
		
		$myts = & MyTextSanitizer :: getInstance();
		
		if ( isset( $this -> cat_title ) && $this -> cat_title != "" )
		{
			$cat_title = $myts -> addSlashes( $this -> cat_title );
		}
		
		if ( isset( $this -> cat_image ) && $this -> cat_image != "" )
		{
			$cat_image = $myts -> addSlashes( $this -> cat_image );
		}
		
		if ( isset( $this -> cat_description ) && $this -> cat_description != "" )
		{
			$cat_description = $myts -> addSlashes( $this -> cat_description );
		}
		
		if ( !isset( $this -> pid ) || !is_numeric( $this -> pid ) )
		{
			$this -> pid = 0;
		}
		
		if ( !isset( $this -> cat_state ) || !is_numeric( $this -> cat_state ) )
		{
			$this -> cat_state = 0;
		}
		
		if ( isset( $this -> cat_url ) && $this -> cat_url != "" )
		{
			$cat_url = $myts -> addSlashes( $this -> cat_url );
		}
		
		if ( !isset( $this -> cat_showdescript ) || !is_numeric( $this -> cat_showdescript ) )
		{
			$this -> cat_showdescript = 0;
		}
		$perm = new Permissions();
		
		if ( empty( $this -> cat_id ) )
		{
			
			$this -> cat_id = $this -> db -> genId( $this -> table . "_cat_id_seq" );
			$sql = "INSERT INTO " . $this -> table . " (cat_id, cat_image, cat_title, cat_description, cat_order, cat_state, cat_url, cat_showdescript) VALUES (" . $this -> cat_id . ", '" . $cat_image . "', '" . $cat_title . "', '" . $cat_description . "', " . $this -> cat_order . ", '" . $this -> cat_state . "', '" . $this -> cat_url . "', '" . $this -> cat_showdescript . "' )";
			$perm->saveCategory_Permissions($this->groups_cat_access, $this->cat_id, 'forum_cat_access');
			
		}
		else
		{
			$sql = "UPDATE " . $this -> table . " SET cat_image='" . $cat_image . "', cat_title='" . $cat_title . "', cat_description='" . $cat_description . "', cat_order='" . $this -> cat_order . "',  cat_state='" . $this -> cat_state . "', cat_url='" . $this -> cat_url . "',  cat_showdescript='" . $this -> cat_showdescript . "' WHERE cat_id='" . $this -> cat_id . "'";
			$perm->saveCategory_Permissions($this->groups_cat_access, $this->cat_id, 'forum_cat_access');
		}
		
		if ( !$result = $this -> db -> query( $sql ) )
		{
			ErrorHandler :: show( '0022' );
		}
		return true;
	}
	
	function delete()
	{
		$sql = "DELETE FROM " . $this -> table . " WHERE cat_id=" . $this -> cat_id . "";
		$this -> db -> query( $sql );
	}
	function cat_id()
	{
		return $this -> cat_id;
	}
	
	function pid()
	{
		return $this -> pid;
	}
	
	function cat_title( $format = "S" )
	{
		if ( !isset( $this -> cat_title ) ) return "";
		$myts = & MyTextSanitizer :: getInstance();
		switch( $format )
		{
			case "S":
			$cat_title = $myts -> htmlSpecialChars( $this -> cat_title );
			break;
			case "E":
			$cat_title = $myts -> htmlSpecialChars( $this -> cat_title );
			break;
			case "P":
			$cat_title = $myts -> htmlSpecialChars($myts->stripSlashesGPC( $this -> cat_title ));
			break;
			case "F":
			$cat_title = $myts -> htmlSpecialChars($myts->stripSlashesGPC( $this -> cat_title ));
			break;
		}
		return $cat_title;
	}
	
	function cat_image( $format = "S" )
	{
		$myts = & MyTextSanitizer :: getInstance();
		switch( $format )
		{
			case "S":
			$cat_image = $myts -> htmlSpecialChars( $this -> cat_image );
			break;
			case "E":
			$cat_image = $myts -> htmlSpecialChars( $this -> cat_image );
			break;
			case "P":
			$cat_image = $myts -> htmlSpecialChars($myts->stripSlashesGPC( $this -> cat_image ));
			break;
			case "F":
			$cat_image = $myts -> htmlSpecialChars($myts->stripSlashesGPC( $this -> cat_image ));
			break;
		}
		return $cat_image;
	}
	
	function cat_description( $format = "S" )
	{
		if ( !isset( $this -> cat_description ) ) return "";
		$html = 1;
		$smiley = 1;
		$xcodes = 1;
		
		$myts = & MyTextSanitizer :: getInstance();
		switch( $format )
		{
			case "S":
			$cat_description = $myts -> displayTarea( $this -> cat_description, $html, $smiley, $xcodes );
			break;
			case "E":
			$cat_description = $myts -> htmlSpecialChars( $this -> cat_description );
			break;
			case "P":
			$cat_description = $myts -> previewTarea( $this -> cat_description, $html, $smiley, $xcodes );
			break;
			case "F":
			$cat_description = $myts -> previewTareaInForm( $this -> cat_description );
			break;
		}
		return $cat_description;
	}
	
	function cat_url( $format = "Show" )
	{
		$myts = & MyTextSanitizer :: getInstance();
		switch( $format )
		{
			case "S":
			$cat_url = $myts -> htmlSpecialChars( $this -> cat_url, 0 );
			break;
			case "E":
			$cat_url = $myts -> htmlSpecialChars( $this -> cat_url );
			break;
			case "P":
			$cat_url = $myts -> htmlSpecialChars($myts->stripSlashesGPC( $this -> cat_url, 0 ));
			break;
			case "F":
			$cat_url = $myts -> htmlSpecialChars($myts->stripSlashesGPC( $this -> cat_url ));
			break;
		}
		return $cat_url;
	}
	
	function cat_order()
	{
		return $this -> cat_order;
	}
	
	function cat_state()
	{
		return $this -> cat_state;
	}
	
	function cat_showdescript()
	{
		return $this -> cat_showdescript;
	}
	
	function groups_cat_access()
	{
		return $this -> cat_state;
	}
	
	function getFirstChild()
	{
		$ret = array();
		$xt = new XoopsTree( $this -> table, "cat_id", 0 );
		$category_arr = $xt -> getFirstChild( $this -> cat_id, 'cat_order' );
		if ( is_array( $category_arr ) && count( $category_arr ) )
		{
			foreach( $category_arr as $category )
			{
				$ret[] = new ForumCat( $category );
			}
		}
		return $ret;
	}
	
	function getAllChild()
	{
		$ret = array();
		$xt = new XoopsTree( $this -> table, "cat_id", "pid" );
		$category_arr = $xt -> getAllChild( $this -> cat_id, orders );
		if ( is_array( $category_arr ) && count( $category_arr ) )
		{
			foreach( $category_arr as $category )
			{
				$ret[] = new ForumCat( $category );
			}
		}
		return $ret;
	}
	
	function getChildTreeArray()
	{
		$ret = array();
		$xt = new XoopsTree( $this -> table, "cat_id", "pid" );
		$category_arr = $xt -> getChildTreeArray( $this -> cat_id, "cat_order" );
		if ( is_array( $category_arr ) && count( $category_arr ) )
		{
			foreach( $category_arr as $category )
			{
				$ret[] = new ForumCat( $category );
			}
		}
		return $ret;
	}
	
	function getAllChildId( $sel_id = 0, $order = "", $parray = array() )
	{
		$sql = "SELECT cat_id FROM " . $this -> table . " WHERE pid=" . $sel_id . "";
		if ( $order != "" )
		{
			$sql .= " ORDER BY $order";
		}
		$result = $this -> db -> query( $sql );
		$count = $this -> db -> getRowsNum( $result );
		if ( $count == 0 )
		{
			return $parray;
		}
		while ( $row = $this -> db -> fetchArray( $result ) )
		{
			array_push( $parray, $row['cat_id'] );
			$parray = $this -> getAllChildId( $row['cat_id'], $order, $parray );
		}
		return $parray;
	}
	
	function isInChild( $sel_id )
	{
		if ( empty( $this -> cat_id ) ) return false;
		if ( $sel_id == $this -> cat_id ) return true;
		$child = $this -> getAllChildId();
		if ( in_array( $sel_id, $child ) ) return true;
		return false;
	}
	function countByCategory( $catid = 0 )
	{
		$db = & Database :: getInstance();
		$sql = "SELECT COUNT(*) FROM " . $db -> prefix( "bb_categories" ) . "";
		if ( $catid != 0 )
		{
			$sql .= " WHERE cat_id=$catid";
		}
		$result = $db -> query( $sql );
		list( $count ) = $db -> fetchRow( $result );
		return $count;
	}
	function makeSelBox( $none = 0, $selcategory = -1, $selname = "", $onchange = "" )
	{
		$xt = new xoopstree( $this -> table, "cat_id", 0 );
		if ( $selcategory != -1 )
		{
			$xt -> makeMySelBox( "cat_title", "cat_title", $selcategory, $none, $selname, $onchange );
		}elseif ( !empty( $this -> cat_id ) )
		{
			$xt -> makeMySelBox( "cat_title", "cat_title", $this -> cat_id, $none, $selname, $onchange );
		}
		else
		{
			$xt -> makeMySelBox( "cat_title", "cat_title", 0, $none, $selname, $onchange );
		}
	}
	function getNicePathFromId( $funcURL )
	{
		$xt = new XoopsTree( $this -> table, "cat_id", "pid" );
		$ret = $xt -> getNicePathFromId( $this -> cat_id, "cat_title", $funcURL );
		return $ret;
	}
	
	function imgLink()
	{
		global $xoopsModule;
		
		$ret = "<a href='" . XOOPS_URL . "/modules/" . $xoopsModule -> dirname() . "/index.php?cat=" . $this -> cat_id() . "'>" . "<img src='" . XOOPS_URL . "/modules/" . $xoopsModule -> dirname() . "/images/topics/" . $this -> cat_image() . "' alt='" . $this -> cat_title() . "' /></a>";
		return $ret;
	}
	
	function textLink()
	{
		global $xoopsModule;
		
		$ret = "<a href='" . XOOPS_URL . "/modules/" . $xoopsModule -> dirname() . "/index.php?cat=" . $this -> cat_id() . "'>" . $this -> cat_title() . "</a>";
		return $ret;
	}
}

?>